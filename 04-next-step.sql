INSERT INTO `platform_font_public_library` (`id`, `name`, `order_index`, `deleted`, `version`) VALUES
(1, 'Main Library', 0, 0, 0);

INSERT INTO `platform_font_public` (`id`, `name`, `class_name`, `def_sz`, `min_sz`, `max_sz`, `data_id`, `deleted`, `library_id`, `order_index`, `version`) VALUES
(1, 'American Text', 'AmericanTextRegular', 32, 10, 96, 14, 0, 1, 0, 0),
(2, 'Alte DIN 1451', 'AlteDIN1451Regular', 32, 10, 96, 15, 0, 1, 0, 0),
(3, 'Dosis', 'DosisSemiBold', 32, 10, 96, 16, 0, 1, 0, 0),
(4, 'Droid Sans', 'DroidSansBold', 32, 10, 96, 17, 0, 1, 0, 0),
(5, 'Fette UNZ Fraktur', 'FetteUNZFrakturRegular', 32, 10, 96, 18, 0, 1, 0, 0),
(6, 'Germania One', 'GermaniaOneRegular', 32, 10, 96, 19, 0, 1, 0, 0),
(7, 'Jockey One', 'JockeyOneRegular', 32, 10, 96, 20, 0, 1, 0, 0),
(8, 'Lobster', 'LobsterRegular', 32, 10, 96, 21, 1, 1, 0, 0),
(9, 'Londrina Solid', 'LondrinaSolidRegular', 32, 10, 96, 22, 0, 1, 0, 0),
(10, 'Lora', 'LoraBold', 32, 10, 96, 23, 1, 1, 0, 0),
(11, 'Nunito', 'NunitoBold', 32, 10, 96, 24, 1, 1, 0, 0),
(12, 'Old London', 'OldLondonRegular', 32, 10, 96, 25, 0, 1, 0, 0),
(13, 'Passion One', 'PassionOneRegular', 32, 10, 96, 26, 0, 1, 0, 0),
(14, 'Patua One', 'PatuaOneRegular', 32, 10, 96, 27, 0, 1, 0, 0),
(15, 'Paytone One', 'PaytoneOneRegular', 32, 10, 96, 28, 0, 1, 0, 0),
(16, 'Pirata One', 'PirataOneRegular', 32, 10, 96, 29, 0, 1, 0, 0),
(17, 'Sancreek', 'SancreekRegular', 32, 10, 96, 30, 0, 1, 0, 0),
(18, 'Shojumaru', 'ShojumaruRegular', 32, 10, 96, 31, 0, 1, 0, 0),
(19, 'Ubuntu', 'UbuntuRegular', 32, 10, 96, 32, 0, 1, 0, 0),
(23, 'Pacifico', 'CreatorFontData_160', 32, 10, 96, 160, 0, 1, 0, 0),
(25, 'Lora', 'CreatorFontData_159', 32, 10, 96, 159, 0, 1, 0, 0),
(26, 'Nunito', 'CreatorFontData_158', 32, 10, 96, 158, 0, 1, 0, 0),
(27, 'Montserrat', 'CreatorFontData_157', 32, 10, 96, 157, 0, 1, 0, 0),
(29, 'Lobster', 'CreatorFontData_155', 32, 10, 96, 155, 0, 1, 0, 0);

INSERT INTO `platform_shipping_module_type` (`id`, `name`) VALUES
(4, 'ATPOSTSS'),
(2, 'BALIKOBOT'),
(3, 'DEPOST'),
(11, 'EASYPOST'),
(10, 'GENERIC'),
(6, 'GLS-CZ'),
(5, 'GLS-SK'),
(1, 'SHIPCLOUD'),
(9, 'SHIPPYPRO'),
(8, 'SPOD'),
(7, 'SPRINGGDS');

INSERT INTO `platform_shipping_module_provider` (`id`, `module_type`, `provider`, `service_type`, `name`) VALUES
(1, 1, 'dhl', 'standard', 'DHl - Standard'),
(2, 1, 'dhl_express', 'one_day_early', 'DHL Express - One Day Early'),
(3, 2, 'intime', '1', 'Intime - Small Colli 24 - CZ'),
(4, 2, 'cp', 'DR', 'ČP - balík do ruky'),
(5, 3, '', '', 'DEPOST'),
(6, 4, '', '', 'ATPOSTSS'),
(7, 2, 'cp', 'RR', 'ČP - doporučená zásilka'),
(8, 2, 'cp', 'NP', 'ČP - na poštu'),
(9, 2, 'cp', 'DV', 'ČP - balík do ruky pro vybrané podavatele'),
(10, 2, 'cp', 'VL', 'ČP - cenné psaní'),
(11, 2, 'cp', 'SR', 'ČP - doporučená zásilka standard'),
(12, 2, 'cp', 'BA', 'ČP - doporučený balíček'),
(13, 2, 'cp', 'BB', 'ČP - cenný balík'),
(14, 2, 'cp', 'BN', 'ČP - balík nadrozměr'),
(15, 2, 'intime', '3', 'Intime - Large Colli 24 - CZ'),
(16, 2, 'intime', '4', 'Intime - Poštomat CZ'),
(17, 2, 'intime', '5', 'Intime - Poštomat SK'),
(18, 2, 'intime', '6', 'Intime - Large Colli 48 - SK'),
(19, 2, 'intime', '7', 'Intime - Extra Large Colli 24 - CZ'),
(20, 1, 'dhl_express', 'one_day', 'DHL Express - One Day'),
(21, 2, 'intime', '2', 'Intime - Medium Colli 24 - CZ'),
(22, 5, '', '', 'GLS - Standard'),
(23, 2, 'zasilkovna', '', 'Zásilkovna'),
(24, 1, 'dhl', 'one_day_early', 'DHL - One Day Early'),
(25, 1, 'dhl', 'one_day', 'DHL - One Day'),
(26, 6, '', '', 'GLS CZ - Standard'),
(27, 3, '', 'kilotarif', 'DE POST KILOTARIF'),
(28, 1, 'gls', 'standard', 'GLS - Standard'),
(29, 1, 'gls', 'one_day', 'GLS - One Day'),
(31, 2, 'dpd', '1', 'DPD Classic'),
(33, 8, '', 'standard', 'Standard'),
(34, 8, '', 'premium', 'Premium'),
(35, 8, '', 'express', 'Express'),
(40, 7, '', 'PPBT', 'GDS - PPBT'),
(41, 7, '', 'PPNT', 'GDS - PPNT'),
(42, 7, '', 'PPTT', 'GDS - PPTT'),
(43, 7, '', 'PPTR', 'GDS - PPTR'),
(50, 2, 'dpd', '2', 'DPD Private'),
(51, 7, '', 'PPLEU', 'GDS - PPLEU'),
(60, 9, '1113', 'Generic', 'Generic'),
(70, 10, 'generic_standard', 'generic_standard', 'Generic Standard'),
(71, 10, 'generic_premium', 'generic_premium', 'Generic Premium'),
(72, 10, 'generic_express', 'generic_express', 'Generic Express'),
(80, 11, 'usps', 'ParcelSelect', 'USPS - ParcelSelect'),
(81, 11, 'usps', 'Priority', 'USPS - Priority'),
(82, 11, 'usps', 'First', 'USPS - First'),
(83, 11, 'usps', 'Express', 'USPS - Express'),
(90, 1, 'dpag', 'standard_grosbrief', 'DP - Großbrief'),
(91, 1, 'dpag', 'standard_maxi', 'DP - Maxi');


INSERT INTO `platform_production_display_module` (`id`, `name`, `deleted`) VALUES
(8, 'LAYER', 0),
(9, 'MOTIVE', 0),
(10, 'COMPOSITION', 0),
(11, 'VIEW', 0);

INSERT INTO `platform_production_file_module` (`id`, `name`, `deleted`) VALUES
(5, 'FILE_NAME_PATERN_SPLIT', 0);

INSERT INTO `platform_production_response_module` (`id`, `name`, `deleted`) VALUES
(9, 'ZIP_PDF', 0),
(10, 'ZIP_IMG', 0);

INSERT INTO `platform_production_transformer_module` (`id`, `name`, `deleted`) VALUES
(8, 'BASIC_TILING', 0),
(9, 'AS_COMPOSITION', 0),
(10, 'FIXED_POSITION', 0),
(11, 'ADVANCED_TILING', 0),
(12, 'HORIZONTAL_TILING', 0);

INSERT INTO `platform_production_display_module_property` (`id`, `display_module_id`, `name`, `value`) VALUES
(1, 8, 'ENABLE_OUTLINE', 'true'),
(2, 9, 'ENABLE_OUTLINE', 'false'),
(3, 10, 'ENABLE_OUTLINE', 'false'),
(4, 10, 'AVOID_OUTLINE_INTERSECTION', 'true'),
(5, 8, 'FILL_OUTLINE', 'false'),
(6, 9, 'FILL_OUTLINE', 'false'),
(7, 10, 'FILL_OUTLINE', 'false'),
(8, 11, 'AVOID_OUTLINE_INTERSECTION', 'true'),
(9, 11, 'FILL_OUTLINE', 'false'),
(10, 11, 'ENABLE_OUTLINE', 'false'),
(11, 11, 'HORIZONTAL_TILING', 'false'),
(12, 8, 'BOUNDS_ONLY', 'false'),
(13, 9, 'BOUNDS_ONLY', 'false'),
(14, 10, 'BOUNDS_ONLY', 'false'),
(15, 11, 'BOUNDS_ONLY', 'false'),
(20, 8, 'HORIZONTAL_MIRROR_RESULT', 'false'),
(21, 9, 'HORIZONTAL_MIRROR_RESULT', 'false'),
(22, 10, 'HORIZONTAL_MIRROR_RESULT', 'false'),
(23, 11, 'HORIZONTAL_MIRROR_RESULT', 'false'),
(24, 11, 'CUT_BY_MASK', 'true'),
(25, 8, 'CUT_BY_MASK', 'true'),
(26, 9, 'CUT_BY_MASK', 'true'),
(27, 10, 'CUT_BY_MASK', 'true');

INSERT INTO `platform_production_file_module_property` (`id`, `file_module_id`, `name`, `value`) VALUES
(1, 5, 'NAME_PATTERN', '%TECHNOLOGY_ID%_%ORDER_ID%_%PRODUCT_ID%_%COMPOSITION_ID%_%AMOUNT_INDEX%'),
(2, 5, 'PAGE_SORTER', 'PRODUCT'),
(4, 5, 'GROUP_PAGED_OBJECTS', 'false'),
(5, 5, 'IGNORE_AMOUNT', 'false'),
(6, 5, 'INCLUDE_EMPTY_TEMPLATES', 'false'),
(7, 5, 'INCLUDE_BACKGROUND_TEMPLATE', 'true');

INSERT INTO `platform_production_response_module_property` (`id`, `response_module_id`, `name`, `value`) VALUES
(3, 9, 'FILE_NAME_PATTERN', 'response.zip'),
(4, 9, 'TECH_FILE_NAME_PATTERN', '%YYYY-MM-DD%_%TECHNOLOGY_NAME%_%SUMMARY_ID%.zip'),
(5, 9, 'SORT_BY_POSITION', 'false'),
(6, 10, 'FILE_NAME_PATTERN', 'response.zip'),
(7, 10, 'TECH_FILE_NAME_PATTERN', '%YYYY-MM-DD%_%TECHNOLOGY_NAME%_%SUMMARY_ID%.zip'),
(8, 10, 'SORT_BY_POSITION', 'false'),
(9, 10, 'DPI', '300'),
(10, 10, 'IMG_FORMAT', 'PNG'),
(12, 9, 'LEGEND_BARCODE_PATTERN', 'FOL_{oProductId}'),
(14, 10, 'LEGEND_BARCODE_PATTERN', 'FOL_{oProductId}'),
(15, 9, 'LEGEND_PATTERN', 'O:{orderId}\nFOL_{oProductId}'),
(16, 10, 'LEGEND_PATTERN', 'O:{orderId}\nFOL_{oProductId}'),
(17, 9, 'LEGEND_ENABLE', 'true'),
(18, 10, 'LEGEND_ENABLE', 'true');

INSERT INTO `platform_production_transformer_module_property` (`id`, `transformer_module_id`, `name`, `value`) VALUES
(2, 8, 'PAGE_WIDTH_MM', '420'),
(3, 8, 'PAGE_HEIGHT_MM', '1000'),
(4, 8, 'PAGE_BORDER_MM', '0'),
(5, 8, 'MINIMIZE_PAGE_HEIGHT', 'true'),
(6, 10, 'PAGE_WIDTH_MM', '500'),
(7, 10, 'PAGE_HEIGHT_MM', '2000'),
(8, 10, 'PAGE_BORDER_MM', '0'),
(9, 10, 'MINIMIZE_PAGE_HEIGHT', 'true'),
(10, 10, 'ALLOW_ROTATION_TO_FIT', 'true'),
(11, 10, 'PAGE_SIZE_MODE', 'CUSTOM'),
(12, 10, 'EXCEED_USE', 'AUTO'),
(13, 10, 'ALIGN_VERTICAL', 'TOP'),
(14, 10, 'ALIGN_HORIZONTAL', 'LEFT'),
(15, 9, 'PAGE_WIDTH_MM', '500'),
(16, 9, 'PAGE_HEIGHT_MM', '2000'),
(17, 9, 'PAGE_BORDER_MM', '0'),
(18, 9, 'MINIMIZE_PAGE_HEIGHT', 'true'),
(19, 9, 'ALLOW_ROTATION_TO_FIT', 'true'),
(20, 9, 'PAGE_SIZE_MODE', 'CUSTOM'),
(21, 9, 'EXCEED_USE', 'AUTO'),
(22, 9, 'ALIGN_VERTICAL', 'TOP'),
(23, 9, 'ALIGN_HORIZONTAL', 'LEFT'),
(24, 8, 'ALLOW_ROTATION_TO_FIT', 'true'),
(25, 8, 'PAGE_SIZE_MODE', 'CUSTOM'),
(26, 8, 'EXCEED_USE', 'AUTO'),
(27, 8, 'OBJECT_BORDER_MM', '4'),
(28, 11, 'PAGE_WIDTH_MM', '420'),
(29, 11, 'PAGE_HEIGHT_MM', '1000'),
(30, 11, 'PAGE_BORDER_MM', '0'),
(31, 11, 'MINIMIZE_PAGE_HEIGHT', 'true'),
(32, 11, 'ALLOW_ROTATION_TO_FIT', 'true'),
(33, 11, 'PAGE_SIZE_MODE', 'CUSTOM'),
(34, 11, 'EXCEED_USE', 'AUTO'),
(35, 11, 'OBJECT_BORDER_MM', '4'),
(36, 12, 'PAGE_WIDTH_MM', '420'),
(37, 12, 'PAGE_HEIGHT_MM', '1000'),
(38, 12, 'PAGE_BORDER_MM', '0'),
(39, 12, 'MINIMIZE_PAGE_HEIGHT', 'true'),
(40, 12, 'OBJECT_BORDER_MM', '0'),
(41, 12, 'SPLIT_TEMPLATE', 'true'),
(42, 12, 'SPREAD_CROSS_THE_LINE', 'true'),
(43, 12, 'PAGE_SIZE_MODE', 'CUSTOM'),
(44, 12, 'EXCEED_USE', 'AUTO'),
(45, 11, 'ADVANCED_BOTTOM_SPREAD_ANGLE', ''),
(46, 11, 'ADVANCED_DIVE_HORIZONTAL_FACTOR', '1'),
(47, 11, 'ADVANCED_BEST_ROTATION_STEP', '90'),
(50, 12, 'ALLOW_ROTATION_TO_FIT', 'true'),
(51, 9, 'SPLIT_OVERSIZE', 'IGNORE'),
(52, 8, 'SPLIT_OVERSIZE', 'IGNORE'),
(53, 10, 'SPLIT_OVERSIZE', 'IGNORE'),
(54, 11, 'SPLIT_OVERSIZE', 'IGNORE'),
(55, 12, 'SPLIT_OVERSIZE', 'IGNORE'),
(56, 10, 'THRESHOLD_EXCEED', '{"items":[  {    "thresholdMm":0,    "splitOversize": "IGNORE",    "exceed": "AUTO"  }]}'),
(57, 9, 'THRESHOLD_EXCEED', '{"items":[  {    "thresholdMm":0,    "splitOversize": "IGNORE",    "exceed": "AUTO"  }]}'),
(58, 8, 'THRESHOLD_EXCEED', '{"items":[  {    "thresholdMm":0,    "splitOversize": "IGNORE",    "exceed": "AUTO"  }]}'),
(59, 11, 'THRESHOLD_EXCEED', '{"items":[  {    "thresholdMm":0,    "splitOversize": "IGNORE",    "exceed": "AUTO"  }]}'),
(60, 12, 'THRESHOLD_EXCEED', '{"items":[  {    "thresholdMm":0,    "splitOversize": "IGNORE",    "exceed": "AUTO"  }]}'),
(70, 8, 'EXCEED_STEPS', '{"steps":[]}'),
(71, 9, 'EXCEED_STEPS', '{"steps":[]}'),
(72, 10, 'EXCEED_STEPS', '{"steps":[]}'),
(73, 11, 'EXCEED_STEPS', '{"steps":[]}'),
(74, 12, 'EXCEED_STEPS', '{"steps":[]}'),
(77, 8, 'EXCEED_PAGE', 'AUTO'),
(78, 9, 'EXCEED_PAGE', 'AUTO'),
(79, 10, 'EXCEED_PAGE', 'AUTO'),
(80, 11, 'EXCEED_PAGE', 'AUTO'),
(81, 12, 'EXCEED_PAGE', 'AUTO');


