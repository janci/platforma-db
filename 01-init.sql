-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Hostiteľ: mysql
-- Vygenerované: St 02.Dec 2020, 09:56
-- Verzia serveru: 10.1.45-MariaDB
-- Verzia PHP: 5.6.40

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáza: `platform`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `addon_lock`
--

CREATE TABLE IF NOT EXISTS `addon_lock` (
  `lock_id` varchar(255) NOT NULL DEFAULT '',
  `key_of_lock` varchar(255) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`lock_id`,`key_of_lock`),
  KEY `created` (`created`,`lock_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='provide lock shared between servers';

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `addon_queue_method`
--

CREATE TABLE IF NOT EXISTS `addon_queue_method` (
  `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `app_key` varchar(255) NOT NULL DEFAULT '',
  `app_version` int(11) NOT NULL DEFAULT '0',
  `queue` varchar(255) NOT NULL DEFAULT '',
  `bean_name` varchar(255) NOT NULL DEFAULT '',
  `bean_class` varchar(255) NOT NULL DEFAULT '',
  `method` varchar(255) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `schedule_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `started` timestamp NULL DEFAULT NULL,
  `completed` timestamp NULL DEFAULT NULL,
  `max_attempts` tinyint(3) NOT NULL DEFAULT '2',
  `attempts_left` tinyint(3) NOT NULL DEFAULT '2',
  `delay` int(11) NOT NULL DEFAULT '1000',
  `multiplier` double NOT NULL DEFAULT '0',
  `max_jobs` smallint(6) NOT NULL DEFAULT '0',
  `read_lock_key` varchar(255) NOT NULL DEFAULT '',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `trigger_index` (`schedule_time`),
  KEY `started` (`started`),
  KEY `completed` (`completed`),
  KEY `atempts_left` (`attempts_left`),
  KEY `version` (`version`),
  KEY `key-version` (`app_key`,`app_version`) COMMENT 'foreign key for block execution'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5719204 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `addon_queue_method_param`
--

CREATE TABLE IF NOT EXISTS `addon_queue_method_param` (
  `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `method_id` bigint(20) unsigned NOT NULL,
  `method_type` varchar(100) NOT NULL DEFAULT '' COMMENT 'type of parameter defined in method',
  `type` varchar(100) NOT NULL DEFAULT '',
  `value` text,
  `order_index` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `unique` (`order_index`,`method_id`),
  KEY `method` (`method_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10754632 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_account`
--

CREATE TABLE IF NOT EXISTS `platform_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliate_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `full_name` varchar(255) NOT NULL,
  `company_logo_id` int(11) DEFAULT NULL,
  `core_allowed` tinyint(1) NOT NULL DEFAULT '1',
  `shop_allowed` tinyint(1) NOT NULL DEFAULT '1',
  `publish_allowed` tinyint(1) NOT NULL DEFAULT '0',
  `subpartners_allowed` tinyint(1) NOT NULL DEFAULT '0',
  `pimp_key` varchar(40) DEFAULT NULL,
  `pimp_pass` varchar(20) DEFAULT NULL,
  `pimp_created` timestamp NULL DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `data_retention_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `affiliate_id` (`affiliate_id`),
  KEY `company_logo_id` (`company_logo_id`),
  KEY `deleted` (`deleted`),
  KEY `subpartners_allowed` (`subpartners_allowed`),
  KEY `version` (`version`),
  KEY `data_retention_id` (`data_retention_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=187 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_account_data_retention`
--

CREATE TABLE IF NOT EXISTS `platform_account_data_retention` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keep_personal_data_months` int(3) NOT NULL DEFAULT '6',
  `keep_print_data_months` int(3) NOT NULL DEFAULT '36',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_active_node`
--

CREATE TABLE IF NOT EXISTS `platform_active_node` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node` varchar(255) NOT NULL DEFAULT '',
  `role` varchar(255) NOT NULL DEFAULT '',
  `update_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `node` (`node`),
  KEY `role` (`role`),
  KEY `update_time` (`update_time`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=461 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_address`
--

CREATE TABLE IF NOT EXISTS `platform_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `street` varchar(255) NOT NULL DEFAULT '',
  `street_no` varchar(255) NOT NULL DEFAULT '',
  `address2` varchar(255) NOT NULL DEFAULT '',
  `city` varchar(255) NOT NULL DEFAULT '',
  `company` varchar(255) NOT NULL DEFAULT '',
  `country` varchar(255) NOT NULL DEFAULT '',
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `first_name` varchar(255) NOT NULL DEFAULT '',
  `last_name` varchar(255) NOT NULL DEFAULT '',
  `latitude` decimal(10,7) NOT NULL DEFAULT '0.0000000',
  `longitude` decimal(10,7) NOT NULL DEFAULT '0.0000000',
  `name` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `province` varchar(255) NOT NULL DEFAULT '',
  `province_code` varchar(8) NOT NULL DEFAULT '',
  `zip` varchar(64) NOT NULL DEFAULT '',
  `care_of` varchar(128) NOT NULL DEFAULT '',
  `state_code` varchar(8) NOT NULL DEFAULT '',
  `state_name` varchar(128) NOT NULL DEFAULT '',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `version` (`version`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1382104 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_bounds`
--

CREATE TABLE IF NOT EXISTS `platform_bounds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `x` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `y` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `width` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `height` decimal(9,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1919776 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_brother_preset`
--

CREATE TABLE IF NOT EXISTS `platform_brother_preset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  `pretread_speed` smallint(6) NOT NULL DEFAULT '0',
  `pretread_direction_double` tinyint(1) NOT NULL DEFAULT '0',
  `ink_volume` tinyint(3) NOT NULL DEFAULT '1',
  `color_multiple_pass` tinyint(1) NOT NULL DEFAULT '0',
  `print_bidirectional` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_business_entity`
--

CREATE TABLE IF NOT EXISTS `platform_business_entity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `country_source_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_source_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_carrier_free_delivery_price`
--

CREATE TABLE IF NOT EXISTS `platform_carrier_free_delivery_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carrier_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `version` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `carrier_id_2` (`carrier_id`,`country_id`),
  KEY `carrier_id` (`carrier_id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_carrier_service`
--

CREATE TABLE IF NOT EXISTS `platform_carrier_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `phone_required` tinyint(3) NOT NULL DEFAULT '0',
  `phone_default` varchar(64) NOT NULL,
  `module_id` int(11) NOT NULL DEFAULT '0',
  `provider_id` int(11) DEFAULT NULL,
  `custom_provider` varchar(64) NOT NULL DEFAULT '',
  `version` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(3) NOT NULL DEFAULT '0',
  `logo` mediumblob,
  `active` tinyint(3) NOT NULL DEFAULT '0',
  `free_delivery` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `version` (`version`),
  KEY `deleted` (`deleted`),
  KEY `active` (`active`),
  KEY `module_id` (`module_id`),
  KEY `provider_id` (`provider_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=252 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_carrier_service_connection`
--

CREATE TABLE IF NOT EXISTS `platform_carrier_service_connection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `connector_id` int(11) NOT NULL DEFAULT '0',
  `remote_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `remote_id` (`remote_id`,`connector_id`),
  KEY `connector_id` (`connector_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=56 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_carrier_service_destination`
--

CREATE TABLE IF NOT EXISTS `platform_carrier_service_destination` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `country_id` int(11) NOT NULL DEFAULT '0',
  `carrier_id` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `carrier_id` (`carrier_id`,`country_id`),
  KEY `zone_id` (`zone_id`),
  KEY `country_id` (`country_id`),
  KEY `version` (`version`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=165 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_carrier_service_localized`
--

CREATE TABLE IF NOT EXISTS `platform_carrier_service_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) NOT NULL DEFAULT '0',
  `carrier_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `carrier_id` (`carrier_id`,`lang_id`),
  KEY `lang_id` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=549 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_carrier_service_price`
--

CREATE TABLE IF NOT EXISTS `platform_carrier_service_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `country_id` int(11) NOT NULL DEFAULT '0',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `weight_limit` decimal(10,2) NOT NULL DEFAULT '0.00',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `zone_id_2` (`zone_id`,`country_id`,`weight_limit`),
  KEY `version` (`version`),
  KEY `country_id` (`country_id`),
  KEY `zone_id` (`zone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=879 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_carrier_service_zone`
--

CREATE TABLE IF NOT EXISTS `platform_carrier_service_zone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carrier_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `version` (`version`),
  KEY `carrier_id` (`carrier_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=133 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_collection`
--

CREATE TABLE IF NOT EXISTS `platform_collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `box_color` char(6) NOT NULL COMMENT 'rgb',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `order_index` (`order_index`),
  KEY `shop_id` (`shop_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=315 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_collection_category`
--

CREATE TABLE IF NOT EXISTS `platform_collection_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `parent_category_id` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `shop_id` int(11) NOT NULL,
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `ns_left` int(11) NOT NULL DEFAULT '1',
  `ns_right` int(11) NOT NULL DEFAULT '2',
  `ns_thread` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent_category_id` (`parent_category_id`),
  KEY `active` (`active`),
  KEY `shop_id` (`shop_id`),
  KEY `order_index` (`order_index`),
  KEY `deleted` (`deleted`),
  KEY `ns_thread_2` (`ns_thread`,`ns_right`),
  KEY `ns_thread` (`ns_thread`,`ns_left`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=189 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_collection_category_localized`
--

CREATE TABLE IF NOT EXISTS `platform_collection_category_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `lang_id` (`lang_id`),
  KEY `collection_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=394 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_collection_category_x_product`
--

CREATE TABLE IF NOT EXISTS `platform_collection_category_x_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `product_id` (`product_id`),
  KEY `active` (`active`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4648 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_collection_localized`
--

CREATE TABLE IF NOT EXISTS `platform_collection_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `lang_id` (`lang_id`),
  KEY `collection_id` (`collection_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=788 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_color_texture`
--

CREATE TABLE IF NOT EXISTS `platform_color_texture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `bitmap` mediumblob NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `created` (`created`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1403 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_company_address`
--

CREATE TABLE IF NOT EXISTS `platform_company_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `country_source_id` int(11) NOT NULL,
  `street` varchar(1024) NOT NULL,
  `zip_code` varchar(64) NOT NULL,
  `city` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_source_id`),
  KEY `deleted` (`deleted`),
  KEY `company_id` (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=148 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_company_business`
--

CREATE TABLE IF NOT EXISTS `platform_company_business` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `business_entity_id` int(11) NOT NULL,
  `business_id` varchar(64) NOT NULL,
  `business_vat_id` varchar(64) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `business_entity_id` (`business_entity_id`),
  KEY `deleted` (`deleted`),
  KEY `company_id` (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=148 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_company_contact`
--

CREATE TABLE IF NOT EXISTS `platform_company_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `person` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `company_id` (`account_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=147 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_company_logo`
--

CREATE TABLE IF NOT EXISTS `platform_company_logo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` mediumblob NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=79 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_country`
--

CREATE TABLE IF NOT EXISTS `platform_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` char(2) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `account_id` int(11) NOT NULL DEFAULT '0',
  `currency_id` int(11) NOT NULL DEFAULT '0',
  `vat_standard_id` int(11) NOT NULL DEFAULT '0',
  `vat_reduced_id` int(11) DEFAULT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(3) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  `price_precision` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `price_fractional_digits` tinyint(1) unsigned NOT NULL DEFAULT '2',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`,`account_id`),
  KEY `shop_id` (`account_id`),
  KEY `deleted` (`deleted`),
  KEY `version` (`version`),
  KEY `vat_standard_id` (`vat_standard_id`),
  KEY `vat_reduced_id` (`vat_reduced_id`),
  KEY `currency_id` (`currency_id`),
  KEY `default` (`default`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1593 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_country_source`
--

CREATE TABLE IF NOT EXISTS `platform_country_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=528 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_creator_theme`
--

CREATE TABLE IF NOT EXISTS `platform_creator_theme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `file_name` varchar(255) NOT NULL DEFAULT '',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `background_color` char(6) NOT NULL DEFAULT 'ffffff',
  `mobile` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_currency`
--

CREATE TABLE IF NOT EXISTS `platform_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `code` char(3) NOT NULL DEFAULT '',
  `symbol` varchar(5) NOT NULL DEFAULT '',
  `account_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(3) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `version` (`version`),
  KEY `shop_id` (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1303 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_customer`
--

CREATE TABLE IF NOT EXISTS `platform_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL DEFAULT '',
  `last_name` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(255) NOT NULL DEFAULT '',
  `accepts_marketing` tinyint(3) NOT NULL DEFAULT '0',
  `billing_address_id` int(11) NOT NULL DEFAULT '0',
  `shipping_address_id` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `billing_address_id` (`billing_address_id`),
  KEY `shipping_address_id` (`shipping_address_id`),
  KEY `version` (`version`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=825875 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_design`
--

CREATE TABLE IF NOT EXISTS `platform_design` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  `creator_type` int(11) NOT NULL DEFAULT '0',
  `price` decimal(9,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4574051 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_designed_ordered_product`
--

CREATE TABLE IF NOT EXISTS `platform_designed_ordered_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `design_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `color_id` int(11) NOT NULL DEFAULT '0',
  `size_id` int(11) NOT NULL DEFAULT '0',
  `amount` smallint(3) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `price` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `version` int(11) NOT NULL DEFAULT '0',
  `team_design_collection_id` int(11) DEFAULT NULL,
  `price_calc_report_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `base_id` (`design_id`),
  KEY `color_id` (`color_id`),
  KEY `size_id` (`size_id`),
  KEY `team_design_collection_id` (`team_design_collection_id`),
  KEY `uuid` (`uuid`),
  KEY `price_calc_report_id` (`price_calc_report_id`),
  KEY `created` (`created`),
  KEY `deleted` (`deleted`),
  KEY `version` (`version`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4133174 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_designed_ordered_product_connection`
--

CREATE TABLE IF NOT EXISTS `platform_designed_ordered_product_connection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordered_product_id` int(11) NOT NULL DEFAULT '0',
  `connector_id` int(11) NOT NULL DEFAULT '0',
  `remote_id` bigint(20) NOT NULL DEFAULT '0',
  `type` enum('ORDER_ITEM','BASKET_ITEM') NOT NULL DEFAULT 'ORDER_ITEM',
  `basket_item_used` tinyint(3) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ordered_product_id` (`ordered_product_id`,`connector_id`),
  UNIQUE KEY `remote_id` (`remote_id`,`connector_id`,`type`),
  KEY `connector_id` (`connector_id`),
  KEY `basket_item_used` (`basket_item_used`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15516 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_designed_ordered_product_custom_attributes`
--

CREATE TABLE IF NOT EXISTS `platform_designed_ordered_product_custom_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `ordered_product_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ordered_product_id` (`ordered_product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14527 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_designed_shared_product`
--

CREATE TABLE IF NOT EXISTS `platform_designed_shared_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `design_id` int(11) NOT NULL DEFAULT '0',
  `color_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `engine_version` int(11) NOT NULL DEFAULT '0',
  `shop_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `design_id` (`design_id`),
  KEY `color_id` (`color_id`),
  KEY `deleted` (`deleted`),
  KEY `version` (`version`),
  KEY `shop_id` (`shop_id`),
  KEY `created` (`created`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=849361 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_designed_template_product`
--

CREATE TABLE IF NOT EXISTS `platform_designed_template_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `design_id` int(11) NOT NULL,
  `collection_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `color_id` int(11) NOT NULL DEFAULT '0',
  `size_id` int(11) NOT NULL DEFAULT '0',
  `including_new_colors` tinyint(1) NOT NULL DEFAULT '1',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  `engine_version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `design_id` (`design_id`),
  KEY `color_id` (`color_id`),
  KEY `size_id` (`size_id`),
  KEY `collection_id` (`collection_id`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8931 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_designed_template_product_color`
--

CREATE TABLE IF NOT EXISTS `platform_designed_template_product_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) NOT NULL,
  `assigned_color_id` int(11) NOT NULL,
  `active` tinyint(3) NOT NULL DEFAULT '1',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `template_id` (`template_id`,`assigned_color_id`),
  KEY `template_id_2` (`template_id`),
  KEY `assigned_color_id` (`assigned_color_id`),
  KEY `active` (`active`),
  KEY `version` (`version`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='matrix allows enable only some combination' AUTO_INCREMENT=85343 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_designed_template_product_connection`
--

CREATE TABLE IF NOT EXISTS `platform_designed_template_product_connection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_product_id` int(11) NOT NULL DEFAULT '0',
  `connector_id` int(11) NOT NULL DEFAULT '0',
  `product_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `template_product_id` (`template_product_id`,`connector_id`),
  UNIQUE KEY `product_id` (`product_id`,`connector_id`),
  KEY `connector_id` (`connector_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6231 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_designed_template_product_image_connection`
--

CREATE TABLE IF NOT EXISTS `platform_designed_template_product_image_connection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_product_id` int(11) NOT NULL DEFAULT '0',
  `connector_id` int(11) NOT NULL DEFAULT '0',
  `remote_id` bigint(20) NOT NULL DEFAULT '0',
  `assigned_view_id` int(11) DEFAULT NULL,
  `assigned_color_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `remote_id` (`remote_id`,`connector_id`),
  KEY `connector_id` (`connector_id`),
  KEY `template_product_id` (`template_product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=74794 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_designed_template_product_localized`
--

CREATE TABLE IF NOT EXISTS `platform_designed_template_product_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `template_id` (`template_id`),
  KEY `lang_id` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17717 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_designed_template_product_price`
--

CREATE TABLE IF NOT EXISTS `platform_designed_template_product_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `price` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  KEY `template_id` (`template_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12550 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_designed_template_product_variant_connection`
--

CREATE TABLE IF NOT EXISTS `platform_designed_template_product_variant_connection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_product_id` int(11) NOT NULL DEFAULT '0',
  `assigned_color_id` int(11) NOT NULL DEFAULT '0',
  `assigned_size_id` int(11) NOT NULL DEFAULT '0',
  `remote_id` bigint(20) NOT NULL DEFAULT '0',
  `connector_id` int(11) NOT NULL DEFAULT '0',
  `inventory_management` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `remote_id` (`remote_id`,`connector_id`),
  UNIQUE KEY `template_product_id` (`template_product_id`,`connector_id`,`assigned_color_id`,`assigned_size_id`),
  KEY `assigned_color_id` (`assigned_color_id`),
  KEY `assigned_size_id` (`assigned_size_id`),
  KEY `connector_id` (`connector_id`),
  KEY `inventory_management` (`inventory_management`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=529174 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_designed_template_product_view_disabled`
--

CREATE TABLE IF NOT EXISTS `platform_designed_template_product_view_disabled` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) NOT NULL,
  `assigned_view_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `template_id` (`template_id`,`assigned_view_id`),
  KEY `assigned_view_id` (`assigned_view_id`),
  KEY `template_id_2` (`template_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4633 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_design_composition`
--

CREATE TABLE IF NOT EXISTS `platform_design_composition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `design_id` int(11) NOT NULL DEFAULT '0',
  `original_product_area_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  `num_reduced_colors` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `design_id` (`design_id`),
  KEY `product_area_id` (`original_product_area_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14496860 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_design_element`
--

CREATE TABLE IF NOT EXISTS `platform_design_element` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `element_Id` int(11) NOT NULL DEFAULT '0',
  `element_name` varchar(255) DEFAULT NULL,
  `element_editable` tinyint(3) NOT NULL DEFAULT '1',
  `element_limit_width` tinyint(3) NOT NULL DEFAULT '0',
  `element_limit_height` tinyint(3) NOT NULL DEFAULT '0',
  `composition_id` int(11) NOT NULL,
  `x` smallint(6) NOT NULL DEFAULT '0',
  `y` smallint(6) NOT NULL DEFAULT '0',
  `width` smallint(6) NOT NULL DEFAULT '0',
  `height` smallint(6) NOT NULL DEFAULT '0',
  `rotation` smallint(5) unsigned NOT NULL DEFAULT '0',
  `scale` float NOT NULL DEFAULT '0',
  `layer_index` smallint(6) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `pimp_task_id` bigint(20) unsigned DEFAULT NULL,
  `pimp_image_task_id` int(11) DEFAULT NULL,
  `pimp_task_status` tinyint(3) unsigned DEFAULT NULL,
  `pimp_task_download_status` enum('ERROR','DOWNLOADED') DEFAULT NULL,
  `pimp_price` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `width_mm` smallint(6) unsigned NOT NULL DEFAULT '0',
  `height_mm` smallint(6) unsigned NOT NULL DEFAULT '0',
  `main_acc` tinyint(1) NOT NULL DEFAULT '0',
  `pimp_callback_time` timestamp NULL DEFAULT NULL,
  `pimp_callback_account` int(11) DEFAULT NULL,
  `pimp_processing_error_time` timestamp NULL DEFAULT NULL,
  `transform_bounds_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `composition_id` (`composition_id`),
  KEY `pimp_task_id` (`pimp_task_id`),
  KEY `pimp_image_task_id` (`pimp_image_task_id`),
  KEY `main_acc` (`main_acc`),
  KEY `deleted` (`deleted`),
  KEY `pimp_callback_account` (`pimp_callback_account`),
  KEY `pimp_callback_time` (`pimp_callback_time`),
  KEY `pimp_processing_error_time` (`pimp_processing_error_time`),
  KEY `pimp_task_status` (`pimp_task_status`),
  KEY `transform_bounds_id` (`transform_bounds_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10286561 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_design_element_layer`
--

CREATE TABLE IF NOT EXISTS `platform_design_element_layer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `element_id` int(11) NOT NULL DEFAULT '0',
  `swf_layer_id` int(11) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  `print_technology_id` int(11) NOT NULL DEFAULT '0',
  `assigned_print_color_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `swf_layer_id` (`swf_layer_id`),
  KEY `element_id` (`element_id`),
  KEY `assigned_print_color_id` (`assigned_print_color_id`),
  KEY `print_technology_id` (`print_technology_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10888897 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_design_element_motive`
--

CREATE TABLE IF NOT EXISTS `platform_design_element_motive` (
  `element_id` int(11) NOT NULL,
  `motive_id` int(11) NOT NULL,
  `allow_override_low_quality_scale_restriction` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `element_id` (`element_id`),
  KEY `motive_id` (`motive_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_design_element_pimp_text`
--

CREATE TABLE IF NOT EXISTS `platform_design_element_pimp_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `element_id` int(11) NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  `font_id` int(11) DEFAULT NULL,
  `align` enum('LEFT','CENTER','RIGHT','JUSTIFY') NOT NULL DEFAULT 'LEFT',
  `resource_id` int(11) DEFAULT NULL,
  `resource_width` int(11) NOT NULL DEFAULT '0',
  `resource_height` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `element_id` (`element_id`),
  KEY `resource_id` (`resource_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=384207 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_design_element_qrcode`
--

CREATE TABLE IF NOT EXISTS `platform_design_element_qrcode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `element_id` int(11) NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  `correction_level` enum('L','M','Q','H') NOT NULL DEFAULT 'L' COMMENT 'percentual value',
  `data_type` enum('TEXT','SMS','URL','PHONE') NOT NULL DEFAULT 'TEXT',
  PRIMARY KEY (`id`),
  KEY `element_id` (`element_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4008 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_design_element_svg_text`
--

CREATE TABLE IF NOT EXISTS `platform_design_element_svg_text` (
  `element_id` int(11) NOT NULL DEFAULT '0',
  `fx` tinyint(3) NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  `font_id` int(11) NOT NULL DEFAULT '0',
  `align` enum('LEFT','CENTER','RIGHT') NOT NULL DEFAULT 'CENTER',
  `u` float NOT NULL DEFAULT '0',
  `v` float NOT NULL DEFAULT '0',
  `w` float NOT NULL DEFAULT '0',
  `line_spacing_multiple` float NOT NULL DEFAULT '1',
  `team_text` tinyint(3) NOT NULL DEFAULT '0',
  `svg_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`element_id`),
  KEY `font_id` (`font_id`),
  KEY `svg_id` (`svg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_design_element_text`
--

CREATE TABLE IF NOT EXISTS `platform_design_element_text` (
  `element_id` int(11) NOT NULL,
  `type` tinyint(3) NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  `font_id` int(11) NOT NULL DEFAULT '0',
  `font_size` tinyint(3) NOT NULL DEFAULT '0',
  `align` enum('LEFT','CENTER','RIGHT') NOT NULL DEFAULT 'CENTER',
  `u` float NOT NULL DEFAULT '0',
  `v` float NOT NULL DEFAULT '0',
  `w` float NOT NULL DEFAULT '0',
  `line_spacing_multiple` float NOT NULL DEFAULT '1',
  `team_text` tinyint(3) NOT NULL DEFAULT '0',
  UNIQUE KEY `element_id` (`element_id`),
  KEY `font_id` (`font_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_design_element_text_char`
--

CREATE TABLE IF NOT EXISTS `platform_design_element_text_char` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `element_id` int(11) NOT NULL DEFAULT '0',
  `x` smallint(6) NOT NULL DEFAULT '0',
  `y` smallint(6) NOT NULL DEFAULT '0',
  `width` smallint(6) NOT NULL DEFAULT '0',
  `height` smallint(6) NOT NULL DEFAULT '0',
  `rotation` smallint(5) unsigned NOT NULL DEFAULT '0',
  `order_index` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `element_id` (`element_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1600822 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_design_element_text_line`
--

CREATE TABLE IF NOT EXISTS `platform_design_element_text_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `element_id` int(11) NOT NULL DEFAULT '0',
  `x` smallint(6) NOT NULL DEFAULT '0',
  `y` smallint(6) NOT NULL DEFAULT '0',
  `width` smallint(6) NOT NULL DEFAULT '0',
  `height` smallint(6) NOT NULL DEFAULT '0',
  `order_index` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `element_id` (`element_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4375468 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_design_price_calc_report`
--

CREATE TABLE IF NOT EXISTS `platform_design_price_calc_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1051041 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_factory`
--

CREATE TABLE IF NOT EXISTS `platform_factory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `account_id` int(11) NOT NULL COMMENT 'owner',
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `public` (`public`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=153 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_factory_assigned`
--

CREATE TABLE IF NOT EXISTS `platform_factory_assigned` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `factory_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `constraint` (`factory_id`,`account_id`),
  KEY `production_id` (`factory_id`),
  KEY `active` (`active`),
  KEY `deleted` (`deleted`),
  KEY `order_index` (`order_index`),
  KEY `account_id` (`account_id`),
  KEY `default` (`default`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=490 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_font`
--

CREATE TABLE IF NOT EXISTS `platform_font` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `def_sz` smallint(6) NOT NULL DEFAULT '0',
  `min_sz` smallint(6) NOT NULL DEFAULT '0',
  `max_sz` smallint(6) NOT NULL DEFAULT '0',
  `data_id` int(11) NOT NULL DEFAULT '0',
  `account_id` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `data_id` (`data_id`),
  KEY `enabled` (`enabled`),
  KEY `account_id` (`account_id`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2904 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_font_data`
--

CREATE TABLE IF NOT EXISTS `platform_font_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ttf_file` mediumblob NOT NULL,
  `woff_file` mediumblob,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=309 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_font_public`
--

CREATE TABLE IF NOT EXISTS `platform_font_public` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `class_name` varchar(32) NOT NULL,
  `def_sz` tinyint(3) NOT NULL,
  `min_sz` tinyint(3) NOT NULL,
  `max_sz` tinyint(3) NOT NULL,
  `data_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `library_id` int(11) NOT NULL,
  `order_index` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `class_name` (`class_name`),
  KEY `deleted` (`deleted`),
  KEY `library_id` (`library_id`),
  KEY `data_id` (`data_id`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_font_public_library`
--

CREATE TABLE IF NOT EXISTS `platform_font_public_library` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `order_index` (`order_index`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_fulfillment_service_connection`
--

CREATE TABLE IF NOT EXISTS `platform_fulfillment_service_connection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `connector_id` int(11) NOT NULL DEFAULT '0',
  `remote_id` bigint(20) NOT NULL DEFAULT '0',
  `handle` varchar(255) NOT NULL DEFAULT '',
  `location_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `remote_id` (`remote_id`,`connector_id`),
  KEY `connector_id` (`connector_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_kornit_media_name`
--

CREATE TABLE IF NOT EXISTS `platform_kornit_media_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_kornit_preset`
--

CREATE TABLE IF NOT EXISTS `platform_kornit_preset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_name_id` int(11) NOT NULL,
  `spray_amount` int(1) NOT NULL DEFAULT '-1',
  `print_speed` enum('HI_QUALITY','PRODUCTION','HI_PRODUCTION') NOT NULL DEFAULT 'PRODUCTION',
  `color_print_mode` enum('NONE','NORMAL','INTERLACE') NOT NULL DEFAULT 'INTERLACE',
  `saturation` enum('NORMAL','DARKER','DARKEST') NOT NULL DEFAULT 'NORMAL',
  `white_print_mode` enum('NONE','SINGLE','DOUBLE') NOT NULL DEFAULT 'SINGLE',
  `opacity` int(1) NOT NULL DEFAULT '85',
  `hilight_white` tinyint(1) NOT NULL DEFAULT '1',
  `hilight_opacity` int(1) NOT NULL DEFAULT '85',
  `direction` enum('UNIDIRECTIONAL','BIDIRECTIONAL') NOT NULL DEFAULT 'BIDIRECTIONAL',
  `name` varchar(32) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `media_name_id` (`media_name_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_kornit_setup`
--

CREATE TABLE IF NOT EXISTS `platform_kornit_setup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_name_id` int(11) NOT NULL,
  `spray_amount` int(1) NOT NULL DEFAULT '-1' COMMENT 'percentual value',
  `print_speed` enum('HI_QUALITY','PRODUCTION','HI_PRODUCTION') NOT NULL DEFAULT 'PRODUCTION',
  `color_print_mode` enum('NONE','NORMAL','INTERLACE') NOT NULL DEFAULT 'INTERLACE',
  `saturation` enum('NORMAL','DARKER','DARKEST') NOT NULL DEFAULT 'NORMAL',
  `white_print_mode` enum('NONE','SINGLE','DOUBLE') NOT NULL DEFAULT 'SINGLE',
  `opacity` int(1) NOT NULL DEFAULT '85',
  `hilight_white` tinyint(1) NOT NULL DEFAULT '1',
  `hilight_opacity` int(1) NOT NULL DEFAULT '85',
  `direction` enum('UNIDIRECTIONAL','BIDIRECTIONAL') NOT NULL DEFAULT 'BIDIRECTIONAL',
  PRIMARY KEY (`id`),
  KEY `media_name_id` (`media_name_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_language`
--

CREATE TABLE IF NOT EXISTS `platform_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` char(2) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `account_id` int(11) NOT NULL DEFAULT '0',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`,`account_id`),
  KEY `deleted` (`deleted`),
  KEY `shop_id` (`account_id`),
  KEY `default` (`default`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1725 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_language_source`
--

CREATE TABLE IF NOT EXISTS `platform_language_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=74 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_motive`
--

CREATE TABLE IF NOT EXISTS `platform_motive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `name` varchar(256) NOT NULL DEFAULT '',
  `type` tinyint(3) DEFAULT NULL,
  `price_multiplier` float(4,2) unsigned NOT NULL DEFAULT '1.00',
  `swf_id` int(11) DEFAULT NULL,
  `svg_id` int(11) DEFAULT NULL,
  `bitmap_id` int(11) DEFAULT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0',
  `pool_id` int(11) DEFAULT NULL,
  `designer_id` int(11) DEFAULT NULL,
  `author_provision` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `motive_profit` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `override_price` tinyint(1) NOT NULL DEFAULT '0',
  `exact_price` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  `original_motive_id` int(11) DEFAULT NULL,
  `pimp_task_id` bigint(20) unsigned DEFAULT NULL,
  `pimp_image_task_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `bitmap_id` (`bitmap_id`),
  KEY `swf_id` (`swf_id`),
  KEY `shop_id` (`shop_id`),
  KEY `active` (`active`),
  KEY `designer_id` (`designer_id`),
  KEY `original_motive_id` (`original_motive_id`),
  KEY `pool_id` (`pool_id`),
  KEY `pimp_image_task_id` (`pimp_image_task_id`),
  KEY `type` (`type`),
  KEY `order_index` (`order_index`),
  KEY `type_2` (`type`,`shop_id`,`deleted`),
  KEY `uuid` (`uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4592195 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_motive_category`
--

CREATE TABLE IF NOT EXISTS `platform_motive_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_category_id` int(11) DEFAULT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `shop_id` int(11) NOT NULL,
  `order_index` int(11) NOT NULL DEFAULT '0' COMMENT 'sort order',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active/removed',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  `ns_left` int(11) NOT NULL DEFAULT '1',
  `ns_right` int(11) NOT NULL DEFAULT '2',
  `ns_thread` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `active` (`active`,`deleted`),
  KEY `parent_category_id` (`parent_category_id`),
  KEY `order_index` (`order_index`),
  KEY `ns_thread` (`ns_thread`,`ns_left`),
  KEY `ns_thread_2` (`ns_thread`,`ns_right`),
  KEY `shop_id` (`shop_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=320522 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_motive_category_localized`
--

CREATE TABLE IF NOT EXISTS `platform_motive_category_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `motive_category_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `motive_category_id` (`motive_category_id`,`lang_id`),
  KEY `lang_id` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=78884 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_motive_data_bitmap`
--

CREATE TABLE IF NOT EXISTS `platform_motive_data_bitmap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `mask_id` int(11) DEFAULT NULL,
  `preview_resource_name` varchar(255) NOT NULL DEFAULT '',
  `production_resource_name` varchar(255) NOT NULL DEFAULT '',
  `original_name` varchar(255) NOT NULL DEFAULT '',
  `original_file_size` int(11) NOT NULL DEFAULT '0' COMMENT 'size of the original file',
  `width` int(11) NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0',
  `production_width` int(11) NOT NULL DEFAULT '0',
  `original_width` int(11) NOT NULL DEFAULT '0',
  `production_height` int(11) NOT NULL DEFAULT '0',
  `original_height` int(11) NOT NULL DEFAULT '0',
  `original_size` int(11) NOT NULL DEFAULT '0',
  `advanced_setup_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  `original_resource_name` varchar(255) NOT NULL DEFAULT '',
  `blocked` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'can be blocked by licence',
  `source_status` enum('IN_USE','BACKUP','LOCKED','BACKUP_LOCKED','DELETED','ERROR') NOT NULL DEFAULT 'IN_USE' COMMENT 'IN_USE - source is still using , LOCKED - source cant be deleted (ie.  used in template product ) , DELETED - source deleted',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `advanced_setup_id` (`advanced_setup_id`),
  KEY `account_id` (`account_id`),
  KEY `deleted` (`deleted`),
  KEY `blocked` (`blocked`),
  KEY `source_status` (`source_status`),
  KEY `created` (`created`),
  KEY `preview_resource_name` (`preview_resource_name`),
  KEY `production_resource_name` (`production_resource_name`),
  KEY `mask_id` (`mask_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4426169 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_motive_data_bitmap_setup`
--

CREATE TABLE IF NOT EXISTS `platform_motive_data_bitmap_setup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clean_resource_name` varchar(255) NOT NULL,
  `preview_resource_name` varchar(255) NOT NULL,
  `enabled_reduce_colors` tinyint(1) NOT NULL DEFAULT '0',
  `reduced_colors` blob,
  `dither` tinyint(3) NOT NULL DEFAULT '0',
  `color_method` tinyint(3) NOT NULL DEFAULT '0',
  `color_amount` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1859 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_motive_data_layer`
--

CREATE TABLE IF NOT EXISTS `platform_motive_data_layer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `motive_id` int(11) NOT NULL DEFAULT '0',
  `layer_index` tinyint(3) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL,
  `default_color` char(6) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `motive_id` (`motive_id`),
  KEY `layer_index` (`layer_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=412217 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_motive_data_layer_technology`
--

CREATE TABLE IF NOT EXISTS `platform_motive_data_layer_technology` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layer_id` int(11) NOT NULL,
  `technology_id` int(11) NOT NULL,
  `default_color_id` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `technology_id` (`technology_id`,`layer_id`),
  KEY `default_color_id` (`default_color_id`),
  KEY `layer_id` (`layer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_motive_data_svg`
--

CREATE TABLE IF NOT EXISTS `platform_motive_data_svg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` mediumtext NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1972070 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_motive_data_swf`
--

CREATE TABLE IF NOT EXISTS `platform_motive_data_swf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie` mediumblob NOT NULL,
  `name` varchar(256) NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `blocked` (`blocked`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11011 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_motive_designer`
--

CREATE TABLE IF NOT EXISTS `platform_motive_designer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_motive_disable_country`
--

CREATE TABLE IF NOT EXISTS `platform_motive_disable_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `motive_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `country_id` (`country_id`,`motive_id`),
  KEY `motive_id` (`motive_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26692 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_motive_localized`
--

CREATE TABLE IF NOT EXISTS `platform_motive_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `motive_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `motive_id` (`motive_id`,`lang_id`),
  KEY `lang_id` (`lang_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1239348 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_motive_pool`
--

CREATE TABLE IF NOT EXISTS `platform_motive_pool` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `deleted_2` (`deleted`),
  KEY `shop_id` (`shop_id`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1445 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_motive_pool_property`
--

CREATE TABLE IF NOT EXISTS `platform_motive_pool_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pool_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `value` varchar(1024) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pool_id_2` (`pool_id`,`name`),
  KEY `pool_id` (`pool_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=791 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_motive_print_technology`
--

CREATE TABLE IF NOT EXISTS `platform_motive_print_technology` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `print_technology_id` int(11) NOT NULL,
  `motive_id` int(11) NOT NULL,
  `min_width` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT 'zero means no limit',
  `min_height` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT 'zero means no limit',
  `max_width` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT 'zero means no limit',
  `max_height` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT 'zero means no limit',
  `allow_scale` tinyint(1) NOT NULL DEFAULT '1',
  `keep_proportions` tinyint(1) NOT NULL DEFAULT '1',
  `allow_rotation` tinyint(1) NOT NULL DEFAULT '1',
  `only_90_degrees_rotation` tinyint(1) NOT NULL DEFAULT '0',
  `allow_mirroring` tinyint(1) NOT NULL DEFAULT '1',
  `allow_color_change` tinyint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `reduce_colors` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `print_technology_id_3` (`print_technology_id`,`motive_id`),
  KEY `print_technology_id_2` (`print_technology_id`),
  KEY `motive_id` (`motive_id`),
  KEY `active` (`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26851 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_motive_x_motive_category`
--

CREATE TABLE IF NOT EXISTS `platform_motive_x_motive_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `motive_category_id` int(11) NOT NULL DEFAULT '0',
  `motive_id` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active/hidden',
  `order_index` int(11) NOT NULL DEFAULT '0' COMMENT 'sort order for each category',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `motive_category_id` (`motive_category_id`,`motive_id`),
  KEY `active` (`active`),
  KEY `motive_id` (`motive_id`),
  KEY `version` (`version`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=860244 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_motive_x_tag`
--

CREATE TABLE IF NOT EXISTS `platform_motive_x_tag` (
  `motive_id` int(11) NOT NULL DEFAULT '0',
  `tag_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`motive_id`,`tag_id`),
  KEY `tag_id` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_order`
--

CREATE TABLE IF NOT EXISTS `platform_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(64) DEFAULT NULL,
  `session_id` char(36) DEFAULT NULL,
  `basket_id` binary(16) DEFAULT NULL,
  `shop_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `customer_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ready_to_produce` timestamp NULL DEFAULT NULL,
  `paid` timestamp NULL DEFAULT NULL,
  `financial_status` enum('PENDING','AUTHORIZED','PARTIALLY_PAID','PAID','PARTIALLY_REFUNDED','REFUNDED','VOIDED') DEFAULT NULL,
  `fulfilment_status` enum('WAITING_FOR_RESOURCES','PREPARED_TO_PRODUCE','NOT_EDITABLE','FINISHED','RECLAMATION','CANCELLED','ERROR_HQ_MOTIVES') DEFAULT NULL,
  `pimp_status` enum('ERROR','CONFIRMED','NOTHING_TO_DO') DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `connection_id` int(11) DEFAULT NULL,
  `browser_ip` varchar(15) DEFAULT '',
  `payment_method` varchar(255) DEFAULT NULL,
  `shop_address_id` int(11) DEFAULT NULL,
  `data_erasure_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id` (`session_id`,`shop_id`),
  UNIQUE KEY `unique_id` (`unique_id`,`shop_id`),
  UNIQUE KEY `basket_id` (`basket_id`),
  KEY `shop_id` (`shop_id`),
  KEY `financial_status` (`financial_status`),
  KEY `fulfilment_status` (`fulfilment_status`),
  KEY `version` (`version`),
  KEY `deleted` (`deleted`),
  KEY `country_id` (`country_id`),
  KEY `pimp_status` (`pimp_status`),
  KEY `ready_to_produce` (`ready_to_produce`),
  KEY `created` (`created`),
  KEY `connection_id` (`connection_id`),
  KEY `customer_id` (`customer_id`),
  KEY `shop_address` (`shop_address_id`),
  KEY `data_erasure_id` (`data_erasure_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4089737 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_ordered_manual_service`
--

CREATE TABLE IF NOT EXISTS `platform_ordered_manual_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(3) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `price` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `amount` int(11) NOT NULL DEFAULT '0',
  `pimp_output_action_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `version` (`version`),
  KEY `deleted` (`deleted`),
  KEY `order_id` (`order_id`),
  KEY `pimp_output_action_id` (`pimp_output_action_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35920 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_order_connection`
--

CREATE TABLE IF NOT EXISTS `platform_order_connection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `connector_id` int(11) NOT NULL DEFAULT '0',
  `order_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_id` (`order_id`,`connector_id`),
  KEY `connector_id` (`connector_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12358 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_order_data_erasure`
--

CREATE TABLE IF NOT EXISTS `platform_order_data_erasure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personal_data_erase_date` date DEFAULT NULL,
  `personal_data_erased` tinyint(3) NOT NULL DEFAULT '0',
  `print_data_erase_date` date DEFAULT NULL,
  `print_data_erased` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `personal_data_erase_date` (`personal_data_erase_date`),
  KEY `personal_data_erased` (`personal_data_erased`),
  KEY `print_data_erase_date` (`print_data_erase_date`),
  KEY `print_data_erased` (`print_data_erased`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_order_deferred`
--

CREATE TABLE IF NOT EXISTS `platform_order_deferred` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `order_data` mediumtext NOT NULL,
  `header` tinyint(3) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `processed` tinyint(3) NOT NULL DEFAULT '0',
  `processed_date` timestamp NULL DEFAULT NULL,
  `processing_start` timestamp NULL DEFAULT NULL,
  `commited` int(11) NOT NULL DEFAULT '0',
  `commited_date` timestamp NULL DEFAULT NULL,
  `prod` tinyint(3) NOT NULL DEFAULT '0',
  `prod_date` timestamp NULL DEFAULT NULL,
  `error` tinyint(3) NOT NULL DEFAULT '0',
  `error_date` timestamp NULL DEFAULT NULL,
  `error_message` text,
  PRIMARY KEY (`id`),
  KEY `created` (`created`),
  KEY `processing_start` (`processing_start`),
  KEY `order_id` (`order_id`),
  KEY `processed` (`processed`),
  KEY `error` (`error`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=96065 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_order_deferred_item`
--

CREATE TABLE IF NOT EXISTS `platform_order_deferred_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deferred_order_id` int(11) NOT NULL DEFAULT '0',
  `item_index` int(11) NOT NULL DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `deferred_order_id` (`deferred_order_id`),
  KEY `item_index` (`item_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=95522 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_order_fulfillment`
--

CREATE TABLE IF NOT EXISTS `platform_order_fulfillment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `carrier_id` int(11) DEFAULT NULL,
  `tracking_number` varchar(255) NOT NULL DEFAULT '',
  `tracking_url` varchar(255) NOT NULL DEFAULT '',
  `version` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `version` (`version`),
  KEY `order_id` (`order_id`),
  KEY `carrier_id` (`carrier_id`),
  KEY `tracking_number` (`tracking_number`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=598734 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_order_fulfillment_connection`
--

CREATE TABLE IF NOT EXISTS `platform_order_fulfillment_connection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fulfillment_id` int(11) NOT NULL DEFAULT '0',
  `connector_id` int(11) NOT NULL DEFAULT '0',
  `remote_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fulfillment_id` (`fulfillment_id`,`connector_id`),
  UNIQUE KEY `remote_id` (`remote_id`,`connector_id`),
  KEY `connector_id` (`connector_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7147 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_order_fulfillment_item`
--

CREATE TABLE IF NOT EXISTS `platform_order_fulfillment_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_fulfillment_id` int(11) NOT NULL DEFAULT '0',
  `order_item_id` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `order_fulfillment_id` (`order_fulfillment_id`),
  KEY `order_item_id` (`order_item_id`),
  KEY `version` (`version`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1530193 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_order_import_csv`
--

CREATE TABLE IF NOT EXISTS `platform_order_import_csv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shopId` int(11) NOT NULL DEFAULT '0',
  `fileName` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `processed` tinyint(3) NOT NULL DEFAULT '0',
  `processedDate` datetime DEFAULT NULL,
  `dataId` int(11) NOT NULL DEFAULT '0',
  `error` tinyint(3) NOT NULL DEFAULT '0',
  `errorMessage` text,
  PRIMARY KEY (`id`),
  KEY `dataId` (`dataId`),
  KEY `processed` (`processed`),
  KEY `error` (`error`),
  KEY `shopId` (`shopId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_order_import_csv_data`
--

CREATE TABLE IF NOT EXISTS `platform_order_import_csv_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` longtext NOT NULL,
  `separator` char(1) NOT NULL DEFAULT '',
  `quote` char(1) NOT NULL DEFAULT '',
  `ignoreFirstLine` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_order_import_csv_line`
--

CREATE TABLE IF NOT EXISTS `platform_order_import_csv_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `csvId` int(11) NOT NULL DEFAULT '0',
  `lineIndex` int(11) NOT NULL DEFAULT '0',
  `orderDeferredId` int(11) DEFAULT NULL,
  `error` tinyint(3) NOT NULL DEFAULT '0',
  `errorMessage` text,
  PRIMARY KEY (`id`),
  KEY `csvId` (`csvId`),
  KEY `error` (`error`),
  KEY `orderDeferredId` (`orderDeferredId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1101 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_order_shipping`
--

CREATE TABLE IF NOT EXISTS `platform_order_shipping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `price` decimal(9,2) NOT NULL DEFAULT '0.00',
  `cod_price` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_value` decimal(9,2) NOT NULL DEFAULT '0.00',
  `prefered_delivery_time` timestamp NULL DEFAULT NULL,
  `branch_id` varchar(128) NOT NULL DEFAULT '',
  `version` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `carrier_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `version` (`version`),
  KEY `carrier_id` (`carrier_id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=708976 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_overview_config`
--

CREATE TABLE IF NOT EXISTS `platform_overview_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `tech_colors` varchar(1024) NOT NULL,
  `main_views` varchar(1024) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_id` (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_photo_character`
--

CREATE TABLE IF NOT EXISTS `platform_photo_character` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `account_id` int(11) NOT NULL,
  `height` smallint(2) NOT NULL DEFAULT '0',
  `weight` smallint(2) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_photo_image`
--

CREATE TABLE IF NOT EXISTS `platform_photo_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` mediumblob,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `created` (`created`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1317 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_pimp_action_group`
--

CREATE TABLE IF NOT EXISTS `platform_pimp_action_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `action_type_id` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `version` int(11) NOT NULL DEFAULT '0',
  `base_price` decimal(9,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_id` (`account_id`,`action_type_id`),
  KEY `enabled` (`enabled`),
  KEY `account_id_2` (`account_id`),
  KEY `action_type_id` (`action_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=263 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_pimp_action_group_price`
--

CREATE TABLE IF NOT EXISTS `platform_pimp_action_group_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pimp_action_group_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `price` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pimp_action_group_id_2` (`pimp_action_group_id`,`country_id`),
  KEY `pimp_action_group_id` (`pimp_action_group_id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=601 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_pimp_output_action`
--

CREATE TABLE IF NOT EXISTS `platform_pimp_output_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_id` int(11) NOT NULL DEFAULT '0',
  `action_group_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `action_group_id` (`action_group_id`),
  KEY `task_id` (`task_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=492746 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_pimp_output_bitmap_font_task`
--

CREATE TABLE IF NOT EXISTS `platform_pimp_output_bitmap_font_task` (
  `id` int(11) NOT NULL DEFAULT '0',
  `action_group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `action_group_id` (`action_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_pimp_output_image_task`
--

CREATE TABLE IF NOT EXISTS `platform_pimp_output_image_task` (
  `id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_pimp_output_task`
--

CREATE TABLE IF NOT EXISTS `platform_pimp_output_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` bigint(20) NOT NULL DEFAULT '0',
  `task_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=756346 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_color`
--

CREATE TABLE IF NOT EXISTS `platform_print_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `palette_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `rgb` char(6) DEFAULT NULL COMMENT 'color value RRGGBB',
  `rgb_print` char(6) NOT NULL DEFAULT '' COMMENT 'value RRGGBB',
  `cmyk_print` varchar(15) NOT NULL DEFAULT '',
  `bitmap_id` int(11) DEFAULT NULL,
  `icon_id` int(11) DEFAULT NULL,
  `preview_id` int(11) DEFAULT NULL,
  `texture_width` smallint(6) NOT NULL DEFAULT '0',
  `texture_height` smallint(6) NOT NULL DEFAULT '0',
  `texture_mode` enum('TILED','FILL','STRETCH') NOT NULL DEFAULT 'FILL' COMMENT 'STRETCH scale texture to fill exact boundaries of the motive (deform textures aspect ratio). Fill scales texture with same horizontal and vertical scale to match motive boundaries',
  `icon_mode` enum('CROP','STRETCH') NOT NULL DEFAULT 'CROP',
  `overlay_id` int(11) DEFAULT NULL,
  `overlay_blending_mode` enum('NORMAL','LUMINOSITY','OVERLAY') NOT NULL DEFAULT 'NORMAL',
  `overlay_alpha` tinyint(4) NOT NULL DEFAULT '100',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `bitmap_id` (`bitmap_id`),
  KEY `palette_id` (`palette_id`),
  KEY `order_index` (`order_index`),
  KEY `icon_id` (`icon_id`),
  KEY `preview_id` (`preview_id`),
  KEY `overlay_id` (`overlay_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32583 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_color_localized`
--

CREATE TABLE IF NOT EXISTS `platform_print_color_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `print_color_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `print_color_id` (`print_color_id`,`lang_id`),
  KEY `lang_id` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=170785 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_color_palette`
--

CREATE TABLE IF NOT EXISTS `platform_print_color_palette` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `type` enum('LIST','WHEEL') NOT NULL DEFAULT 'LIST',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `static` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `order_index` (`order_index`),
  KEY `deleted` (`deleted`),
  KEY `type` (`type`),
  KEY `default` (`default`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2376 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_media_size`
--

CREATE TABLE IF NOT EXISTS `platform_print_media_size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `palette_id` int(11) NOT NULL,
  `width` decimal(9,2) unsigned NOT NULL DEFAULT '0.00',
  `length` decimal(9,2) unsigned NOT NULL DEFAULT '0.00',
  `display_unit` enum('mm','cm','inches') NOT NULL DEFAULT 'mm',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`palette_id`),
  KEY `deleted` (`deleted`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2153 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_media_size_localized`
--

CREATE TABLE IF NOT EXISTS `platform_print_media_size_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_size_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `media_size_id` (`media_size_id`,`lang_id`),
  KEY `lang_id` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11284 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_media_size_palette`
--

CREATE TABLE IF NOT EXISTS `platform_print_media_size_palette` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `account_id` int(11) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `static` tinyint(1) NOT NULL DEFAULT '0',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `deleted` (`deleted`),
  KEY `default` (`default`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1022 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_profile`
--

CREATE TABLE IF NOT EXISTS `platform_print_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `path` varchar(255) NOT NULL,
  `account_id` int(11) NOT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `public` (`public`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT 'Unknown',
  `type` enum('OTHER','FLEX_FLOCK','DIGITAL','SUBLIMATION','LASER','ENGRAVING','SCREENPRINT','DIGITAL_DIRECT') NOT NULL DEFAULT 'OTHER',
  `account_id` int(11) NOT NULL,
  `motive_collection_id` int(11) NOT NULL,
  `qrcode_id` int(11) NOT NULL,
  `text_id` int(11) NOT NULL,
  `upload_id` int(11) NOT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `motive_price_formula` varchar(2048) NOT NULL DEFAULT '',
  `price_formula` varchar(2048) NOT NULL,
  `barcode_naming` varchar(1024) NOT NULL DEFAULT '',
  `file_module_id` int(11) NOT NULL,
  `display_module_id` int(11) NOT NULL,
  `transformer_module_id` int(11) NOT NULL,
  `response_module_id` int(11) NOT NULL,
  `vector_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `bitmap_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `preview_drop_shadow_id` int(11) DEFAULT NULL,
  `preview_blend_mode_dark_id` int(11) DEFAULT NULL,
  `preview_blend_mode_white_id` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `motive_collection_id` (`motive_collection_id`),
  UNIQUE KEY `qrcode_id` (`qrcode_id`),
  UNIQUE KEY `text_id` (`text_id`),
  UNIQUE KEY `upload_id` (`upload_id`),
  UNIQUE KEY `profile_id` (`profile_id`),
  KEY `account_id` (`account_id`),
  KEY `active` (`active`),
  KEY `deleted` (`deleted`),
  KEY `order_index` (`order_index`),
  KEY `filter_module_id` (`file_module_id`),
  KEY `display_module_id` (`display_module_id`),
  KEY `transformer_module_id` (`transformer_module_id`),
  KEY `response_module_id` (`response_module_id`),
  KEY `type` (`type`),
  KEY `preview_drop_shadow_id` (`preview_drop_shadow_id`),
  KEY `preview_blend_mode_white_id` (`preview_blend_mode_white_id`),
  KEY `preview_blend_mode_dark_id` (`preview_blend_mode_dark_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1100 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_assigned_color`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_assigned_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `print_color_id` int(11) NOT NULL,
  `print_technology_id` int(11) NOT NULL,
  `print_color_group_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `text_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `qrcode_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `motive_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `print_color_id` (`print_color_id`),
  KEY `print_color_group_id` (`print_color_group_id`),
  KEY `deleted` (`deleted`),
  KEY `motive_enabled` (`motive_enabled`),
  KEY `qrcode_enabled` (`qrcode_enabled`),
  KEY `text_enabled` (`text_enabled`),
  KEY `active` (`active`),
  KEY `print_technology_id` (`print_technology_id`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45732 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_assigned_media`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_assigned_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `print_technology_id` int(11) NOT NULL,
  `print_media_size_id` int(11) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `default` (`default`),
  KEY `deleted` (`deleted`),
  KEY `technology_id` (`print_technology_id`),
  KEY `print_media_size_id` (`print_media_size_id`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3664 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_color_group`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_color_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `print_technology_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `print_technology_id` (`print_technology_id`),
  KEY `deleted` (`deleted`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3907 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_color_group_localized`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_color_group_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang_id_2` (`lang_id`,`group_id`),
  KEY `lang_id` (`lang_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7525 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_input_motive_collection`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_input_motive_collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `limit` tinyint(4) NOT NULL,
  `min_width` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '-1 means no limit',
  `min_height` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '-1 means no limit',
  `max_width` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '-1 means no limit',
  `max_height` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '-1 means no limit',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `active` (`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1084 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_input_preset`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_input_preset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `account_id` int(11) NOT NULL,
  `motive_collection_id` int(11) NOT NULL,
  `text_id` int(11) NOT NULL,
  `qrcode_id` int(11) NOT NULL,
  `upload_id` int(11) NOT NULL,
  `order_index` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `motive_collection_id` (`motive_collection_id`),
  KEY `text_id` (`text_id`),
  KEY `qrcode_id` (`qrcode_id`),
  KEY `upload_id` (`upload_id`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_input_qrcode`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_input_qrcode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `generate_background` tinyint(1) NOT NULL DEFAULT '1',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1081 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_input_text`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_input_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `font_size_limit` smallint(6) NOT NULL DEFAULT '64',
  `min_width` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT 'min width of text',
  `min_height` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT 'min height of text',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1081 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_input_upload`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_input_upload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `tranparency_enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'PNG,TIFF,GIF,TGA,..',
  `vector_enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'AI,PDF,EPS,SWF',
  `base_images_enabled` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'jpg,bmp',
  `version` int(11) NOT NULL DEFAULT '0',
  `dpi_threshold_level_low` int(11) NOT NULL DEFAULT '100',
  `dpi_threshold_level_high` int(11) NOT NULL DEFAULT '300',
  `match_print_quality` tinyint(1) NOT NULL DEFAULT '1',
  `match_print_quality_level` int(11) NOT NULL DEFAULT '300',
  `show_low_quality_warning` tinyint(1) NOT NULL DEFAULT '1',
  `allow_override_low_quality_restriction` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `active` (`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1081 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_localized`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) NOT NULL,
  `technology_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang_id_2` (`lang_id`,`technology_id`),
  KEY `lang_id` (`lang_id`),
  KEY `technology_id` (`technology_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6945 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_preview_blend_mode`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_preview_blend_mode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mode` enum('MULTIPLY','SCREEN','NORMAL') NOT NULL DEFAULT 'NORMAL',
  `opacity` tinyint(3) unsigned NOT NULL DEFAULT '100',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=168 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_preview_drop_shadow`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_preview_drop_shadow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `distance` tinyint(2) NOT NULL DEFAULT '4',
  `angle` smallint(3) NOT NULL DEFAULT '0',
  `color` char(6) NOT NULL DEFAULT '000000',
  `opacity` tinyint(3) unsigned NOT NULL DEFAULT '100',
  `size` tinyint(2) unsigned NOT NULL DEFAULT '4',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_preview_preset`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_preview_preset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `blend_mode_id` int(11) DEFAULT NULL,
  `drop_shadow_id` int(11) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `blend_mode_id` (`blend_mode_id`),
  KEY `drop_shadow_id` (`drop_shadow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_price_bitmap`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_price_bitmap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `print_technology_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `surface_step` smallint(5) NOT NULL DEFAULT '0' COMMENT 'top level of surface interval which defining where the price belongs to. 0 = infinity',
  `product_amount` int(11) NOT NULL DEFAULT '0' COMMENT 'top level of product amount interval which defining where the price belongs to. 0 = infinity',
  `color_amount` tinyint(3) NOT NULL DEFAULT '0' COMMENT 'defines top level interval of color amount where price belongs to. 0=infinity',
  `white_only` tinyint(1) NOT NULL DEFAULT '0',
  `price` decimal(9,4) NOT NULL DEFAULT '0.0000' COMMENT 'define how much costs defined flat. 0 is for free',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`print_technology_id`,`country_id`,`surface_step`,`product_amount`,`color_amount`,`white_only`),
  KEY `print_technology_id` (`print_technology_id`),
  KEY `country_id` (`country_id`),
  KEY `surface_step` (`surface_step`),
  KEY `product_amount` (`product_amount`),
  KEY `color_amount` (`color_amount`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1936 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_price_color`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_price_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assigned_print_color_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `flat_steps` smallint(5) NOT NULL DEFAULT '0' COMMENT 'top level of surface interval which defining where the price belongs to. 0 = infinity',
  `product_amount` int(11) NOT NULL DEFAULT '0' COMMENT 'top level of product amount interval which defining where the price belongs to. 0 = infinity',
  `white_only` tinyint(1) NOT NULL DEFAULT '0',
  `price` decimal(9,4) NOT NULL DEFAULT '0.0000' COMMENT 'define how much costs defined flat. 0 is for free',
  `stitch_price` decimal(9,4) DEFAULT NULL COMMENT 'if defined ,has biggest priority. Its pure stitch pricing',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`country_id`,`flat_steps`,`product_amount`,`white_only`,`assigned_print_color_id`),
  KEY `country_id` (`country_id`),
  KEY `flat_steps` (`flat_steps`),
  KEY `product_amount` (`product_amount`),
  KEY `assigned_print_color_id` (`assigned_print_color_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36080 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_price_const`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_price_const` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `print_technology_id` int(11) NOT NULL,
  `deleted` tinyint(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `print_technology_id` (`print_technology_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_price_const_value`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_price_const_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `const_id` int(11) NOT NULL,
  `product_amount` int(11) NOT NULL,
  `value` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `const_id_2` (`const_id`,`product_amount`),
  KEY `const_id` (`const_id`),
  KEY `product_amount` (`product_amount`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_price_labor_cost`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_price_labor_cost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `print_technology_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `general` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `each_view` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `each_object` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `each_color` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `print_technology_id_2` (`print_technology_id`,`country_id`),
  KEY `country_id` (`country_id`),
  KEY `print_technology_id` (`print_technology_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5106 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_price_setup_cost`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_price_setup_cost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `print_technology_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `general` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `each_view` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `each_object` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `each_color` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `print_technology_id_2` (`print_technology_id`,`country_id`),
  KEY `country_id` (`country_id`),
  KEY `print_technology_id` (`print_technology_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5106 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_price_text`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_price_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `print_technology_id` int(11) NOT NULL,
  `font_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `white_only` tinyint(1) NOT NULL DEFAULT '0',
  `usage_price` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `character_treshold` tinyint(4) NOT NULL,
  `character_price` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `print_technology_id_2` (`print_technology_id`,`font_id`,`country_id`,`white_only`),
  KEY `print_technology_id` (`print_technology_id`),
  KEY `font_id` (`font_id`),
  KEY `country_id` (`country_id`),
  KEY `active` (`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=84485 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_print_technology_profile`
--

CREATE TABLE IF NOT EXISTS `platform_print_technology_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `advanced` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Advanced color profiling. ',
  `white_profile_id` int(11) DEFAULT NULL,
  `colour_profile_id` int(11) DEFAULT NULL,
  `intent` enum('ABSOLUTE','PERCEPTUAL','RELATIVE','SATURATION') NOT NULL DEFAULT 'RELATIVE',
  `black_point_compensation` tinyint(1) NOT NULL DEFAULT '1',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `enabled` (`enabled`),
  KEY `white_profile` (`white_profile_id`),
  KEY `colour_profile` (`colour_profile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=590 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product`
--

CREATE TABLE IF NOT EXISTS `platform_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_id` int(11) DEFAULT NULL,
  `art_nr` varchar(128) NOT NULL DEFAULT '' COMMENT 'articel number',
  `type_id` int(11) NOT NULL,
  `model` varchar(128) NOT NULL DEFAULT '' COMMENT 'model name',
  `name` varchar(64) NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0',
  `pool_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `child_version` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `shop_id` (`shop_id`),
  KEY `type_id` (`type_id`),
  KEY `order_index` (`order_index`),
  KEY `source_id` (`source_id`),
  KEY `pool_id` (`pool_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=30595 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_production_display_addon_property`
--

CREATE TABLE IF NOT EXISTS `platform_production_display_addon_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `print_technology_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` varchar(128) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `display_id_2` (`print_technology_id`,`name`),
  KEY `display_id` (`print_technology_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1961 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_production_display_module`
--

CREATE TABLE IF NOT EXISTS `platform_production_display_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_production_display_module_property`
--

CREATE TABLE IF NOT EXISTS `platform_production_display_module_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `display_module_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `display_addon_id` (`display_module_id`,`name`),
  KEY `display_addon_id_2` (`display_module_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_production_file_addon_property`
--

CREATE TABLE IF NOT EXISTS `platform_production_file_addon_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `print_technology_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` varchar(128) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `filter_id_2` (`print_technology_id`,`name`),
  KEY `filter_id` (`print_technology_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6173 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_production_file_module`
--

CREATE TABLE IF NOT EXISTS `platform_production_file_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_production_file_module_property`
--

CREATE TABLE IF NOT EXISTS `platform_production_file_module_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_module_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `filter_addon_id` (`file_module_id`,`name`),
  KEY `filter_addon_id_2` (`file_module_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_production_overview`
--

CREATE TABLE IF NOT EXISTS `platform_production_overview` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_production_preset`
--

CREATE TABLE IF NOT EXISTS `platform_production_preset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `module_file_id` int(11) NOT NULL,
  `module_display_id` int(11) NOT NULL,
  `module_transformer_id` int(11) NOT NULL,
  `module_response_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `order_index` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `module_file_id` (`module_file_id`),
  KEY `module_display_id` (`module_display_id`),
  KEY `module_transformer_id` (`module_transformer_id`),
  KEY `module_response_id` (`module_response_id`),
  KEY `account_id` (`account_id`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_production_preset_display_property`
--

CREATE TABLE IF NOT EXISTS `platform_production_preset_display_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `preset_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` varchar(128) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `preset_id_2` (`preset_id`,`name`),
  KEY `preset_id` (`preset_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_production_preset_file_property`
--

CREATE TABLE IF NOT EXISTS `platform_production_preset_file_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `preset_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` varchar(128) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `preset_id_2` (`preset_id`,`name`),
  KEY `preset_id` (`preset_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_production_preset_response_property`
--

CREATE TABLE IF NOT EXISTS `platform_production_preset_response_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `preset_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` varchar(128) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `preset_id_2` (`preset_id`,`name`),
  KEY `preset_id` (`preset_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_production_preset_transformer_property`
--

CREATE TABLE IF NOT EXISTS `platform_production_preset_transformer_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `preset_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` varchar(128) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `preset_id_2` (`preset_id`,`name`),
  KEY `preset_id` (`preset_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_production_response_addon_property`
--

CREATE TABLE IF NOT EXISTS `platform_production_response_addon_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `print_technology_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` varchar(128) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `response_id_2` (`print_technology_id`,`name`),
  KEY `response_id` (`print_technology_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2070 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_production_response_module`
--

CREATE TABLE IF NOT EXISTS `platform_production_response_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_production_response_module_property`
--

CREATE TABLE IF NOT EXISTS `platform_production_response_module_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `response_module_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `response_addon_id` (`response_module_id`,`name`),
  KEY `response_addon_id_2` (`response_module_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_production_transformer_addon_property`
--

CREATE TABLE IF NOT EXISTS `platform_production_transformer_addon_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `print_technology_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` varchar(1024) NOT NULL DEFAULT '',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `transformer_id_2` (`print_technology_id`,`name`),
  KEY `transformer_id` (`print_technology_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10672 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_production_transformer_module`
--

CREATE TABLE IF NOT EXISTS `platform_production_transformer_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_production_transformer_module_property`
--

CREATE TABLE IF NOT EXISTS `platform_production_transformer_module_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transformer_module_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` varchar(1024) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `transformer_addon_id` (`transformer_module_id`,`name`),
  KEY `transformer_addon_id_2` (`transformer_module_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=84 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_area`
--

CREATE TABLE IF NOT EXISTS `platform_product_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `assigned_view_id` int(11) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `bounds_x` smallint(6) NOT NULL DEFAULT '0',
  `bounds_y` smallint(6) NOT NULL DEFAULT '0',
  `bounds_width` smallint(6) NOT NULL DEFAULT '0',
  `bounds_height` smallint(6) NOT NULL DEFAULT '0',
  `base_width` smallint(6) NOT NULL DEFAULT '0',
  `base_height` smallint(6) NOT NULL DEFAULT '0',
  `boundary_geom_id` int(11) NOT NULL DEFAULT '0',
  `mask_geom_id` int(11) DEFAULT NULL,
  `hot_spot_x` smallint(6) NOT NULL DEFAULT '0',
  `hot_spot_y` smallint(6) NOT NULL DEFAULT '0',
  `hot_spot_size` smallint(6) NOT NULL DEFAULT '0',
  `hcl_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `vcl_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `lockAspectRatio` tinyint(1) NOT NULL DEFAULT '1',
  `fillScaling` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `boundary_geom_id` (`boundary_geom_id`),
  KEY `mask_geom_id` (`mask_geom_id`),
  KEY `assigned_view_id` (`assigned_view_id`),
  KEY `enabled` (`enabled`),
  KEY `version` (`version`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=84447 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_area_geom`
--

CREATE TABLE IF NOT EXISTS `platform_product_area_geom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dummy` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17830 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_area_geom_elipse`
--

CREATE TABLE IF NOT EXISTS `platform_product_area_geom_elipse` (
  `geom_id` int(11) NOT NULL DEFAULT '0',
  `x` mediumint(8) NOT NULL DEFAULT '0',
  `y` mediumint(8) NOT NULL DEFAULT '0',
  `a` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `b` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`geom_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_area_geom_rect`
--

CREATE TABLE IF NOT EXISTS `platform_product_area_geom_rect` (
  `geom_id` int(11) NOT NULL,
  `x` mediumint(8) NOT NULL,
  `y` mediumint(8) NOT NULL,
  `width` mediumint(8) unsigned NOT NULL,
  `height` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`geom_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_area_geom_spline`
--

CREATE TABLE IF NOT EXISTS `platform_product_area_geom_spline` (
  `geom_id` int(11) NOT NULL,
  `x` mediumint(8) NOT NULL,
  `y` mediumint(8) NOT NULL,
  `width` mediumint(8) unsigned NOT NULL,
  `height` mediumint(8) unsigned NOT NULL,
  `svg_path` text NOT NULL,
  PRIMARY KEY (`geom_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_area_real_size`
--

CREATE TABLE IF NOT EXISTS `platform_product_area_real_size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_area_id` int(11) NOT NULL,
  `assigned_size_id` int(11) NOT NULL DEFAULT '0',
  `width` float(9,2) NOT NULL DEFAULT '0.00',
  `height` float(9,2) NOT NULL DEFAULT '0.00',
  `orig_width` float(9,2) NOT NULL DEFAULT '0.00',
  `orig_height` float(9,2) NOT NULL DEFAULT '0.00',
  `version` int(11) NOT NULL DEFAULT '1',
  `deleted` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_area_id` (`product_area_id`,`assigned_size_id`),
  KEY `assigned_size_id` (`assigned_size_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=355521 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_area_snap`
--

CREATE TABLE IF NOT EXISTS `platform_product_area_snap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area_id` int(11) NOT NULL,
  `scale_type` enum('EXACT_SCALE','REDUCE_ONLY','ENLARGE_ONLY') NOT NULL DEFAULT 'EXACT_SCALE',
  `x` smallint(6) NOT NULL DEFAULT '0',
  `y` smallint(6) NOT NULL DEFAULT '0',
  `size` smallint(6) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `area_id` (`area_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1181 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_assigned_attribute_list_item`
--

CREATE TABLE IF NOT EXISTS `platform_product_assigned_attribute_list_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `attribute_list_item_id` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_id_2` (`product_id`,`attribute_list_item_id`),
  KEY `product_id` (`product_id`),
  KEY `attrribute_list_item_id` (`attribute_list_item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=122304 ;

--
-- Spúšťače `platform_product_assigned_attribute_list_item`
--
DROP TRIGGER IF EXISTS `insert_product_assigned_attribute_list_item`;
DELIMITER //
CREATE TRIGGER `insert_product_assigned_attribute_list_item` AFTER INSERT ON `platform_product_assigned_attribute_list_item`
 FOR EACH ROW BEGIN
         update platform_product p  
          set p.child_version = p.child_version + 1 
          where
             NEW.product_id = p.id 
            and p.deleted = 0;
     END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `update_product_assigned_attribute_list_item`;
DELIMITER //
CREATE TRIGGER `update_product_assigned_attribute_list_item` AFTER UPDATE ON `platform_product_assigned_attribute_list_item`
 FOR EACH ROW BEGIN
         update platform_product p 
          set p.child_version = p.child_version + 1 
          where
            NEW.product_id = p.id 
            and p.deleted = 0;
     END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_assigned_color`
--

CREATE TABLE IF NOT EXISTS `platform_product_assigned_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `order_index` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(3) NOT NULL DEFAULT '1',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `color_id` (`color_id`),
  KEY `order_index` (`order_index`),
  KEY `deleted` (`deleted`),
  KEY `product_id` (`product_id`),
  KEY `default` (`default`),
  KEY `active` (`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=176571 ;

--
-- Spúšťače `platform_product_assigned_color`
--
DROP TRIGGER IF EXISTS `insert_product_assigned_color`;
DELIMITER //
CREATE TRIGGER `insert_product_assigned_color` AFTER INSERT ON `platform_product_assigned_color`
 FOR EACH ROW BEGIN
         update platform_product p  
          set p.child_version = p.child_version + 1 
          where
             NEW.product_id = p.id 
            and p.deleted = 0;
     END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `update_product_assigned_color`;
DELIMITER //
CREATE TRIGGER `update_product_assigned_color` AFTER UPDATE ON `platform_product_assigned_color`
 FOR EACH ROW BEGIN
         update platform_product p 
          set p.child_version = p.child_version + 1 
          where
            NEW.product_id = p.id 
            and p.deleted = 0;
     END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_assigned_factory`
--

CREATE TABLE IF NOT EXISTS `platform_product_assigned_factory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `factory_assigned_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`,`factory_assigned_id`),
  KEY `product_id` (`product_id`),
  KEY `factory_assigned_id` (`factory_assigned_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51592 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_assigned_print_technology`
--

CREATE TABLE IF NOT EXISTS `platform_product_assigned_print_technology` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_type_print_technology_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_index` int(11) NOT NULL DEFAULT '0',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `print_profile_id` (`product_type_print_technology_id`),
  KEY `order_index` (`order_index`),
  KEY `deleted` (`deleted`),
  KEY `default` (`default`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=213673 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_assigned_print_technology_brother`
--

CREATE TABLE IF NOT EXISTS `platform_product_assigned_print_technology_brother` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `product_print_technology_id` int(11) NOT NULL DEFAULT '0',
  `assigned_color_id` int(11) NOT NULL DEFAULT '0',
  `assigned_view_id` int(11) NOT NULL DEFAULT '0',
  `assigned_size_id` int(11) NOT NULL DEFAULT '0',
  `type` enum('GT3','GTX') NOT NULL,
  `pretread_speed` smallint(6) NOT NULL DEFAULT '0',
  `pretread_direction_double` tinyint(1) NOT NULL DEFAULT '0',
  `ink_volume` tinyint(3) NOT NULL DEFAULT '1',
  `color_multiple_pass` tinyint(1) NOT NULL DEFAULT '0',
  `print_bidirectional` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `product_print_technology_id` (`product_print_technology_id`),
  KEY `assigned_color_id` (`assigned_color_id`),
  KEY `assigned_view_id` (`assigned_view_id`),
  KEY `assigned_size_id` (`assigned_size_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_assigned_print_technology_color`
--

CREATE TABLE IF NOT EXISTS `platform_product_assigned_print_technology_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `white_color` tinyint(1) NOT NULL DEFAULT '0',
  `product_print_technology_id` int(11) NOT NULL,
  `color_value_id` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_print_technology_id` (`product_print_technology_id`,`color_value_id`),
  KEY `product_type_print_technology_id` (`product_print_technology_id`),
  KEY `assigned_color_view_id` (`color_value_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23651 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_assigned_print_technology_general`
--

CREATE TABLE IF NOT EXISTS `platform_product_assigned_print_technology_general` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_print_technology_id` int(11) NOT NULL,
  `assigned_view_id` int(11) NOT NULL,
  `assigned_color_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_print_technology_id_2` (`product_print_technology_id`,`assigned_view_id`,`assigned_color_id`),
  KEY `assigned_view_id` (`assigned_view_id`),
  KEY `assigned_color_id` (`assigned_color_id`),
  KEY `active` (`active`),
  KEY `product_print_technology_id` (`product_print_technology_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=60276054 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_assigned_print_technology_kornit`
--

CREATE TABLE IF NOT EXISTS `platform_product_assigned_print_technology_kornit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_print_technology_id` int(11) NOT NULL,
  `assigned_color_id` int(11) NOT NULL,
  `assigned_view_id` int(11) NOT NULL,
  `assigned_size_id` int(11) NOT NULL,
  `media_name_id` int(11) NOT NULL,
  `spray_amount` int(1) NOT NULL DEFAULT '-1',
  `print_speed` enum('HI_QUALITY','PRODUCTION','HI_PRODUCTION') NOT NULL DEFAULT 'PRODUCTION',
  `color_print_mode` enum('NONE','NORMAL','INTERLACE') NOT NULL DEFAULT 'INTERLACE',
  `saturation` enum('NORMAL','DARKER','DARKEST') NOT NULL DEFAULT 'NORMAL',
  `white_print_mode` enum('NONE','SINGLE','DOUBLE') NOT NULL DEFAULT 'SINGLE',
  `opacity` int(1) NOT NULL DEFAULT '85',
  `hilight_white` tinyint(1) NOT NULL DEFAULT '1',
  `hilight_opacity` int(1) NOT NULL DEFAULT '85',
  `direction` enum('UNIDIRECTIONAL','BIDIRECTIONAL') NOT NULL DEFAULT 'BIDIRECTIONAL',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assigned_print_technology_id` (`product_print_technology_id`,`assigned_size_id`),
  KEY `assigned_print_profile_id` (`product_print_technology_id`),
  KEY `assigned_size_id` (`assigned_size_id`),
  KEY `media_name_id` (`media_name_id`),
  KEY `assigned_color_id` (`assigned_color_id`),
  KEY `assigned_view_id` (`assigned_view_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_assigned_print_technology_media`
--

CREATE TABLE IF NOT EXISTS `platform_product_assigned_print_technology_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_print_technology_id` int(11) NOT NULL,
  `assigned_view_id` int(11) NOT NULL,
  `assigned_size_id` int(11) NOT NULL,
  `downgradable` tinyint(1) NOT NULL DEFAULT '0',
  `assigned_media_size_id` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_print_technology_id_2` (`product_print_technology_id`,`assigned_view_id`,`assigned_size_id`),
  KEY `assigned_view_id` (`assigned_view_id`),
  KEY `assigned_size_id` (`assigned_size_id`),
  KEY `media_size_id` (`assigned_media_size_id`),
  KEY `product_print_technology_id` (`product_print_technology_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=146959 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_assigned_size`
--

CREATE TABLE IF NOT EXISTS `platform_product_assigned_size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `width` float(9,2) NOT NULL DEFAULT '0.00' COMMENT 'product width in mm',
  `height` float(9,2) NOT NULL DEFAULT '0.00' COMMENT 'product height in mm',
  `length` float(9,2) NOT NULL DEFAULT '0.00' COMMENT 'product length in mm',
  `accuracy` decimal(3,2) NOT NULL DEFAULT '0.00',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `size_id` (`size_id`),
  KEY `order_index` (`order_index`),
  KEY `deleted` (`deleted`),
  KEY `product_id` (`product_id`),
  KEY `default` (`default`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=96092 ;

--
-- Spúšťače `platform_product_assigned_size`
--
DROP TRIGGER IF EXISTS `insert_product_assigned_size`;
DELIMITER //
CREATE TRIGGER `insert_product_assigned_size` AFTER INSERT ON `platform_product_assigned_size`
 FOR EACH ROW BEGIN
         update platform_product p  
          set p.child_version = p.child_version + 1 
          where
             NEW.product_id = p.id 
            and p.deleted = 0;
     END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `update_product_assigned_size`;
DELIMITER //
CREATE TRIGGER `update_product_assigned_size` AFTER UPDATE ON `platform_product_assigned_size`
 FOR EACH ROW BEGIN
         update platform_product p 
          set p.child_version = p.child_version + 1 
          where
            NEW.product_id = p.id 
            and p.deleted = 0;
     END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_assigned_sticker`
--

CREATE TABLE IF NOT EXISTS `platform_product_assigned_sticker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assigned_view_id` int(11) NOT NULL,
  `sticker_id` int(11) NOT NULL,
  `on_all_colors` tinyint(1) NOT NULL DEFAULT '1',
  `above_motives` tinyint(1) NOT NULL DEFAULT '1',
  `creator_only` tinyint(1) NOT NULL DEFAULT '0',
  `display_until` timestamp NULL DEFAULT NULL,
  `x` smallint(6) NOT NULL,
  `y` smallint(6) NOT NULL,
  `width` smallint(6) NOT NULL,
  `height` smallint(6) NOT NULL,
  `base_width` smallint(6) NOT NULL,
  `base_height` smallint(6) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assigned_view_id` (`assigned_view_id`),
  KEY `sticker_id` (`sticker_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=897 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_assigned_view`
--

CREATE TABLE IF NOT EXISTS `platform_product_assigned_view` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `view_id` int(11) NOT NULL,
  `order_index` int(11) NOT NULL DEFAULT '0',
  `swf_id` int(11) DEFAULT NULL,
  `type` enum('TECHNICAL','HUMAN') NOT NULL DEFAULT 'TECHNICAL',
  `template_id` int(11) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `order_index` (`order_index`),
  KEY `product_id` (`product_id`),
  KEY `view_id` (`view_id`),
  KEY `deleted` (`deleted`),
  KEY `swf_id` (`swf_id`),
  KEY `default` (`default`),
  KEY `type` (`type`),
  KEY `template_id` (`template_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=82923 ;

--
-- Spúšťače `platform_product_assigned_view`
--
DROP TRIGGER IF EXISTS `insert_product_assigned_view`;
DELIMITER //
CREATE TRIGGER `insert_product_assigned_view` AFTER INSERT ON `platform_product_assigned_view`
 FOR EACH ROW BEGIN
         update platform_product p  
          set p.child_version = p.child_version + 1 
          where
             NEW.product_id = p.id 
            and p.deleted = 0;
     END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `update_product_assigned_view`;
DELIMITER //
CREATE TRIGGER `update_product_assigned_view` AFTER UPDATE ON `platform_product_assigned_view`
 FOR EACH ROW BEGIN
         update platform_product p 
          set p.child_version = p.child_version + 1 
          where
            NEW.product_id = p.id 
            and p.deleted = 0;
     END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_assigned_view_color`
--

CREATE TABLE IF NOT EXISTS `platform_product_assigned_view_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assigned_view_id` int(11) NOT NULL DEFAULT '0',
  `assigned_color_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) DEFAULT NULL,
  `layer1_color` char(6) NOT NULL DEFAULT '000000',
  `layer2_color` char(6) NOT NULL DEFAULT '000000',
  `shadows1_alpha` tinyint(3) NOT NULL DEFAULT '50',
  `shadows2_alpha` tinyint(3) NOT NULL DEFAULT '50',
  `tex_alpha` tinyint(4) NOT NULL DEFAULT '100',
  `tex2_alpha` tinyint(3) NOT NULL DEFAULT '0',
  `default_color_value_id` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assigned_view_id` (`assigned_view_id`,`assigned_color_id`),
  KEY `image_id` (`image_id`),
  KEY `assigned_color_id` (`assigned_color_id`),
  KEY `default_color_value_id` (`default_color_value_id`),
  KEY `assigned_view_id_2` (`assigned_view_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=599738 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_assigned_view_snap`
--

CREATE TABLE IF NOT EXISTS `platform_product_assigned_view_snap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assigned_view_id` int(11) NOT NULL,
  `snap_view_id` int(11) NOT NULL,
  `bounds_x` smallint(6) NOT NULL,
  `bounds_y` smallint(6) NOT NULL,
  `base_width` smallint(6) NOT NULL,
  `base_height` smallint(6) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assigned_view_id` (`assigned_view_id`),
  KEY `snap_view_id` (`snap_view_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=39 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_category`
--

CREATE TABLE IF NOT EXISTS `platform_product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `parent_category_id` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  `shop_id` int(11) NOT NULL,
  `ns_left` int(11) NOT NULL DEFAULT '1',
  `ns_right` int(11) NOT NULL DEFAULT '2',
  `ns_thread` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `order_index` (`order_index`),
  KEY `version` (`version`),
  KEY `shop_id` (`shop_id`),
  KEY `parent_category_id` (`parent_category_id`),
  KEY `active` (`active`),
  KEY `ns_thread` (`ns_thread`,`ns_left`),
  KEY `ns_thread_2` (`ns_thread`,`ns_right`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16123 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_category_localized`
--

CREATE TABLE IF NOT EXISTS `platform_product_category_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_category_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_category_id` (`product_category_id`,`lang_id`),
  KEY `lang_id` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=121579 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_category_margin`
--

CREATE TABLE IF NOT EXISTS `platform_product_category_margin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `margin` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `country_id_2` (`country_id`,`category_id`),
  KEY `country_id` (`country_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=460 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_colision`
--

CREATE TABLE IF NOT EXISTS `platform_product_colision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assigned_view_id` int(11) NOT NULL,
  `bounds_x` smallint(6) NOT NULL,
  `bounds_y` smallint(6) NOT NULL,
  `bounds_width` smallint(6) NOT NULL,
  `bounds_height` smallint(6) NOT NULL,
  `base_width` smallint(6) NOT NULL,
  `base_height` smallint(6) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assigned_view_id` (`assigned_view_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3171 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_color`
--

CREATE TABLE IF NOT EXISTS `platform_product_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  `pool_id` int(11) NOT NULL,
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `pool_id` (`pool_id`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=96311 ;

--
-- Spúšťače `platform_product_color`
--
DROP TRIGGER IF EXISTS `update_product_color`;
DELIMITER //
CREATE TRIGGER `update_product_color` AFTER UPDATE ON `platform_product_color`
 FOR EACH ROW BEGIN
         update platform_product p , 
          platform_product_assigned_color ac
          set p.child_version = p.child_version + 1 
          where
            ac.product_id = p.id 
            and ac.deleted = 0
            and p.deleted = 0
            and ac.color_id= NEW.id;
     END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_color_localized`
--

CREATE TABLE IF NOT EXISTS `platform_product_color_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_color_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_color_id` (`product_color_id`,`lang_id`),
  KEY `lang_id` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=238368 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_color_pool`
--

CREATE TABLE IF NOT EXISTS `platform_product_color_pool` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `product_type_id` int(11) NOT NULL,
  `static` tinyint(1) NOT NULL DEFAULT '0',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `default` (`default`),
  KEY `product_type_id` (`product_type_id`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2863 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_color_value`
--

CREATE TABLE IF NOT EXISTS `platform_product_color_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_color_id` int(11) NOT NULL,
  `color_value` char(6) NOT NULL,
  `color_index` int(11) NOT NULL DEFAULT '0',
  `texture_id` int(11) DEFAULT NULL,
  `reference_id` int(11) DEFAULT NULL,
  `tiled` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `product_color_id` (`product_color_id`),
  KEY `texture_id` (`texture_id`),
  KEY `reference_id` (`reference_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36151 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_connection`
--

CREATE TABLE IF NOT EXISTS `platform_product_connection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `connector_id` int(11) NOT NULL DEFAULT '0',
  `remote_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_id` (`product_id`,`connector_id`),
  UNIQUE KEY `remote_id` (`remote_id`,`connector_id`),
  KEY `connector_id` (`connector_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=94 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_group`
--

CREATE TABLE IF NOT EXISTS `platform_product_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_group_attribute`
--

CREATE TABLE IF NOT EXISTS `platform_product_group_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  `account_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_group_attribute_localized`
--

CREATE TABLE IF NOT EXISTS `platform_product_group_attribute_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `attribute_id` (`attribute_id`,`lang_id`),
  KEY `lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_image`
--

CREATE TABLE IF NOT EXISTS `platform_product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `small_path` varchar(255) NOT NULL COMMENT '400px x 400px',
  `original_path` varchar(255) NOT NULL,
  `status` enum('IN_USE','ERROR','DELETED') NOT NULL DEFAULT 'IN_USE',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `created` (`created`),
  KEY `status` (`status`),
  KEY `original_path` (`original_path`),
  KEY `small_path` (`small_path`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50091 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_image_connection`
--

CREATE TABLE IF NOT EXISTS `platform_product_image_connection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `connector_id` int(11) NOT NULL DEFAULT '0',
  `remote_id` bigint(20) NOT NULL DEFAULT '0',
  `assigned_view_id` int(11) DEFAULT NULL,
  `assigned_color_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `remote_id` (`remote_id`,`connector_id`),
  KEY `connector_id` (`connector_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9783 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_localized`
--

CREATE TABLE IF NOT EXISTS `platform_product_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `short_description` varchar(1024) NOT NULL DEFAULT '',
  `description` varchar(20480) NOT NULL DEFAULT '',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_id_2` (`product_id`,`lang_id`),
  KEY `lang_id` (`lang_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=259769 ;

--
-- Spúšťače `platform_product_localized`
--
DROP TRIGGER IF EXISTS `update_product_localized`;
DELIMITER //
CREATE TRIGGER `update_product_localized` AFTER UPDATE ON `platform_product_localized`
 FOR EACH ROW BEGIN
         update platform_product p 
          set p.child_version = p.child_version + 1 
          where
            NEW.product_id = p.id 
            and p.deleted = 0;
     END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_photo_character`
--

CREATE TABLE IF NOT EXISTS `platform_product_photo_character` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `character_id` int(11) NOT NULL,
  `assigned_size_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `filename` varchar(32) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `order_index` int(5) NOT NULL,
  `version` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `character_id` (`character_id`),
  KEY `size_id` (`assigned_size_id`),
  KEY `image_id` (`image_id`),
  KEY `order_index` (`order_index`),
  KEY `product_id` (`product_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_photo_detail`
--

CREATE TABLE IF NOT EXISTS `platform_product_photo_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `assigned_color_id` int(11) NOT NULL,
  `product_photo` tinyint(1) NOT NULL DEFAULT '0',
  `detail_stripe` tinyint(1) NOT NULL DEFAULT '0',
  `filename` varchar(32) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `order_index` int(4) NOT NULL DEFAULT '0' COMMENT 'also order in stripe i guess',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `order_index` (`order_index`),
  KEY `image_id` (`image_id`),
  KEY `assigned_color_id` (`assigned_color_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11662 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_photo_measure`
--

CREATE TABLE IF NOT EXISTS `platform_product_photo_measure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `filename` varchar(32) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `order_index` (`order_index`),
  KEY `image_id` (`image_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18977 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_pool`
--

CREATE TABLE IF NOT EXISTS `platform_product_pool` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `deleted_2` (`deleted`),
  KEY `shop_id` (`shop_id`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1331 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_pool_property`
--

CREATE TABLE IF NOT EXISTS `platform_product_pool_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pool_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `value` varchar(1024) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pool_id_2` (`pool_id`,`name`),
  KEY `pool_id` (`pool_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2552 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_price`
--

CREATE TABLE IF NOT EXISTS `platform_product_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `country_id` int(11) NOT NULL DEFAULT '0',
  `color_id` int(11) DEFAULT NULL,
  `size_id` int(11) DEFAULT NULL,
  `amount` int(11) NOT NULL DEFAULT '0',
  `price` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `margin` decimal(9,4) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_id` (`product_id`,`country_id`,`color_id`,`size_id`,`amount`),
  KEY `version` (`version`),
  KEY `country_id` (`country_id`),
  KEY `color_id` (`color_id`),
  KEY `size_id` (`size_id`),
  KEY `amount` (`amount`),
  KEY `product_id_3` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=179834 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_size`
--

CREATE TABLE IF NOT EXISTS `platform_product_size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  `pool_id` int(11) NOT NULL,
  `dimensions` tinyint(2) NOT NULL DEFAULT '2',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `pool_id` (`pool_id`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5214 ;

--
-- Spúšťače `platform_product_size`
--
DROP TRIGGER IF EXISTS `update_product_size`;
DELIMITER //
CREATE TRIGGER `update_product_size` AFTER UPDATE ON `platform_product_size`
 FOR EACH ROW BEGIN
         update platform_product p , 
          platform_product_assigned_size ac
          set p.child_version = p.child_version + 1 
          where
            ac.product_id = p.id 
            and ac.deleted = 0
            and p.deleted = 0
            and ac.size_id= NEW.id;
     END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_size_localized`
--

CREATE TABLE IF NOT EXISTS `platform_product_size_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `size_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `alt_name` varchar(64) NOT NULL,
  `width` varchar(32) NOT NULL DEFAULT 'width',
  `height` varchar(32) NOT NULL DEFAULT 'height',
  `length` varchar(32) NOT NULL DEFAULT 'length',
  `units` varchar(10) NOT NULL DEFAULT 'cm',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `size_id` (`size_id`,`lang_id`),
  KEY `lang_id` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31636 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_size_pool`
--

CREATE TABLE IF NOT EXISTS `platform_product_size_pool` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `product_type_id` int(11) NOT NULL,
  `static` tinyint(1) NOT NULL DEFAULT '0',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `default` (`default`),
  KEY `product_type_id` (`product_type_id`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=834 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_sku`
--

CREATE TABLE IF NOT EXISTS `platform_product_sku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `assigned_color_id` int(11) NOT NULL DEFAULT '0',
  `assigned_size_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `stock_id` int(11) DEFAULT NULL,
  `availability` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0 - FALSE , 1 - TRUE , 2 - AUTO',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assigned_color_id_2` (`assigned_color_id`,`assigned_size_id`),
  KEY `assigned_color_id` (`assigned_color_id`),
  KEY `assigned_size_id` (`assigned_size_id`),
  KEY `deleted` (`deleted`),
  KEY `product_id` (`product_id`),
  KEY `stock_id` (`stock_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1385381 ;

--
-- Spúšťače `platform_product_sku`
--
DROP TRIGGER IF EXISTS `insert_product_sku`;
DELIMITER //
CREATE TRIGGER `insert_product_sku` AFTER INSERT ON `platform_product_sku`
 FOR EACH ROW BEGIN
         update platform_product p ,
         platform_product_assigned_color ac 
          set p.child_version = p.child_version + 1 
          where
            ac.product_id = p.id
            and NEW.assigned_color_id = ac.id 
            and ac.deleted = 0
            and p.deleted = 0;
     END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `update_product_sku`;
DELIMITER //
CREATE TRIGGER `update_product_sku` AFTER UPDATE ON `platform_product_sku`
 FOR EACH ROW BEGIN
         update platform_product p ,
         platform_product_assigned_color ac 
          set p.child_version = p.child_version + 1 
          where
            ac.product_id = p.id
            and NEW.assigned_color_id = ac.id 
            and ac.deleted = 0
            and p.deleted = 0;
     END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_swf`
--

CREATE TABLE IF NOT EXISTS `platform_product_swf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `swf` longblob,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `created` (`created`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1878 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_to_template`
--

CREATE TABLE IF NOT EXISTS `platform_product_to_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `assigned_print_technology_id` int(11) NOT NULL,
  `show_boundaries` tinyint(1) NOT NULL DEFAULT '1',
  `ignore_clip` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `area_id` (`area_id`),
  KEY `template_id` (`template_id`),
  KEY `assigned_print_technology_id` (`assigned_print_technology_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4867 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_type`
--

CREATE TABLE IF NOT EXISTS `platform_product_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(1024) NOT NULL DEFAULT '',
  `account_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `shop_id` (`account_id`),
  KEY `deleted` (`deleted`),
  KEY `version` (`version`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=538 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_type_assigned_print_technology`
--

CREATE TABLE IF NOT EXISTS `platform_product_type_assigned_print_technology` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_type_id` int(11) NOT NULL,
  `print_technology_id` int(11) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `print_profile_id` (`print_technology_id`),
  KEY `deleted` (`deleted`),
  KEY `default` (`default`),
  KEY `product_type_id` (`product_type_id`),
  KEY `active` (`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1421 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_type_attribute_flag`
--

CREATE TABLE IF NOT EXISTS `platform_product_type_attribute_flag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `order_index` (`order_index`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_type_attribute_list`
--

CREATE TABLE IF NOT EXISTS `platform_product_type_attribute_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `product_type_id` int(11) NOT NULL,
  `is_contain_picture` tinyint(1) NOT NULL DEFAULT '0',
  `contain_url` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `product_type_id` (`product_type_id`),
  KEY `is_contain_picture` (`is_contain_picture`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1225 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_type_attribute_list_item`
--

CREATE TABLE IF NOT EXISTS `platform_product_type_attribute_list_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `list_id` (`list_id`),
  KEY `deleted` (`deleted`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16609 ;

--
-- Spúšťače `platform_product_type_attribute_list_item`
--
DROP TRIGGER IF EXISTS `update_product_type_attribute_list_item`;
DELIMITER //
CREATE TRIGGER `update_product_type_attribute_list_item` AFTER UPDATE ON `platform_product_type_attribute_list_item`
 FOR EACH ROW BEGIN
         update platform_product p , 
          platform_product_assigned_attribute_list_item ac
          set p.child_version = p.child_version + 1 
          where
            ac.product_id = p.id 
            and p.deleted = 0
            and ac.attribute_list_item_id= NEW.id;
     END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_type_attribute_list_item_localized`
--

CREATE TABLE IF NOT EXISTS `platform_product_type_attribute_list_item_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `url` varchar(1024) NOT NULL,
  `picture_id` int(11) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  KEY `lang_id` (`lang_id`),
  KEY `picture_id` (`picture_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=145009 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_type_attribute_list_item_picture`
--

CREATE TABLE IF NOT EXISTS `platform_product_type_attribute_list_item_picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` mediumblob NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `created` (`created`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_type_attribute_list_localized`
--

CREATE TABLE IF NOT EXISTS `platform_product_type_attribute_list_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `item_name` varchar(32) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `list_id` (`list_id`),
  KEY `lang_id` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8152 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_type_attribute_list_x_flag`
--

CREATE TABLE IF NOT EXISTS `platform_product_type_attribute_list_x_flag` (
  `attribute_list_id` int(11) NOT NULL,
  `attribute_flag_id` int(11) NOT NULL,
  PRIMARY KEY (`attribute_list_id`,`attribute_flag_id`),
  KEY `attribute_flag_id` (`attribute_flag_id`),
  KEY `attribute_list_id` (`attribute_list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_type_localized`
--

CREATE TABLE IF NOT EXISTS `platform_product_type_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `lang_id` (`lang_id`),
  KEY `type_id` (`type_id`,`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2161 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_variant_connection`
--

CREATE TABLE IF NOT EXISTS `platform_product_variant_connection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `assigned_color_id` int(11) NOT NULL DEFAULT '0',
  `assigned_size_id` int(11) NOT NULL DEFAULT '0',
  `remote_id` bigint(20) NOT NULL DEFAULT '0',
  `connector_id` int(11) NOT NULL DEFAULT '0',
  `inventory_management` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `remote_id` (`remote_id`,`connector_id`),
  UNIQUE KEY `product_id` (`product_id`,`connector_id`,`assigned_color_id`,`assigned_size_id`),
  KEY `assigned_color_id` (`assigned_color_id`),
  KEY `assigned_size_id` (`assigned_size_id`),
  KEY `connector_id` (`connector_id`),
  KEY `inventory_management` (`inventory_management`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11002 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_view`
--

CREATE TABLE IF NOT EXISTS `platform_product_view` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  `product_type_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `static` tinyint(1) NOT NULL DEFAULT '0',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `position` enum('NONE','FRONT','BACK','LEFT','RIGHT','TOP','BOTTOM','LEFT_HOOD','RIGHT_HOOD') NOT NULL DEFAULT 'NONE',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `product_type_id` (`product_type_id`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=2685 ;

--
-- Spúšťače `platform_product_view`
--
DROP TRIGGER IF EXISTS `update_product_view`;
DELIMITER //
CREATE TRIGGER `update_product_view` AFTER UPDATE ON `platform_product_view`
 FOR EACH ROW BEGIN
         update platform_product p , 
          platform_product_assigned_view ac
          set p.child_version = p.child_version + 1 
          where
            ac.product_id = p.id 
            and ac.deleted = 0
            and p.deleted = 0
            and ac.view_id= NEW.id;
     END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_view_localized`
--

CREATE TABLE IF NOT EXISTS `platform_product_view_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `view_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `view_id` (`view_id`,`lang_id`),
  KEY `lang_id` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17114 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_x_group`
--

CREATE TABLE IF NOT EXISTS `platform_product_x_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `attribute_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_id` (`product_id`,`group_id`,`attribute_id`),
  KEY `group_id` (`group_id`),
  KEY `attribute_id` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_product_x_product_category`
--

CREATE TABLE IF NOT EXISTS `platform_product_x_product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_category_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_category_id` (`product_category_id`,`product_id`),
  KEY `product_id` (`product_id`),
  KEY `version` (`version`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58397 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_resource_file`
--

CREATE TABLE IF NOT EXISTS `platform_resource_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `path` varchar(255) NOT NULL DEFAULT '',
  `source_status` enum('IN_USE','BACKUP','BACKUP_LOCKED','LOCKED','DELETED','ERROR') NOT NULL DEFAULT 'IN_USE',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `path` (`path`),
  KEY `source_status` (`source_status`),
  KEY `created` (`created`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=133238 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_session`
--

CREATE TABLE IF NOT EXISTS `platform_session` (
  `uuid` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `data` mediumblob,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`uuid`),
  KEY `created` (`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_shipping_module`
--

CREATE TABLE IF NOT EXISTS `platform_shipping_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `api_user` varchar(255) NOT NULL DEFAULT '',
  `api_key` varchar(255) NOT NULL DEFAULT '',
  `module_type` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(3) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `deleted` (`deleted`),
  KEY `version` (`version`),
  KEY `module_type` (`module_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=158 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_shipping_module_provider`
--

CREATE TABLE IF NOT EXISTS `platform_shipping_module_provider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_type` int(11) NOT NULL DEFAULT '0',
  `provider` varchar(32) NOT NULL,
  `service_type` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module_type` (`module_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=92 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_shipping_module_type`
--

CREATE TABLE IF NOT EXISTS `platform_shipping_module_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_shop`
--

CREATE TABLE IF NOT EXISTS `platform_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `creator_config_id` int(11) DEFAULT NULL,
  `logo_id` int(11) DEFAULT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `type` enum('SHOP','CORE','PUBLISH','SUBPARTNER_SAMPLE','SUBPARTNER_LIBRARY') NOT NULL DEFAULT 'SHOP',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  `default_product_id` int(11) DEFAULT NULL,
  `show_prices_excluding_vat` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `creator_config_id` (`creator_config_id`),
  KEY `account_id` (`account_id`),
  KEY `deleted` (`deleted`),
  KEY `version` (`version`),
  KEY `default_product_id` (`default_product_id`),
  KEY `type` (`type`),
  KEY `default` (`default`),
  KEY `logo_id` (`logo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1036 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_shopify_auth_request`
--

CREATE TABLE IF NOT EXISTS `platform_shopify_auth_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nonce` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `shop_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nonce` (`nonce`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=73 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_shopify_callback`
--

CREATE TABLE IF NOT EXISTS `platform_shopify_callback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `connector_id` int(11) DEFAULT NULL,
  `callback` varchar(255) NOT NULL DEFAULT '',
  `request_data` mediumtext,
  `request_time` timestamp NULL DEFAULT NULL,
  `processed` tinyint(3) NOT NULL DEFAULT '0',
  `processed_error_time` timestamp NULL DEFAULT NULL,
  `disabled` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `request_time` (`request_time`),
  KEY `processed` (`processed`),
  KEY `processed_error_time` (`processed_error_time`),
  KEY `connector_id` (`connector_id`),
  KEY `disabled` (`disabled`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=47445 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_shop_address`
--

CREATE TABLE IF NOT EXISTS `platform_shop_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL DEFAULT '0',
  `country_id` int(11) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `shop_country` (`shop_id`,`country_id`),
  KEY `shop_id` (`shop_id`),
  KEY `country_id` (`country_id`),
  KEY `address_id` (`address_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=291 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_shop_callback`
--

CREATE TABLE IF NOT EXISTS `platform_shop_callback` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL DEFAULT '',
  `user` varchar(128) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `shop_id` (`shop_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_shop_config_creator`
--

CREATE TABLE IF NOT EXISTS `platform_shop_config_creator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enable_motive` tinyint(1) NOT NULL DEFAULT '1',
  `enable_text` tinyint(1) NOT NULL DEFAULT '1',
  `enable_pimp_text` tinyint(1) NOT NULL DEFAULT '1',
  `enable_user_upload` tinyint(1) NOT NULL DEFAULT '1',
  `enable_change_product` tinyint(1) NOT NULL DEFAULT '1',
  `enable_share_product` tinyint(1) NOT NULL DEFAULT '1',
  `enable_pimp_service` tinyint(1) NOT NULL DEFAULT '1',
  `enable_start_controll` tinyint(1) NOT NULL DEFAULT '1',
  `enable_color_change` tinyint(1) NOT NULL DEFAULT '1',
  `enable_size_change` tinyint(1) NOT NULL DEFAULT '1',
  `enable_team_text` tinyint(1) NOT NULL DEFAULT '1',
  `decoration_color` varchar(6) NOT NULL DEFAULT '51C6BD',
  `font_data_id` int(11) DEFAULT NULL,
  `font_name` varchar(128) NOT NULL,
  `show_price_details` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  `creator_theme_id` int(11) NOT NULL DEFAULT '1',
  `creator_mobile_theme_id` int(11) DEFAULT '3',
  `background_color` char(6) NOT NULL DEFAULT 'ffffff',
  `background_alpha` float(3,2) NOT NULL DEFAULT '1.00',
  `size_amount_button_background_color` char(6) NOT NULL DEFAULT 'f05600',
  `size_amount_button_background_stroke` char(6) NOT NULL DEFAULT 'ac4309',
  `use_svg_text` tinyint(1) NOT NULL DEFAULT '1',
  `enable_object_size_label` tinyint(1) NOT NULL DEFAULT '0',
  `enable_individualisation_button` tinyint(1) NOT NULL DEFAULT '1',
  `enable_undo_redo` tinyint(1) NOT NULL DEFAULT '0',
  `addNewMotiveButton_motiveIcoUp` int(11) DEFAULT NULL,
  `addNewMotiveButton_textIcoUp` int(11) DEFAULT NULL,
  `addNewMotiveButton_uploadIcoUp` int(11) DEFAULT NULL,
  `addNewMotiveButton_gfxTextIcoUp` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `font_data_id` (`font_data_id`),
  KEY `creator_theme_id` (`creator_theme_id`),
  KEY `addNewMotiveButton_motiveIcoUp` (`addNewMotiveButton_motiveIcoUp`),
  KEY `addNewMotiveButton_textIcoUp` (`addNewMotiveButton_textIcoUp`),
  KEY `addNewMotiveButton_uploadIcoUp` (`addNewMotiveButton_uploadIcoUp`),
  KEY `addNewMotiveButton_gfxTextIcoUp` (`addNewMotiveButton_gfxTextIcoUp`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=88 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_shop_config_creator_resource`
--

CREATE TABLE IF NOT EXISTS `platform_shop_config_creator_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL DEFAULT '0',
  `data` mediumblob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_shop_connection`
--

CREATE TABLE IF NOT EXISTS `platform_shop_connection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `connector` enum('SHOPIFY','MAGENTO') NOT NULL DEFAULT 'SHOPIFY',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(3) NOT NULL DEFAULT '0',
  `shop_name` varchar(255) NOT NULL DEFAULT '',
  `access_token` char(255) DEFAULT NULL,
  `scope` varchar(1024) DEFAULT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `website_id` int(11) DEFAULT NULL,
  `target_shop_id` bigint(20) NOT NULL DEFAULT '0',
  `country_id` int(11) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `url` text NOT NULL,
  `email` text NOT NULL,
  `new_account` tinyint(3) NOT NULL DEFAULT '0',
  `upgrade_required` tinyint(3) NOT NULL DEFAULT '0',
  `update_auth_required` tinyint(3) NOT NULL DEFAULT '0',
  `last_fetch_created_orders` timestamp NULL DEFAULT NULL,
  `last_fetch_updated_orders` timestamp NULL DEFAULT NULL,
  `hash_id` int(11) DEFAULT NULL,
  `oauth_id` int(11) DEFAULT NULL,
  `app_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id2` (`connector`,`shop_name`,`shop_id`,`country_id`,`lang_id`,`target_shop_id`),
  KEY `shopify_shop_id` (`target_shop_id`),
  KEY `country_id` (`country_id`),
  KEY `lang_id` (`lang_id`),
  KEY `connector` (`connector`),
  KEY `shop_id` (`shop_id`),
  KEY `shop_name` (`shop_name`),
  KEY `upgrade_required` (`upgrade_required`),
  KEY `website_id` (`website_id`),
  KEY `hash_id` (`hash_id`),
  KEY `oauth_id` (`oauth_id`),
  KEY `app_user_id` (`app_user_id`),
  KEY `deleted` (`deleted`),
  KEY `created` (`created`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=125 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_shop_connection_property`
--

CREATE TABLE IF NOT EXISTS `platform_shop_connection_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `connector_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `connector_id` (`connector_id`),
  KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=54 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_shop_connector_front`
--

CREATE TABLE IF NOT EXISTS `platform_shop_connector_front` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designed_product_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `connector_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `connector_id` (`connector_id`),
  KEY `product_id` (`product_id`),
  KEY `designed_product_id` (`designed_product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=800 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_shop_property`
--

CREATE TABLE IF NOT EXISTS `platform_shop_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `value` varchar(1024) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `shop_id_2` (`shop_id`,`name`),
  KEY `shop_id` (`shop_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=944 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_state_source`
--

CREATE TABLE IF NOT EXISTS `platform_state_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `code` varchar(8) NOT NULL,
  `name` varchar(128) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7225 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_sticker`
--

CREATE TABLE IF NOT EXISTS `platform_sticker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `image_id` (`image_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=198 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_sticker_image`
--

CREATE TABLE IF NOT EXISTS `platform_sticker_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` mediumblob NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `created` (`created`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=73 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_stock_item`
--

CREATE TABLE IF NOT EXISTS `platform_stock_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_account_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `active` tinyint(1) DEFAULT '1',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`active`),
  KEY `name` (`name`),
  KEY `owner_account_id` (`owner_account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10836 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_stock_item_to_factory`
--

CREATE TABLE IF NOT EXISTS `platform_stock_item_to_factory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) NOT NULL,
  `factory_id` int(11) NOT NULL,
  `plu` varchar(64) NOT NULL,
  `available_in_stock` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stock_id` (`stock_id`,`factory_id`),
  KEY `available_in_stock` (`available_in_stock`),
  KEY `factory_id_2` (`factory_id`),
  KEY `plu` (`plu`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23585 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_stock_supplier`
--

CREATE TABLE IF NOT EXISTS `platform_stock_supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('LSHOP') NOT NULL DEFAULT 'LSHOP',
  `owner_account_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `logo_path` varchar(512) NOT NULL,
  `basic_auth_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `basic_auth_id` (`basic_auth_id`),
  KEY `deleted` (`deleted`),
  KEY `enabled` (`enabled`),
  KEY `owner_account_id` (`owner_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_stock_supplier_x_account`
--

CREATE TABLE IF NOT EXISTS `platform_stock_supplier_x_account` (
  `account_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  UNIQUE KEY `account_id` (`account_id`,`supplier_id`),
  KEY `supplier_id` (`supplier_id`),
  KEY `account_id_2` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_stock_to_supplier`
--

CREATE TABLE IF NOT EXISTS `platform_stock_to_supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `supplier_plu` varchar(32) NOT NULL,
  `available_in_stock` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stock_id` (`stock_id`,`supplier_id`),
  UNIQUE KEY `supplier_id` (`supplier_id`,`supplier_plu`),
  KEY `available_in_stock` (`available_in_stock`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_tag`
--

CREATE TABLE IF NOT EXISTS `platform_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `name` varchar(50) DEFAULT NULL,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `account_id` (`account_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=80883 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_tag_localized`
--

CREATE TABLE IF NOT EXISTS `platform_tag_localized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag_id` (`tag_id`,`lang_id`),
  KEY `lang_id` (`lang_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=290182 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_team_design_collection`
--

CREATE TABLE IF NOT EXISTS `platform_team_design_collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32650 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_template`
--

CREATE TABLE IF NOT EXISTS `platform_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `account_id` int(11) NOT NULL,
  `order_index` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `account_id` (`account_id`),
  KEY `order_index` (`order_index`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1220 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_template_svg`
--

CREATE TABLE IF NOT EXISTS `platform_template_svg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` mediumblob NOT NULL,
  `clip_geom_id` int(11) DEFAULT NULL,
  `background_geom_id` int(11) DEFAULT NULL,
  `bounds_geom_id` int(11) DEFAULT NULL,
  `base_width` mediumint(8) unsigned NOT NULL,
  `base_height` mediumint(8) unsigned NOT NULL,
  `account_id` int(11) NOT NULL COMMENT 'uploader',
  `file_name` varchar(64) NOT NULL,
  `file_size` mediumint(8) unsigned zerofill NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `clip_geom_id` (`clip_geom_id`),
  UNIQUE KEY `background_geom_id` (`background_geom_id`),
  UNIQUE KEY `bounds_geom_id` (`bounds_geom_id`),
  KEY `account` (`account_id`),
  KEY `created` (`created`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=488 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_template_to_factory`
--

CREATE TABLE IF NOT EXISTS `platform_template_to_factory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) NOT NULL,
  `factory_assigned_id` int(11) NOT NULL,
  `svg_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `template_id_2` (`template_id`,`factory_assigned_id`),
  KEY `template_id` (`template_id`),
  KEY `factory_assigned_id` (`factory_assigned_id`),
  KEY `svg_id` (`svg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3751 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_transformation`
--

CREATE TABLE IF NOT EXISTS `platform_transformation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assigned_view_id` int(11) NOT NULL,
  `product_area_id` int(11) NOT NULL,
  `base_width_twips` smallint(6) NOT NULL DEFAULT '0',
  `base_height_twips` smallint(6) NOT NULL DEFAULT '0',
  `m00` decimal(22,18) NOT NULL DEFAULT '1.000000000000000000' COMMENT 'a',
  `m01` decimal(22,18) NOT NULL DEFAULT '0.000000000000000000' COMMENT 'c',
  `m02` decimal(22,18) NOT NULL DEFAULT '0.000000000000000000' COMMENT 'e',
  `m10` decimal(22,18) NOT NULL DEFAULT '0.000000000000000000' COMMENT 'b',
  `m11` decimal(22,18) NOT NULL DEFAULT '1.000000000000000000' COMMENT 'd',
  `m12` decimal(22,18) NOT NULL DEFAULT '0.000000000000000000' COMMENT 'f',
  `m20` decimal(22,18) NOT NULL DEFAULT '0.000000000000000000',
  `m21` decimal(22,18) NOT NULL DEFAULT '0.000000000000000000',
  `m22` decimal(22,18) NOT NULL DEFAULT '1.000000000000000000',
  `surface_lines` tinyint(2) NOT NULL DEFAULT '0',
  `surface_columns` tinyint(2) NOT NULL DEFAULT '0',
  `surface_vectors` varchar(1024) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `assigned_view_id_2` (`assigned_view_id`),
  KEY `product_area_id` (`product_area_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7480 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_user`
--

CREATE TABLE IF NOT EXISTS `platform_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  `login` varchar(80) NOT NULL DEFAULT '',
  `password` varchar(80) NOT NULL DEFAULT '',
  `email` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(3) NOT NULL DEFAULT '0',
  `user_type` enum('USER','APP') NOT NULL DEFAULT 'USER',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `email` (`email`),
  KEY `enabled` (`enabled`),
  KEY `deleted` (`deleted`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=395 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_user_authority`
--

CREATE TABLE IF NOT EXISTS `platform_user_authority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `version` int(11) NOT NULL DEFAULT '1',
  `deleted` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_user_granted_authority`
--

CREATE TABLE IF NOT EXISTS `platform_user_granted_authority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `authority_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`authority_id`),
  KEY `authority_id` (`authority_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=417 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_user_granted_shop_access`
--

CREATE TABLE IF NOT EXISTS `platform_user_granted_shop_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `shop_id` int(11) NOT NULL DEFAULT '0',
  `write_allowed` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`shop_id`),
  KEY `shop_id` (`shop_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1404 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_user_motive`
--

CREATE TABLE IF NOT EXISTS `platform_user_motive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `motive_id` int(11) NOT NULL COMMENT 'FK to platform_motive',
  PRIMARY KEY (`id`),
  KEY `motive_id` (`motive_id`),
  KEY `uuid` (`uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Identify user uploaded motive' AUTO_INCREMENT=3819721 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_vat`
--

CREATE TABLE IF NOT EXISTS `platform_vat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `percent_value` decimal(9,2) NOT NULL DEFAULT '0.00',
  `version` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `shop_id` (`account_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1967 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_webhook`
--

CREATE TABLE IF NOT EXISTS `platform_webhook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL DEFAULT '',
  `topic` varchar(255) NOT NULL DEFAULT '',
  `auth_token` char(64) NOT NULL DEFAULT '',
  `media_type` varchar(255) NOT NULL DEFAULT '',
  `shop_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `shop_id` (`shop_id`),
  KEY `topic` (`topic`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_webhook_exec`
--

CREATE TABLE IF NOT EXISTS `platform_webhook_exec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `webhook_id` int(11) NOT NULL DEFAULT '0',
  `data_id` int(11) NOT NULL DEFAULT '0',
  `data_text` text,
  `created` datetime DEFAULT NULL,
  `executed` datetime DEFAULT NULL,
  `tryouts` int(11) NOT NULL DEFAULT '0',
  `finished` tinyint(3) NOT NULL DEFAULT '0',
  `disabled` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `webhook_id` (`webhook_id`),
  KEY `finished` (`finished`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=660304 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `secure_basic`
--

CREATE TABLE IF NOT EXISTS `secure_basic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `deleted` tinyint(1) DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `secure_hash`
--

CREATE TABLE IF NOT EXISTS `secure_hash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) NOT NULL DEFAULT '',
  `deleted` tinyint(1) DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `secure_nonce`
--

CREATE TABLE IF NOT EXISTS `secure_nonce` (
  `uuid` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `secure_oauth`
--

CREATE TABLE IF NOT EXISTS `secure_oauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consumer_key` varchar(255) NOT NULL DEFAULT '',
  `consumer_secret` varchar(255) NOT NULL DEFAULT '',
  `token` varchar(255) NOT NULL DEFAULT '',
  `secret` varchar(255) NOT NULL DEFAULT '',
  `deleted` tinyint(1) DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_importable_print_color`
--

CREATE TABLE IF NOT EXISTS `sharing_importable_print_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `print_color_palette_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `print_color_palette_id` (`print_color_palette_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_importable_print_media`
--

CREATE TABLE IF NOT EXISTS `sharing_importable_print_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `print_media_size_palette_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `print_media_size_palette_id` (`print_media_size_palette_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_importable_print_technology`
--

CREATE TABLE IF NOT EXISTS `sharing_importable_print_technology` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `print_technology_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `print_technology_id` (`print_technology_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_importable_product`
--

CREATE TABLE IF NOT EXISTS `sharing_importable_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL DEFAULT '0.00',
  `premium_price` decimal(9,2) NOT NULL DEFAULT '0.00',
  `free_for_share` tinyint(1) NOT NULL DEFAULT '0',
  `is_premium` tinyint(1) NOT NULL DEFAULT '0',
  `on_change` enum('IGNORE','UPDATE_CHILDS','SEND_NOTIFICATION') NOT NULL DEFAULT 'IGNORE',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `order_index_2` (`order_index`),
  KEY `deleted` (`deleted`),
  KEY `free_for_share` (`free_for_share`),
  KEY `is_premium` (`is_premium`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_motive`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_motive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `source_type` enum('PUBLIC','OWN') NOT NULL DEFAULT 'PUBLIC',
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `source_id` (`source_id`),
  KEY `destination_id` (`destination_id`),
  KEY `deleted` (`deleted`),
  KEY `source_type` (`source_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=316362 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_motive_category`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_motive_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_id` int(11) NOT NULL,
  `thread` int(11) DEFAULT NULL COMMENT 'joins categories in pool. Shops should have null',
  `destination_id` int(11) NOT NULL,
  `source_type` enum('PUBLIC','OWN') NOT NULL DEFAULT 'PUBLIC',
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `source_id` (`source_id`),
  KEY `destination_id` (`destination_id`),
  KEY `deleted` (`deleted`),
  KEY `source_type` (`source_type`),
  KEY `thread` (`thread`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12231 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_photo_character`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_photo_character` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_print_color`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_print_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `src_print_color_id` int(11) NOT NULL,
  `dst_print_color_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `src_print_color_id` (`src_print_color_id`),
  KEY `dst_print_color_id` (`dst_print_color_id`),
  KEY `created` (`created`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31134 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_print_color_palette`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_print_color_palette` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2300 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_print_media_size`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_print_media_size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `src_print_media_size_id` int(11) NOT NULL,
  `dst_print_media_size_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `src_print_media_size_id` (`src_print_media_size_id`),
  KEY `dst_print_media_size_id` (`dst_print_media_size_id`),
  KEY `created` (`created`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2064 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_print_media_size_palette`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_print_media_size_palette` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=971 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_print_technology`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_print_technology` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `source_id` (`source_id`),
  KEY `destination_id` (`destination_id`),
  KEY `deleted` (`deleted`),
  KEY `created` (`created`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=891 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `source_type` enum('PUBLIC','OWN') NOT NULL DEFAULT 'PUBLIC',
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`),
  KEY `deleted` (`deleted`),
  KEY `source_type` (`source_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22113 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_area`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66424 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_area_real_size`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_area_real_size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=300120 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_assigned_color`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_assigned_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=149268 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_assigned_print_technology`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_assigned_print_technology` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=56311 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_assigned_size`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_assigned_size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=82923 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_assigned_sticker`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_assigned_sticker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=771 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_assigned_view`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_assigned_view` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=69754 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_assigned_view_snap`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_assigned_view_snap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_category`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `thread` int(11) DEFAULT NULL COMMENT 'joins categories in pool. Shops should have null',
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`),
  KEY `thread` (`thread`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14656 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_colision`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_colision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3025 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_color`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21790 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_color_pool`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_color_pool` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2253 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_color_value`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_color_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25419 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_photo_character`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_photo_character` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_photo_detail`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_photo_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10579 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_photo_measure`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_photo_measure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18310 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_size`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4570 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_size_pool`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_size_pool` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=715 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_sku`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_sku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=761486 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_to_template`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_to_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4138 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_type`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=445 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_type_assigned_print_technology`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_type_assigned_print_technology` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=957 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_type_attribute_list`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_type_attribute_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1103 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_type_attribute_list_item`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_type_attribute_list_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11135 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_product_view`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_product_view` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2024 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_sticker`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_sticker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=141 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_template`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1013 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `sharing_imported_transformation`
--

CREATE TABLE IF NOT EXISTS `sharing_imported_transformation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `destination_id` (`destination_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5243 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `spod_product_variant`
--

CREATE TABLE IF NOT EXISTS `spod_product_variant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` varchar(255) NOT NULL DEFAULT '',
  `sku` varchar(255) NOT NULL DEFAULT '',
  `size_id` varchar(255) NOT NULL DEFAULT '',
  `appearance_id` varchar(255) NOT NULL DEFAULT '',
  `shop_id` int(11) NOT NULL DEFAULT '0',
  `manufacturer_module_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sku` (`sku`,`shop_id`,`manufacturer_module_id`),
  KEY `shop_id` (`shop_id`),
  KEY `manufacturer_module_id` (`manufacturer_module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3999 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `spod_remote_order`
--

CREATE TABLE IF NOT EXISTS `spod_remote_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `factory_order_id` int(11) NOT NULL DEFAULT '0',
  `remote_order_id` varchar(65) NOT NULL DEFAULT '',
  `state` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fetch_state_time` timestamp NULL DEFAULT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0',
  `manufacturer_module_id` int(11) NOT NULL DEFAULT '0',
  `acknowledged_time` timestamp NULL DEFAULT NULL,
  `confirmed_time` timestamp NULL DEFAULT NULL,
  `shipped_time` timestamp NULL DEFAULT NULL,
  `disabled` tinyint(3) NOT NULL DEFAULT '0',
  `processing_error` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `factory_order_id` (`factory_order_id`),
  KEY `remote_order_id` (`remote_order_id`),
  KEY `state` (`state`),
  KEY `fetch_state_time` (`fetch_state_time`),
  KEY `shop_id` (`shop_id`),
  KEY `manufacturer_module_id` (`manufacturer_module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20406 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_element`
--

CREATE TABLE IF NOT EXISTS `tool_element` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `platform_element_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `media_size_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `platform_element_id` (`platform_element_id`),
  KEY `product_id` (`product_id`),
  KEY `media_size_id` (`media_size_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3232376 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_external_endpoint_configuration`
--

CREATE TABLE IF NOT EXISTS `tool_external_endpoint_configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(1024) NOT NULL,
  `secure_basic_id` int(11) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `secure_basic_id` (`secure_basic_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_factory_place`
--

CREATE TABLE IF NOT EXISTS `tool_factory_place` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `account_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `deleted` (`deleted`),
  KEY `version` (`version`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_factory_place_user_setting`
--

CREATE TABLE IF NOT EXISTS `tool_factory_place_user_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `warehouse_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `visible` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `warehouse_id` (`warehouse_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=91 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_filter_item`
--

CREATE TABLE IF NOT EXISTS `tool_filter_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `preset_id` int(11) NOT NULL,
  `filter_id` enum('ORDER_ID','SUMMARY_ID','UNIQ_ID','FACILITY','PRODUCABLE','TARGET_COUNTRY','SOURCE_SHOP','PRODUCTS_COUNT','STATUS','PRODUCT_DIGITALS','PRODUCT_LASERS','PRODUCT_SUBLIMATIONS','PRODUCT_FLEX_SIDES','PRODUCT_FLEX_CHARACTERS','PRODUCT_ENGRAVINGS','PRODUCT_SCREEN_PRINTS','DESCRIPTION','TIME_SLOT','WORK_SPACE','GROUP','PRODUCT_PLU','PRODUCT_ORDERED_ID','PRODUCT_ID','CALENDAR_DATE','ORDER_FLAG','SHIP_PROVIDER_ID','SHIP_PROVIDER_SERVICE','COMMENT','ASSIGNED_PRODUCTION','LAST_STATUS_CHANGE','PRODUCT_EXACT_PLU','FINANCIAL_STATUS','FULLFILMENT_STATUS','PLATFORM_SHOP','TRACK_NUM','PRODUCT_DIGITALDIRECTS','TICKET_PRINT_DATE','PRODUCT_COLOR','PRINT_MEDIA','EXACT_PRINT_MEDIA','PAID','READY_TO_PRODUCE','PLATFORM_ORDER_ID','STATE_CODE','REMOTE_ID') NOT NULL,
  `value` text NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `preset_id` (`preset_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1270 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_filter_preset`
--

CREATE TABLE IF NOT EXISTS `tool_filter_preset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` enum('ORDER_LIST','SUMMARY_LIST','CALENDAR','SUPER_ORDER_LIST','READONLY_ORDER_LIST','READONLY_CALENDAR','READONLY_SUMMARY_LIST','SHOP_ACCESS_ORDER_LIST') NOT NULL,
  `auto` smallint(6) NOT NULL,
  `locked` smallint(6) NOT NULL,
  `visible` smallint(6) NOT NULL,
  `name` varchar(32) NOT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `auto` (`auto`),
  KEY `type` (`type`),
  KEY `deleted` (`deleted`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=260 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_gather_record`
--

CREATE TABLE IF NOT EXISTS `tool_gather_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_id` int(11) NOT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `carrier_id` int(11) DEFAULT NULL,
  `file_url` varchar(128) DEFAULT NULL,
  `handover_url` varchar(128) DEFAULT NULL,
  `labels_url` varchar(128) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` smallint(6) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `provider_id` (`provider_id`),
  KEY `carrier_id` (`carrier_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21594 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_group`
--

CREATE TABLE IF NOT EXISTS `tool_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `short` varchar(2) NOT NULL,
  `factory_place_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `short` (`short`,`factory_place_id`,`deleted`),
  KEY `production_id` (`factory_place_id`),
  KEY `delete` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=140 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_manufacturer_module`
--

CREATE TABLE IF NOT EXISTS `tool_manufacturer_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` enum('NONE','BASIC','K2','ZOHO','SPOD','OPT') DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `warehouse_id` int(11) NOT NULL DEFAULT '0',
  `factory_id` int(11) NOT NULL,
  `url` varchar(128) NOT NULL DEFAULT '',
  `secure_basic_id` int(11) DEFAULT NULL,
  `external_endpoint_configuration` int(11) DEFAULT NULL,
  `active` tinyint(3) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `factory_id` (`factory_id`),
  KEY `warehouse` (`warehouse_id`),
  KEY `external_endpoint_configuration` (`external_endpoint_configuration`),
  KEY `active` (`active`),
  KEY `secure_basic_id` (`secure_basic_id`),
  KEY `module` (`module`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_manufacturer_module_property`
--

CREATE TABLE IF NOT EXISTS `tool_manufacturer_module_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `value` varchar(256) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stock_module_id_2` (`module_id`,`name`),
  KEY `stock_module_id` (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_manufacturer_module_shop_property`
--

CREATE TABLE IF NOT EXISTS `tool_manufacturer_module_shop_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tool_manufacturer_module_id` int(11) NOT NULL,
  `platform_shop_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `value` varchar(1024) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tool_manufacturer_module_id_2` (`tool_manufacturer_module_id`,`platform_shop_id`,`name`),
  KEY `tool_manufacturer_module_id` (`tool_manufacturer_module_id`),
  KEY `platform_shop_id` (`platform_shop_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=124 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_media_size`
--

CREATE TABLE IF NOT EXISTS `tool_media_size` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `deleted` tinyint(1) DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_notification_seen`
--

CREATE TABLE IF NOT EXISTS `tool_notification_seen` (
  `user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`) USING BTREE,
  KEY `last_date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_order`
--

CREATE TABLE IF NOT EXISTS `tool_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flag` set('RECLAMATION','EXPRESS') DEFAULT NULL,
  `public_unique_id` varchar(64) NOT NULL,
  `platform_order_id` int(11) DEFAULT NULL,
  `manufacturer_remote_id` varchar(64) NOT NULL DEFAULT '',
  `shop_id` int(11) DEFAULT NULL,
  `reservation_id` int(11) DEFAULT NULL,
  `reserved_warehouse_id` int(11) DEFAULT NULL,
  `producable` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sender_address_id` int(11) DEFAULT NULL,
  `receiver_address_id` int(11) DEFAULT NULL,
  `shipping_id` int(11) DEFAULT NULL,
  `status` enum('NEW','IN_SUMMARY','GENERATING','PREPARED','IN_PROGRESS','PRODUCED','CANCELLED','ERROR','PREPARED_FOR_SHIPMENT','SHIPPED','DELIVERED','OUT_OF_STOCK','REFUSED','HOLD') NOT NULL DEFAULT 'NEW',
  `module_status` enum('NONE','SENDING','ACCEPTED','PREPARED','PRODUCED','SHIPPED','DELIVERED','OUT_OF_STOCK','CANCELLED','ERROR','NOT_FOUND') NOT NULL DEFAULT 'NONE',
  `remote_status` varchar(32) NOT NULL DEFAULT '',
  `deleted` tinyint(1) DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `reservation_id` (`reservation_id`),
  KEY `intern_id` (`public_unique_id`),
  KEY `shop_id` (`shop_id`),
  KEY `producable` (`producable`),
  KEY `status` (`status`),
  KEY `deleted` (`deleted`),
  KEY `platform_order_id` (`platform_order_id`),
  KEY `reserved_factory_place_id` (`reserved_warehouse_id`),
  KEY `sender_address_id` (`sender_address_id`),
  KEY `receiver_address_id` (`receiver_address_id`),
  KEY `shipping_id` (`shipping_id`),
  KEY `flag` (`flag`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=759890 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_order_action`
--

CREATE TABLE IF NOT EXISTS `tool_order_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` enum('MOVE_ORDERS','VALIDATE_AVAILABILITY','SET_STATUS','CANCEL_RESERVATION','CHANGE_SHIPPING','RELOAD_ORDERS','APPLY_SHIPPING_RULES','ADD_NOTE') NOT NULL,
  `state` enum('IN_PROGRESS','DONE','DONE_WITH_ERROR','ERROR') NOT NULL,
  `info` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `created` (`created`),
  KEY `account_id` (`account_id`),
  KEY `updated` (`updated`),
  KEY `state` (`state`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6405 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_order_action_item`
--

CREATE TABLE IF NOT EXISTS `tool_order_action_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `result` tinyint(4) DEFAULT NULL,
  `message` varchar(10000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `action_id` (`action_id`),
  KEY `order_id` (`order_id`),
  KEY `result` (`result`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=327656 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_order_attachment`
--

CREATE TABLE IF NOT EXISTS `tool_order_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(512) NOT NULL,
  `original_name` varchar(256) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=197 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_order_flag`
--

CREATE TABLE IF NOT EXISTS `tool_order_flag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `short_name` varchar(2) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_order_history`
--

CREATE TABLE IF NOT EXISTS `tool_order_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` enum('NEW','IN_SUMMARY','GENERATING','PREPARED','IN_PROGRESS','PRODUCED','CANCELLED','ERROR','PREPARED_FOR_SHIPMENT','SHIPPED','DELIVERED','OUT_OF_STOCK','REFUSED','HOLD') NOT NULL DEFAULT 'NEW',
  `title` varchar(256) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `attachments` tinyint(3) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7643145 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_order_reservation`
--

CREATE TABLE IF NOT EXISTS `tool_order_reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer_module_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `forced` smallint(6) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stock_module_id` (`manufacturer_module_id`),
  KEY `created` (`created`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1782847 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_order_shipping`
--

CREATE TABLE IF NOT EXISTS `tool_order_shipping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carrier_id` int(11) DEFAULT NULL,
  `shipping_provider_id` int(11) DEFAULT NULL,
  `provider_service_type` varchar(32) DEFAULT NULL,
  `price` decimal(9,4) DEFAULT NULL,
  `cod_price` decimal(9,4) DEFAULT NULL,
  `order_value` decimal(9,4) NOT NULL DEFAULT '0.0000',
  `currency` varchar(3) DEFAULT NULL,
  `prefered_delivery_time` datetime DEFAULT NULL,
  `branch_id` varchar(128) NOT NULL DEFAULT '',
  `ticket_print_user_id` int(11) DEFAULT NULL,
  `ticket_scan_time` timestamp NULL DEFAULT NULL,
  `gather_record_id` int(11) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `shipping_provider_id` (`shipping_provider_id`),
  KEY `gather_record_id` (`gather_record_id`),
  KEY `ticket_print_user_id` (`ticket_print_user_id`),
  KEY `carrier_id` (`carrier_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=765773 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_order_x_flag`
--

CREATE TABLE IF NOT EXISTS `tool_order_x_flag` (
  `order_id` int(11) NOT NULL,
  `flag_id` int(11) NOT NULL,
  UNIQUE KEY `order_id_2` (`order_id`,`flag_id`),
  KEY `order_id` (`order_id`),
  KEY `flag_id` (`flag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_product`
--

CREATE TABLE IF NOT EXISTS `tool_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intern_id` varchar(11) NOT NULL DEFAULT '0',
  `platform_ordered_product_id` int(11) DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  `amount` smallint(3) unsigned NOT NULL DEFAULT '1',
  `wrongly_printed` smallint(6) NOT NULL DEFAULT '0',
  `plu` varchar(128) NOT NULL DEFAULT '',
  `ignore_reservation` tinyint(1) NOT NULL DEFAULT '0',
  `digitals` smallint(5) unsigned NOT NULL DEFAULT '0',
  `digital_directs` smallint(5) unsigned NOT NULL DEFAULT '0',
  `lasers` smallint(5) unsigned NOT NULL DEFAULT '0',
  `foils` smallint(5) unsigned NOT NULL DEFAULT '0',
  `foil_motives` smallint(5) unsigned NOT NULL DEFAULT '0',
  `foil_characters` smallint(5) unsigned NOT NULL DEFAULT '0',
  `sublimations` smallint(5) unsigned NOT NULL DEFAULT '0',
  `engravings` smallint(5) unsigned NOT NULL DEFAULT '0',
  `screen_prints` smallint(5) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `plu` (`plu`),
  KEY `deleted` (`deleted`),
  KEY `intern_id` (`intern_id`),
  KEY `platform_ordered_product_id` (`platform_ordered_product_id`),
  KEY `version` (`version`),
  KEY `wrongly_printed` (`wrongly_printed`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1255736 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_product_comment`
--

CREATE TABLE IF NOT EXISTS `tool_product_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(2048) NOT NULL,
  `promoted` smallint(6) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) DEFAULT '0',
  `type` enum('USER','RESERVATION','SYSTEM') NOT NULL DEFAULT 'USER',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `deleted` (`deleted`),
  KEY `version` (`version`),
  KEY `order_id` (`order_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=53077 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_provider_branch`
--

CREATE TABLE IF NOT EXISTS `tool_provider_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_provider_id` int(11) NOT NULL,
  `branch_id` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `module_provider_id_2` (`module_provider_id`,`branch_id`),
  KEY `module_provider_id` (`module_provider_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34733 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_saved_order_note`
--

CREATE TABLE IF NOT EXISTS `tool_saved_order_note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_shipping_address`
--

CREATE TABLE IF NOT EXISTS `tool_shipping_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT '',
  `country` varchar(255) NOT NULL,
  `country_code` varchar(2) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `street` varchar(1024) NOT NULL,
  `street_no` varchar(255) NOT NULL,
  `company` varchar(255) DEFAULT '',
  `care_of` varchar(255) NOT NULL DEFAULT '',
  `state_code` varchar(8) NOT NULL DEFAULT '',
  `state_name` varchar(128) NOT NULL DEFAULT '',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=761276 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_shipping_customize`
--

CREATE TABLE IF NOT EXISTS `tool_shipping_customize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `warehouse_id` int(11) NOT NULL DEFAULT '0',
  `filter_preset_id` int(11) NOT NULL DEFAULT '0',
  `platform_shipping_module_provider_id` int(11) NOT NULL DEFAULT '0',
  `order_index` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `shipping` (`platform_shipping_module_provider_id`),
  KEY `filter_preset_id` (`filter_preset_id`),
  KEY `warehouse_id` (`warehouse_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_shipping_module`
--

CREATE TABLE IF NOT EXISTS `tool_shipping_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('SHIPCLOUD','BALIKOBOT','DEPOST','ATPOSTSS','GLS') NOT NULL,
  `account_id` int(11) NOT NULL,
  `api_user` varchar(32) NOT NULL,
  `api_key` varchar(128) NOT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_shipping_property`
--

CREATE TABLE IF NOT EXISTS `tool_shipping_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_id` int(11) NOT NULL,
  `key` enum('ID','PACKAGE_ID','PACKAGE_POS','TRACK_NUM') NOT NULL,
  `value` varchar(1024) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `shipping_id` (`shipping_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1147943 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_shipping_provider`
--

CREATE TABLE IF NOT EXISTS `tool_shipping_provider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `provider` varchar(16) NOT NULL,
  `name` varchar(32) NOT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `module_id_2` (`module_id`,`provider`,`deleted`),
  KEY `module_id` (`module_id`),
  KEY `deleted` (`deleted`),
  KEY `provider` (`provider`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_shipping_setup`
--

CREATE TABLE IF NOT EXISTS `tool_shipping_setup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `module_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_id_2` (`account_id`,`module_type_id`),
  KEY `account_id` (`account_id`),
  KEY `module_type_id` (`module_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_shipping_setup_property`
--

CREATE TABLE IF NOT EXISTS `tool_shipping_setup_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setup_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `value` varchar(128) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `setup_id_2` (`setup_id`,`name`),
  KEY `version` (`version`),
  KEY `setup_id` (`setup_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_shop`
--

CREATE TABLE IF NOT EXISTS `tool_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `factory_account_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT '0',
  `code` varchar(2) DEFAULT NULL,
  `platform_shop_id` int(11) DEFAULT NULL,
  `status_callback_url` varchar(255) NOT NULL DEFAULT '',
  `callback_user` varchar(32) NOT NULL DEFAULT '',
  `callback_pass` varchar(255) NOT NULL DEFAULT '',
  `deleted` tinyint(1) DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_id_2` (`factory_account_id`,`platform_shop_id`,`deleted`),
  KEY `type` (`platform_shop_id`),
  KEY `account_id` (`factory_account_id`),
  KEY `deleted` (`deleted`),
  KEY `type_2` (`platform_shop_id`),
  KEY `version` (`version`),
  KEY `account_id_3` (`factory_account_id`,`code`,`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=135 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_shop_x_address`
--

CREATE TABLE IF NOT EXISTS `tool_shop_x_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `default` smallint(6) NOT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `address_id` (`address_id`),
  KEY `shop_id` (`shop_id`),
  KEY `default` (`default`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_stock_management_rule`
--

CREATE TABLE IF NOT EXISTS `tool_stock_management_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `preset_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) NOT NULL,
  `stock_required` smallint(6) NOT NULL,
  `order_index` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `preset_id` (`preset_id`),
  KEY `version` (`version`),
  KEY `order_index` (`order_index`),
  KEY `warehouse_id` (`warehouse_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_summary`
--

CREATE TABLE IF NOT EXISTS `tool_summary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timeslot_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL DEFAULT '',
  `status` enum('NEW','SCHEDULED','GENERATING','PREPARED','IN_PROGRESS','PRODUCED','ERROR') NOT NULL DEFAULT 'NEW',
  `generate_cmd_time` timestamp NULL DEFAULT NULL COMMENT 'time when it was scheduled in data generation phase',
  `deleted` tinyint(1) DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `status` (`status`),
  KEY `generate_cmd_time` (`generate_cmd_time`),
  KEY `account_id` (`account_id`),
  KEY `timeslot_id` (`timeslot_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16283 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_summary_x_order`
--

CREATE TABLE IF NOT EXISTS `tool_summary_x_order` (
  `summary_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  UNIQUE KEY `order_id` (`order_id`),
  KEY `summary_id` (`summary_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_technology_map`
--

CREATE TABLE IF NOT EXISTS `tool_technology_map` (
  `factory_technology_id` int(11) NOT NULL,
  `shop_technology_id` int(11) NOT NULL,
  UNIQUE KEY `factory_technology_id` (`factory_technology_id`,`shop_technology_id`),
  KEY `shop_technology_id` (`shop_technology_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_timeslot`
--

CREATE TABLE IF NOT EXISTS `tool_timeslot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `workspace_id` int(11) NOT NULL,
  `merge_id` int(11) DEFAULT NULL,
  `book_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `duration` int(11) NOT NULL,
  `locked` smallint(6) NOT NULL DEFAULT '0',
  `deleted` smallint(6) DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `account_id` (`account_id`),
  KEY `group_id` (`group_id`),
  KEY `workspace_id` (`workspace_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9557 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_warehouse_stock_email`
--

CREATE TABLE IF NOT EXISTS `tool_warehouse_stock_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `warehouse_id` int(11) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `warehouse_id` (`warehouse_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_work_space`
--

CREATE TABLE IF NOT EXISTS `tool_work_space` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` varchar(32) NOT NULL,
  `slot_size` int(11) NOT NULL,
  `autocreate_slot` tinyint(3) NOT NULL DEFAULT '0',
  `factory_place_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `production_id` (`factory_place_id`),
  KEY `deleted` (`deleted`),
  KEY `version` (`version`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=75 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_zoho_contact`
--

CREATE TABLE IF NOT EXISTS `tool_zoho_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `contact_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=117 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_zoho_fulfilled`
--

CREATE TABLE IF NOT EXISTS `tool_zoho_fulfilled` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zoho_order_id` int(11) NOT NULL,
  `result` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `zoho_order_id` (`zoho_order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=101712 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_zoho_inventory`
--

CREATE TABLE IF NOT EXISTS `tool_zoho_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3255 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_zoho_invoice`
--

CREATE TABLE IF NOT EXISTS `tool_zoho_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `invoice_number` varchar(64) NOT NULL,
  `invoice_id` bigint(20) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `client` varchar(64) NOT NULL,
  `total` decimal(10,5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1792 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_zoho_invoice_item`
--

CREATE TABLE IF NOT EXISTS `tool_zoho_invoice_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `rate` double(10,5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_id` (`invoice_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=188590 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_zoho_invoice_x_order`
--

CREATE TABLE IF NOT EXISTS `tool_zoho_invoice_x_order` (
  `order_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  KEY `order_id` (`order_id`),
  KEY `invoice_id` (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_zoho_item`
--

CREATE TABLE IF NOT EXISTS `tool_zoho_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `zoho_name` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6738 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_zoho_order`
--

CREATE TABLE IF NOT EXISTS `tool_zoho_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `sales_order_id` bigint(20) NOT NULL,
  `contact` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=265834 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `tool_zoho_order_item`
--

CREATE TABLE IF NOT EXISTS `tool_zoho_order_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zoho_order_id` int(11) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `line_item_id` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zoho_order_id` (`zoho_order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=216437 ;

--
-- Obmedzenie pre exportované tabuľky
--

--
-- Obmedzenie pre tabuľku `addon_queue_method_param`
--
ALTER TABLE `addon_queue_method_param`
  ADD CONSTRAINT `addon_queue_method_param_ibfk_1` FOREIGN KEY (`method_id`) REFERENCES `addon_queue_method` (`Id`);

--
-- Obmedzenie pre tabuľku `platform_account`
--
ALTER TABLE `platform_account`
  ADD CONSTRAINT `platform_account.data_retention_id` FOREIGN KEY (`data_retention_id`) REFERENCES `platform_account_data_retention` (`id`),
  ADD CONSTRAINT `platform_account_ibfk_1` FOREIGN KEY (`company_logo_id`) REFERENCES `platform_company_logo` (`id`);

--
-- Obmedzenie pre tabuľku `platform_business_entity`
--
ALTER TABLE `platform_business_entity`
  ADD CONSTRAINT `platform_business_entity_ibfk_1` FOREIGN KEY (`country_source_id`) REFERENCES `platform_country_source` (`id`);

--
-- Obmedzenie pre tabuľku `platform_carrier_free_delivery_price`
--
ALTER TABLE `platform_carrier_free_delivery_price`
  ADD CONSTRAINT `platform_carrier_free_delivery_price_ibfk_1` FOREIGN KEY (`carrier_id`) REFERENCES `platform_carrier_service` (`id`),
  ADD CONSTRAINT `platform_carrier_free_delivery_price_ibfk_2` FOREIGN KEY (`country_id`) REFERENCES `platform_country` (`id`);

--
-- Obmedzenie pre tabuľku `platform_carrier_service`
--
ALTER TABLE `platform_carrier_service`
  ADD CONSTRAINT `platform_carrier_service_module_ID` FOREIGN KEY (`module_id`) REFERENCES `platform_shipping_module` (`id`),
  ADD CONSTRAINT `platform_carrier_service_provider_ID` FOREIGN KEY (`provider_id`) REFERENCES `platform_shipping_module_provider` (`id`);

--
-- Obmedzenie pre tabuľku `platform_carrier_service_connection`
--
ALTER TABLE `platform_carrier_service_connection`
  ADD CONSTRAINT `carrier_service_connection_shop_connection_ID` FOREIGN KEY (`connector_id`) REFERENCES `platform_shop_connection` (`id`);

--
-- Obmedzenie pre tabuľku `platform_carrier_service_destination`
--
ALTER TABLE `platform_carrier_service_destination`
  ADD CONSTRAINT `platform_carrier_service_destination_carrier_ID` FOREIGN KEY (`carrier_id`) REFERENCES `platform_carrier_service` (`id`),
  ADD CONSTRAINT `platform_carrier_service_destination_country_ID` FOREIGN KEY (`country_id`) REFERENCES `platform_country` (`id`),
  ADD CONSTRAINT `platform_carrier_service_destination_zone_ID` FOREIGN KEY (`zone_id`) REFERENCES `platform_carrier_service_zone` (`id`);

--
-- Obmedzenie pre tabuľku `platform_carrier_service_localized`
--
ALTER TABLE `platform_carrier_service_localized`
  ADD CONSTRAINT `carrier_service_localized_carrier_ID` FOREIGN KEY (`carrier_id`) REFERENCES `platform_carrier_service` (`id`),
  ADD CONSTRAINT `carrier_service_localized_language_ID` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`);

--
-- Obmedzenie pre tabuľku `platform_carrier_service_price`
--
ALTER TABLE `platform_carrier_service_price`
  ADD CONSTRAINT `platform_carrier_service_price_country_ID` FOREIGN KEY (`country_id`) REFERENCES `platform_country` (`id`),
  ADD CONSTRAINT `platform_carrier_service_price_zone_ID` FOREIGN KEY (`zone_id`) REFERENCES `platform_carrier_service_zone` (`id`);

--
-- Obmedzenie pre tabuľku `platform_carrier_service_zone`
--
ALTER TABLE `platform_carrier_service_zone`
  ADD CONSTRAINT `carrier_service_fixed_rate_zone_carrier_ID` FOREIGN KEY (`carrier_id`) REFERENCES `platform_carrier_service` (`id`);

--
-- Obmedzenie pre tabuľku `platform_collection`
--
ALTER TABLE `platform_collection`
  ADD CONSTRAINT `platform_collection_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `platform_shop` (`id`);

--
-- Obmedzenie pre tabuľku `platform_collection_category`
--
ALTER TABLE `platform_collection_category`
  ADD CONSTRAINT `platform_collection_category_ibfk_1` FOREIGN KEY (`parent_category_id`) REFERENCES `platform_collection_category` (`id`),
  ADD CONSTRAINT `platform_collection_category_ibfk_2` FOREIGN KEY (`shop_id`) REFERENCES `platform_shop` (`id`);

--
-- Obmedzenie pre tabuľku `platform_collection_category_localized`
--
ALTER TABLE `platform_collection_category_localized`
  ADD CONSTRAINT `platform_collection_category_localized_ibfk_1` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`),
  ADD CONSTRAINT `platform_collection_category_localized_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `platform_collection_category` (`id`);

--
-- Obmedzenie pre tabuľku `platform_collection_category_x_product`
--
ALTER TABLE `platform_collection_category_x_product`
  ADD CONSTRAINT `platform_collection_category_x_product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `platform_collection_category` (`id`),
  ADD CONSTRAINT `platform_collection_category_x_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `platform_designed_template_product` (`id`);

--
-- Obmedzenie pre tabuľku `platform_collection_localized`
--
ALTER TABLE `platform_collection_localized`
  ADD CONSTRAINT `platform_collection_localized_ibfk_1` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`),
  ADD CONSTRAINT `platform_collection_localized_ibfk_2` FOREIGN KEY (`collection_id`) REFERENCES `platform_collection` (`id`);

--
-- Obmedzenie pre tabuľku `platform_color_texture`
--
ALTER TABLE `platform_color_texture`
  ADD CONSTRAINT `platform_color_texture_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_company_address`
--
ALTER TABLE `platform_company_address`
  ADD CONSTRAINT `platform_company_address_ibfk_3` FOREIGN KEY (`country_source_id`) REFERENCES `platform_country_source` (`id`),
  ADD CONSTRAINT `platform_company_address_ibfk_4` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_company_business`
--
ALTER TABLE `platform_company_business`
  ADD CONSTRAINT `platform_company_business_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`),
  ADD CONSTRAINT `platform_company_business_ibfk_2` FOREIGN KEY (`business_entity_id`) REFERENCES `platform_business_entity` (`id`);

--
-- Obmedzenie pre tabuľku `platform_company_contact`
--
ALTER TABLE `platform_company_contact`
  ADD CONSTRAINT `platform_company_contact_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_country`
--
ALTER TABLE `platform_country`
  ADD CONSTRAINT `platform_country_ibfk_10` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`),
  ADD CONSTRAINT `platform_country_ibfk_3` FOREIGN KEY (`vat_standard_id`) REFERENCES `platform_vat` (`id`),
  ADD CONSTRAINT `platform_country_ibfk_4` FOREIGN KEY (`vat_reduced_id`) REFERENCES `platform_vat` (`id`),
  ADD CONSTRAINT `platform_country_ibfk_5` FOREIGN KEY (`currency_id`) REFERENCES `platform_currency` (`id`);

--
-- Obmedzenie pre tabuľku `platform_currency`
--
ALTER TABLE `platform_currency`
  ADD CONSTRAINT `platform_currency_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_customer`
--
ALTER TABLE `platform_customer`
  ADD CONSTRAINT `customer_billing_address_address_ID` FOREIGN KEY (`billing_address_id`) REFERENCES `platform_address` (`id`),
  ADD CONSTRAINT `customer_shipping_address_address_ID` FOREIGN KEY (`shipping_address_id`) REFERENCES `platform_address` (`id`);

--
-- Obmedzenie pre tabuľku `platform_design`
--
ALTER TABLE `platform_design`
  ADD CONSTRAINT `platform_design_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`);

--
-- Obmedzenie pre tabuľku `platform_designed_ordered_product`
--
ALTER TABLE `platform_designed_ordered_product`
  ADD CONSTRAINT `platform_designed_ordered_product.price_calc_report_id` FOREIGN KEY (`price_calc_report_id`) REFERENCES `platform_design_price_calc_report` (`id`),
  ADD CONSTRAINT `platform_designed_ordered_product.team_design_collection_id` FOREIGN KEY (`team_design_collection_id`) REFERENCES `platform_team_design_collection` (`id`),
  ADD CONSTRAINT `platform_designed_ordered_product_ibfk_10` FOREIGN KEY (`order_id`) REFERENCES `platform_order` (`id`),
  ADD CONSTRAINT `platform_designed_ordered_product_ibfk_11` FOREIGN KEY (`design_id`) REFERENCES `platform_design` (`id`),
  ADD CONSTRAINT `platform_designed_ordered_product_ibfk_12` FOREIGN KEY (`color_id`) REFERENCES `platform_product_assigned_color` (`id`),
  ADD CONSTRAINT `platform_designed_ordered_product_ibfk_13` FOREIGN KEY (`size_id`) REFERENCES `platform_product_assigned_size` (`id`);

--
-- Obmedzenie pre tabuľku `platform_designed_ordered_product_connection`
--
ALTER TABLE `platform_designed_ordered_product_connection`
  ADD CONSTRAINT `designed_ordered_product_connection_ordered_product_ID` FOREIGN KEY (`ordered_product_id`) REFERENCES `platform_designed_ordered_product` (`id`),
  ADD CONSTRAINT `designed_ordered_product_connection_shop_connection_ID` FOREIGN KEY (`connector_id`) REFERENCES `platform_shop_connection` (`id`);

--
-- Obmedzenie pre tabuľku `platform_designed_ordered_product_custom_attributes`
--
ALTER TABLE `platform_designed_ordered_product_custom_attributes`
  ADD CONSTRAINT `platform_designed_ordered_product_custom_attributes_ibfk_1` FOREIGN KEY (`ordered_product_id`) REFERENCES `platform_designed_ordered_product` (`id`);

--
-- Obmedzenie pre tabuľku `platform_designed_shared_product`
--
ALTER TABLE `platform_designed_shared_product`
  ADD CONSTRAINT `platform_designed_shared_product.design_id` FOREIGN KEY (`design_id`) REFERENCES `platform_design` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `platform_designed_shared_product_ibfk_2` FOREIGN KEY (`color_id`) REFERENCES `platform_product_assigned_color` (`id`),
  ADD CONSTRAINT `platform_designed_shared_product_ibfk_5` FOREIGN KEY (`shop_id`) REFERENCES `platform_shop` (`id`);

--
-- Obmedzenie pre tabuľku `platform_designed_template_product`
--
ALTER TABLE `platform_designed_template_product`
  ADD CONSTRAINT `platform_designed_template_product_ibfk_3` FOREIGN KEY (`design_id`) REFERENCES `platform_design` (`id`),
  ADD CONSTRAINT `platform_designed_template_product_ibfk_6` FOREIGN KEY (`color_id`) REFERENCES `platform_product_assigned_color` (`id`),
  ADD CONSTRAINT `platform_designed_template_product_ibfk_7` FOREIGN KEY (`size_id`) REFERENCES `platform_product_assigned_size` (`id`),
  ADD CONSTRAINT `platform_designed_template_product_ibfk_8` FOREIGN KEY (`collection_id`) REFERENCES `platform_collection` (`id`);

--
-- Obmedzenie pre tabuľku `platform_designed_template_product_color`
--
ALTER TABLE `platform_designed_template_product_color`
  ADD CONSTRAINT `platform_designed_template_product_color_ibfk_1` FOREIGN KEY (`template_id`) REFERENCES `platform_designed_template_product` (`id`),
  ADD CONSTRAINT `platform_designed_template_product_color_ibfk_3` FOREIGN KEY (`assigned_color_id`) REFERENCES `platform_product_assigned_color` (`id`);

--
-- Obmedzenie pre tabuľku `platform_designed_template_product_connection`
--
ALTER TABLE `platform_designed_template_product_connection`
  ADD CONSTRAINT `template_product_connection_shop_connection_id` FOREIGN KEY (`connector_id`) REFERENCES `platform_shop_connection` (`id`),
  ADD CONSTRAINT `template_product_connection_template_product_id` FOREIGN KEY (`template_product_id`) REFERENCES `platform_designed_template_product` (`id`);

--
-- Obmedzenie pre tabuľku `platform_designed_template_product_image_connection`
--
ALTER TABLE `platform_designed_template_product_image_connection`
  ADD CONSTRAINT `template_image_connection_shop_connection_ID` FOREIGN KEY (`connector_id`) REFERENCES `platform_shop_connection` (`id`),
  ADD CONSTRAINT `template_image_connection_template_product_ID` FOREIGN KEY (`template_product_id`) REFERENCES `platform_designed_template_product` (`id`);

--
-- Obmedzenie pre tabuľku `platform_designed_template_product_localized`
--
ALTER TABLE `platform_designed_template_product_localized`
  ADD CONSTRAINT `platform_designed_template_product_localized_ibfk_1` FOREIGN KEY (`template_id`) REFERENCES `platform_designed_template_product` (`id`),
  ADD CONSTRAINT `platform_designed_template_product_localized_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`);

--
-- Obmedzenie pre tabuľku `platform_designed_template_product_price`
--
ALTER TABLE `platform_designed_template_product_price`
  ADD CONSTRAINT `platform_designed_template_product_price_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `platform_country` (`id`),
  ADD CONSTRAINT `platform_designed_template_product_price_ibfk_2` FOREIGN KEY (`template_id`) REFERENCES `platform_designed_template_product` (`id`);

--
-- Obmedzenie pre tabuľku `platform_designed_template_product_variant_connection`
--
ALTER TABLE `platform_designed_template_product_variant_connection`
  ADD CONSTRAINT `template_variant_connection_assigned_color_ID` FOREIGN KEY (`assigned_color_id`) REFERENCES `platform_product_assigned_color` (`id`),
  ADD CONSTRAINT `template_variant_connection_product_assigned_size_ID` FOREIGN KEY (`assigned_size_id`) REFERENCES `platform_product_assigned_size` (`id`),
  ADD CONSTRAINT `template_variant_connection_shop_connection_ID` FOREIGN KEY (`connector_id`) REFERENCES `platform_shop_connection` (`id`),
  ADD CONSTRAINT `template_variant_connection_template_product_ID` FOREIGN KEY (`template_product_id`) REFERENCES `platform_designed_template_product` (`id`);

--
-- Obmedzenie pre tabuľku `platform_designed_template_product_view_disabled`
--
ALTER TABLE `platform_designed_template_product_view_disabled`
  ADD CONSTRAINT `platform_designed_template_product_view_disabled_ibfk_1` FOREIGN KEY (`template_id`) REFERENCES `platform_designed_template_product` (`id`),
  ADD CONSTRAINT `platform_designed_template_product_view_disabled_ibfk_2` FOREIGN KEY (`assigned_view_id`) REFERENCES `platform_product_assigned_view` (`id`);

--
-- Obmedzenie pre tabuľku `platform_design_composition`
--
ALTER TABLE `platform_design_composition`
  ADD CONSTRAINT `platform_design_composition.design_id` FOREIGN KEY (`design_id`) REFERENCES `platform_design` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `platform_design_composition_ibfk_2` FOREIGN KEY (`original_product_area_id`) REFERENCES `platform_product_area` (`id`);

--
-- Obmedzenie pre tabuľku `platform_design_element`
--
ALTER TABLE `platform_design_element`
  ADD CONSTRAINT `platform_design_element.composition_id` FOREIGN KEY (`composition_id`) REFERENCES `platform_design_composition` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `platform_design_element.transform_bounds_id` FOREIGN KEY (`transform_bounds_id`) REFERENCES `platform_bounds` (`id`),
  ADD CONSTRAINT `platform_design_element_ibfk_2` FOREIGN KEY (`pimp_image_task_id`) REFERENCES `platform_pimp_output_task` (`id`);

--
-- Obmedzenie pre tabuľku `platform_design_element_layer`
--
ALTER TABLE `platform_design_element_layer`
  ADD CONSTRAINT `platform_design_element_layer.element_id` FOREIGN KEY (`element_id`) REFERENCES `platform_design_element` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `platform_design_element_layer_ibfk_11` FOREIGN KEY (`assigned_print_color_id`) REFERENCES `platform_print_technology_assigned_color` (`id`),
  ADD CONSTRAINT `platform_design_element_layer_ibfk_12` FOREIGN KEY (`print_technology_id`) REFERENCES `platform_print_technology` (`id`),
  ADD CONSTRAINT `platform_design_element_layer_ibfk_5` FOREIGN KEY (`swf_layer_id`) REFERENCES `platform_motive_data_layer` (`id`);

--
-- Obmedzenie pre tabuľku `platform_design_element_motive`
--
ALTER TABLE `platform_design_element_motive`
  ADD CONSTRAINT `platform_design_element_motive.element_id` FOREIGN KEY (`element_id`) REFERENCES `platform_design_element` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `platform_design_element_motive_ibfk_2` FOREIGN KEY (`motive_id`) REFERENCES `platform_motive` (`id`);

--
-- Obmedzenie pre tabuľku `platform_design_element_pimp_text`
--
ALTER TABLE `platform_design_element_pimp_text`
  ADD CONSTRAINT `platform_design_element_pimp_text.element_id` FOREIGN KEY (`element_id`) REFERENCES `platform_design_element` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `platform_design_element_pimp_text_ibfk_3` FOREIGN KEY (`resource_id`) REFERENCES `platform_resource_file` (`id`);

--
-- Obmedzenie pre tabuľku `platform_design_element_qrcode`
--
ALTER TABLE `platform_design_element_qrcode`
  ADD CONSTRAINT `platform_design_element_qrcode.element_id` FOREIGN KEY (`element_id`) REFERENCES `platform_design_element` (`id`) ON DELETE CASCADE;

--
-- Obmedzenie pre tabuľku `platform_design_element_svg_text`
--
ALTER TABLE `platform_design_element_svg_text`
  ADD CONSTRAINT `platform_design_element_svg_text.element_id` FOREIGN KEY (`element_id`) REFERENCES `platform_design_element` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `platform_design_element_svg_text.font_id` FOREIGN KEY (`font_id`) REFERENCES `platform_font` (`id`),
  ADD CONSTRAINT `platform_design_element_svg_text.svg_id` FOREIGN KEY (`svg_id`) REFERENCES `platform_motive_data_svg` (`id`) ON DELETE CASCADE;

--
-- Obmedzenie pre tabuľku `platform_design_element_text`
--
ALTER TABLE `platform_design_element_text`
  ADD CONSTRAINT `platform_design_element_text.element_id` FOREIGN KEY (`element_id`) REFERENCES `platform_design_element` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `platform_design_element_text_ibfk_3` FOREIGN KEY (`font_id`) REFERENCES `platform_font` (`id`);

--
-- Obmedzenie pre tabuľku `platform_design_element_text_char`
--
ALTER TABLE `platform_design_element_text_char`
  ADD CONSTRAINT `platform_design_element_text_char.element_id` FOREIGN KEY (`element_id`) REFERENCES `platform_design_element` (`id`) ON DELETE CASCADE;

--
-- Obmedzenie pre tabuľku `platform_design_element_text_line`
--
ALTER TABLE `platform_design_element_text_line`
  ADD CONSTRAINT `platform_design_element_text_line.element_id` FOREIGN KEY (`element_id`) REFERENCES `platform_design_element` (`id`) ON DELETE CASCADE;

--
-- Obmedzenie pre tabuľku `platform_factory`
--
ALTER TABLE `platform_factory`
  ADD CONSTRAINT `platform_factory_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_factory_assigned`
--
ALTER TABLE `platform_factory_assigned`
  ADD CONSTRAINT `platform_factory_assigned_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`),
  ADD CONSTRAINT `platform_factory_assigned_ibfk_3` FOREIGN KEY (`factory_id`) REFERENCES `platform_factory` (`id`);

--
-- Obmedzenie pre tabuľku `platform_font`
--
ALTER TABLE `platform_font`
  ADD CONSTRAINT `platform_font_ibfk_1` FOREIGN KEY (`data_id`) REFERENCES `platform_font_data` (`id`),
  ADD CONSTRAINT `platform_font_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_font_public`
--
ALTER TABLE `platform_font_public`
  ADD CONSTRAINT `platform_font_public_ibfk_1` FOREIGN KEY (`data_id`) REFERENCES `platform_font_data` (`id`),
  ADD CONSTRAINT `platform_font_public_ibfk_2` FOREIGN KEY (`library_id`) REFERENCES `platform_font_public_library` (`id`);

--
-- Obmedzenie pre tabuľku `platform_fulfillment_service_connection`
--
ALTER TABLE `platform_fulfillment_service_connection`
  ADD CONSTRAINT `fulfillment_service_connection.connector_id` FOREIGN KEY (`connector_id`) REFERENCES `platform_shop_connection` (`id`);

--
-- Obmedzenie pre tabuľku `platform_kornit_preset`
--
ALTER TABLE `platform_kornit_preset`
  ADD CONSTRAINT `platform_kornit_preset_ibfk_1` FOREIGN KEY (`media_name_id`) REFERENCES `platform_kornit_media_name` (`id`);

--
-- Obmedzenie pre tabuľku `platform_kornit_setup`
--
ALTER TABLE `platform_kornit_setup`
  ADD CONSTRAINT `platform_kornit_setup_ibfk_1` FOREIGN KEY (`media_name_id`) REFERENCES `platform_kornit_media_name` (`id`);

--
-- Obmedzenie pre tabuľku `platform_language`
--
ALTER TABLE `platform_language`
  ADD CONSTRAINT `platform_language_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_motive`
--
ALTER TABLE `platform_motive`
  ADD CONSTRAINT `platform_motive_ibfk_1` FOREIGN KEY (`bitmap_id`) REFERENCES `platform_motive_data_bitmap` (`id`),
  ADD CONSTRAINT `platform_motive_ibfk_10` FOREIGN KEY (`pool_id`) REFERENCES `platform_motive_pool` (`id`),
  ADD CONSTRAINT `platform_motive_ibfk_11` FOREIGN KEY (`pimp_image_task_id`) REFERENCES `platform_pimp_output_task` (`id`),
  ADD CONSTRAINT `platform_motive_ibfk_2` FOREIGN KEY (`swf_id`) REFERENCES `platform_motive_data_swf` (`id`),
  ADD CONSTRAINT `platform_motive_ibfk_3` FOREIGN KEY (`shop_id`) REFERENCES `platform_shop` (`id`),
  ADD CONSTRAINT `platform_motive_ibfk_8` FOREIGN KEY (`designer_id`) REFERENCES `platform_motive_designer` (`id`),
  ADD CONSTRAINT `platform_motive_ibfk_9` FOREIGN KEY (`original_motive_id`) REFERENCES `platform_motive` (`id`);

--
-- Obmedzenie pre tabuľku `platform_motive_category`
--
ALTER TABLE `platform_motive_category`
  ADD CONSTRAINT `platform_motive_category_ibfk_2` FOREIGN KEY (`parent_category_id`) REFERENCES `platform_motive_category` (`id`),
  ADD CONSTRAINT `platform_motive_category_ibfk_4` FOREIGN KEY (`shop_id`) REFERENCES `platform_shop` (`id`);

--
-- Obmedzenie pre tabuľku `platform_motive_category_localized`
--
ALTER TABLE `platform_motive_category_localized`
  ADD CONSTRAINT `platform_motive_category_localized_ibfk_1` FOREIGN KEY (`motive_category_id`) REFERENCES `platform_motive_category` (`id`),
  ADD CONSTRAINT `platform_motive_category_localized_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`);

--
-- Obmedzenie pre tabuľku `platform_motive_data_bitmap`
--
ALTER TABLE `platform_motive_data_bitmap`
  ADD CONSTRAINT `platform_motive_data_bitmap_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`),
  ADD CONSTRAINT `platform_motive_data_bitmap_ibfk_3` FOREIGN KEY (`advanced_setup_id`) REFERENCES `platform_motive_data_bitmap_setup` (`id`);

--
-- Obmedzenie pre tabuľku `platform_motive_data_layer`
--
ALTER TABLE `platform_motive_data_layer`
  ADD CONSTRAINT `platform_motive_data_layer_ibfk_1` FOREIGN KEY (`motive_id`) REFERENCES `platform_motive` (`id`);

--
-- Obmedzenie pre tabuľku `platform_motive_data_layer_technology`
--
ALTER TABLE `platform_motive_data_layer_technology`
  ADD CONSTRAINT `platform_motive_data_layer_technology.default_color_id` FOREIGN KEY (`default_color_id`) REFERENCES `platform_print_color` (`id`),
  ADD CONSTRAINT `platform_motive_data_layer_technology.layer_id` FOREIGN KEY (`layer_id`) REFERENCES `platform_motive_data_layer` (`id`),
  ADD CONSTRAINT `platform_motive_data_layer_technology.technology_id` FOREIGN KEY (`technology_id`) REFERENCES `platform_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `platform_motive_designer`
--
ALTER TABLE `platform_motive_designer`
  ADD CONSTRAINT `platform_motive_designer_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_motive_disable_country`
--
ALTER TABLE `platform_motive_disable_country`
  ADD CONSTRAINT `platform_motive_disable_country_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `platform_country` (`id`),
  ADD CONSTRAINT `platform_motive_disable_country_ibfk_2` FOREIGN KEY (`motive_id`) REFERENCES `platform_motive` (`id`);

--
-- Obmedzenie pre tabuľku `platform_motive_localized`
--
ALTER TABLE `platform_motive_localized`
  ADD CONSTRAINT `platform_motive_localized_ibfk_1` FOREIGN KEY (`motive_id`) REFERENCES `platform_motive` (`id`),
  ADD CONSTRAINT `platform_motive_localized_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`);

--
-- Obmedzenie pre tabuľku `platform_motive_pool`
--
ALTER TABLE `platform_motive_pool`
  ADD CONSTRAINT `platform_motive_pool_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `platform_shop` (`id`);

--
-- Obmedzenie pre tabuľku `platform_motive_pool_property`
--
ALTER TABLE `platform_motive_pool_property`
  ADD CONSTRAINT `platform_motive_pool_property_ibfk_1` FOREIGN KEY (`pool_id`) REFERENCES `platform_motive_pool` (`id`);

--
-- Obmedzenie pre tabuľku `platform_motive_print_technology`
--
ALTER TABLE `platform_motive_print_technology`
  ADD CONSTRAINT `platform_motive_print_technology_ibfk_1` FOREIGN KEY (`print_technology_id`) REFERENCES `platform_print_technology` (`id`),
  ADD CONSTRAINT `platform_motive_print_technology_ibfk_2` FOREIGN KEY (`motive_id`) REFERENCES `platform_motive` (`id`);

--
-- Obmedzenie pre tabuľku `platform_motive_x_motive_category`
--
ALTER TABLE `platform_motive_x_motive_category`
  ADD CONSTRAINT `platform_motive_x_motive_category_ibfk_1` FOREIGN KEY (`motive_category_id`) REFERENCES `platform_motive_category` (`id`),
  ADD CONSTRAINT `platform_motive_x_motive_category_ibfk_2` FOREIGN KEY (`motive_id`) REFERENCES `platform_motive` (`id`);

--
-- Obmedzenie pre tabuľku `platform_motive_x_tag`
--
ALTER TABLE `platform_motive_x_tag`
  ADD CONSTRAINT `platform_motive_x_tag_ibfk_1` FOREIGN KEY (`motive_id`) REFERENCES `platform_motive` (`id`),
  ADD CONSTRAINT `platform_motive_x_tag_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `platform_tag` (`id`);

--
-- Obmedzenie pre tabuľku `platform_order`
--
ALTER TABLE `platform_order`
  ADD CONSTRAINT `order_customer_ID` FOREIGN KEY (`customer_id`) REFERENCES `platform_customer` (`id`),
  ADD CONSTRAINT `order_order_connection_id` FOREIGN KEY (`connection_id`) REFERENCES `platform_order_connection` (`id`),
  ADD CONSTRAINT `platform_order.data_erasure_id` FOREIGN KEY (`data_erasure_id`) REFERENCES `platform_order_data_erasure` (`id`),
  ADD CONSTRAINT `platform_order_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `platform_shop` (`id`),
  ADD CONSTRAINT `platform_order_ibfk_2` FOREIGN KEY (`country_id`) REFERENCES `platform_country` (`id`),
  ADD CONSTRAINT `sender_address` FOREIGN KEY (`shop_address_id`) REFERENCES `platform_address` (`id`);

--
-- Obmedzenie pre tabuľku `platform_ordered_manual_service`
--
ALTER TABLE `platform_ordered_manual_service`
  ADD CONSTRAINT `platform_ordered_manual_service.order_id` FOREIGN KEY (`order_id`) REFERENCES `platform_order` (`id`),
  ADD CONSTRAINT `platform_ordered_manual_service.pimp_output_action_id` FOREIGN KEY (`pimp_output_action_id`) REFERENCES `platform_pimp_output_action` (`id`);

--
-- Obmedzenie pre tabuľku `platform_order_connection`
--
ALTER TABLE `platform_order_connection`
  ADD CONSTRAINT `order_connection_shop_connection_id` FOREIGN KEY (`connector_id`) REFERENCES `platform_shop_connection` (`id`);

--
-- Obmedzenie pre tabuľku `platform_order_deferred_item`
--
ALTER TABLE `platform_order_deferred_item`
  ADD CONSTRAINT `platform_order_deferred_item.deferred_order_id` FOREIGN KEY (`deferred_order_id`) REFERENCES `platform_order_deferred` (`id`);

--
-- Obmedzenie pre tabuľku `platform_order_fulfillment`
--
ALTER TABLE `platform_order_fulfillment`
  ADD CONSTRAINT `order_fulfillment_carrier_ID` FOREIGN KEY (`carrier_id`) REFERENCES `platform_carrier_service` (`id`),
  ADD CONSTRAINT `order_fulfillment_order_ID` FOREIGN KEY (`order_id`) REFERENCES `platform_order` (`id`);

--
-- Obmedzenie pre tabuľku `platform_order_fulfillment_connection`
--
ALTER TABLE `platform_order_fulfillment_connection`
  ADD CONSTRAINT `order_fulfillment_connection_order_fulfillment_ID` FOREIGN KEY (`fulfillment_id`) REFERENCES `platform_order_fulfillment` (`id`),
  ADD CONSTRAINT `order_fulfillment_connection_shop_connection_ID` FOREIGN KEY (`connector_id`) REFERENCES `platform_shop_connection` (`id`);

--
-- Obmedzenie pre tabuľku `platform_order_fulfillment_item`
--
ALTER TABLE `platform_order_fulfillment_item`
  ADD CONSTRAINT `order_fulfillment_item_designed_ordered_product_ID` FOREIGN KEY (`order_item_id`) REFERENCES `platform_designed_ordered_product` (`id`),
  ADD CONSTRAINT `order_fulfillment_item_order_fulfillment_ID` FOREIGN KEY (`order_fulfillment_id`) REFERENCES `platform_order_fulfillment` (`id`);

--
-- Obmedzenie pre tabuľku `platform_order_import_csv`
--
ALTER TABLE `platform_order_import_csv`
  ADD CONSTRAINT `platform_order_import_csv.dataId` FOREIGN KEY (`dataId`) REFERENCES `platform_order_import_csv_data` (`id`);

--
-- Obmedzenie pre tabuľku `platform_order_import_csv_line`
--
ALTER TABLE `platform_order_import_csv_line`
  ADD CONSTRAINT `platform_order_import_csv_line.csvId` FOREIGN KEY (`csvId`) REFERENCES `platform_order_import_csv` (`id`);

--
-- Obmedzenie pre tabuľku `platform_order_shipping`
--
ALTER TABLE `platform_order_shipping`
  ADD CONSTRAINT `order_shipping_carrier_service_ID` FOREIGN KEY (`carrier_id`) REFERENCES `platform_carrier_service` (`id`),
  ADD CONSTRAINT `order_shipping_order_ID` FOREIGN KEY (`order_id`) REFERENCES `platform_order` (`id`);

--
-- Obmedzenie pre tabuľku `platform_overview_config`
--
ALTER TABLE `platform_overview_config`
  ADD CONSTRAINT `platform_overview_config_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_photo_character`
--
ALTER TABLE `platform_photo_character`
  ADD CONSTRAINT `platform_photo_character_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_pimp_action_group`
--
ALTER TABLE `platform_pimp_action_group`
  ADD CONSTRAINT `platform_pimp_action_group_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_pimp_action_group_price`
--
ALTER TABLE `platform_pimp_action_group_price`
  ADD CONSTRAINT `platform_pimp_action_group_price_ibfk_1` FOREIGN KEY (`pimp_action_group_id`) REFERENCES `platform_pimp_action_group` (`id`),
  ADD CONSTRAINT `platform_pimp_action_group_price_ibfk_2` FOREIGN KEY (`country_id`) REFERENCES `platform_country` (`id`);

--
-- Obmedzenie pre tabuľku `platform_pimp_output_action`
--
ALTER TABLE `platform_pimp_output_action`
  ADD CONSTRAINT `platform_pimp_output_action_ibfk_1` FOREIGN KEY (`action_group_id`) REFERENCES `platform_pimp_action_group` (`id`),
  ADD CONSTRAINT `platform_pimp_output_action_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `platform_pimp_output_image_task` (`id`);

--
-- Obmedzenie pre tabuľku `platform_pimp_output_bitmap_font_task`
--
ALTER TABLE `platform_pimp_output_bitmap_font_task`
  ADD CONSTRAINT `platform_pimp_output_bitmap_font_task_ibfk_1` FOREIGN KEY (`id`) REFERENCES `platform_pimp_output_task` (`id`),
  ADD CONSTRAINT `platform_pimp_output_bitmap_font_task_ibfk_2` FOREIGN KEY (`action_group_id`) REFERENCES `platform_pimp_action_group` (`id`);

--
-- Obmedzenie pre tabuľku `platform_pimp_output_image_task`
--
ALTER TABLE `platform_pimp_output_image_task`
  ADD CONSTRAINT `platform_pimp_output_image_task_ibfk_1` FOREIGN KEY (`id`) REFERENCES `platform_pimp_output_task` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_color`
--
ALTER TABLE `platform_print_color`
  ADD CONSTRAINT `platform_print_color_ibfk_11` FOREIGN KEY (`bitmap_id`) REFERENCES `platform_color_texture` (`id`),
  ADD CONSTRAINT `platform_print_color_ibfk_12` FOREIGN KEY (`overlay_id`) REFERENCES `platform_color_texture` (`id`),
  ADD CONSTRAINT `platform_print_color_ibfk_3` FOREIGN KEY (`palette_id`) REFERENCES `platform_print_color_palette` (`id`),
  ADD CONSTRAINT `platform_print_color_ibfk_8` FOREIGN KEY (`icon_id`) REFERENCES `platform_color_texture` (`id`),
  ADD CONSTRAINT `platform_print_color_ibfk_9` FOREIGN KEY (`preview_id`) REFERENCES `platform_color_texture` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_color_localized`
--
ALTER TABLE `platform_print_color_localized`
  ADD CONSTRAINT `platform_print_color_localized_ibfk_1` FOREIGN KEY (`print_color_id`) REFERENCES `platform_print_color` (`id`),
  ADD CONSTRAINT `platform_print_color_localized_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_color_palette`
--
ALTER TABLE `platform_print_color_palette`
  ADD CONSTRAINT `platform_print_color_palette_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_media_size`
--
ALTER TABLE `platform_print_media_size`
  ADD CONSTRAINT `platform_print_media_size_ibfk_1` FOREIGN KEY (`palette_id`) REFERENCES `platform_print_media_size_palette` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_media_size_localized`
--
ALTER TABLE `platform_print_media_size_localized`
  ADD CONSTRAINT `platform_print_media_size_localized_ibfk_1` FOREIGN KEY (`media_size_id`) REFERENCES `platform_print_media_size` (`id`),
  ADD CONSTRAINT `platform_print_media_size_localized_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_media_size_palette`
--
ALTER TABLE `platform_print_media_size_palette`
  ADD CONSTRAINT `platform_print_media_size_palette_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_profile`
--
ALTER TABLE `platform_print_profile`
  ADD CONSTRAINT `platform_print_profile_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_technology`
--
ALTER TABLE `platform_print_technology`
  ADD CONSTRAINT `platform_print_technology_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`),
  ADD CONSTRAINT `platform_print_technology_ibfk_10` FOREIGN KEY (`motive_collection_id`) REFERENCES `platform_print_technology_input_motive_collection` (`id`),
  ADD CONSTRAINT `platform_print_technology_ibfk_11` FOREIGN KEY (`qrcode_id`) REFERENCES `platform_print_technology_input_qrcode` (`id`),
  ADD CONSTRAINT `platform_print_technology_ibfk_12` FOREIGN KEY (`display_module_id`) REFERENCES `platform_production_display_module` (`id`),
  ADD CONSTRAINT `platform_print_technology_ibfk_13` FOREIGN KEY (`transformer_module_id`) REFERENCES `platform_production_transformer_module` (`id`),
  ADD CONSTRAINT `platform_print_technology_ibfk_14` FOREIGN KEY (`response_module_id`) REFERENCES `platform_production_response_module` (`id`),
  ADD CONSTRAINT `platform_print_technology_ibfk_15` FOREIGN KEY (`file_module_id`) REFERENCES `platform_production_file_module` (`id`),
  ADD CONSTRAINT `platform_print_technology_ibfk_16` FOREIGN KEY (`text_id`) REFERENCES `platform_print_technology_input_text` (`id`),
  ADD CONSTRAINT `platform_print_technology_ibfk_17` FOREIGN KEY (`upload_id`) REFERENCES `platform_print_technology_input_upload` (`id`),
  ADD CONSTRAINT `platform_print_technology_ibfk_18` FOREIGN KEY (`profile_id`) REFERENCES `platform_print_technology_profile` (`id`),
  ADD CONSTRAINT `platform_print_technology_ibfk_19` FOREIGN KEY (`preview_drop_shadow_id`) REFERENCES `platform_print_technology_preview_drop_shadow` (`id`),
  ADD CONSTRAINT `platform_print_technology_ibfk_20` FOREIGN KEY (`preview_blend_mode_dark_id`) REFERENCES `platform_print_technology_preview_blend_mode` (`id`),
  ADD CONSTRAINT `platform_print_technology_ibfk_21` FOREIGN KEY (`preview_blend_mode_white_id`) REFERENCES `platform_print_technology_preview_blend_mode` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_technology_assigned_color`
--
ALTER TABLE `platform_print_technology_assigned_color`
  ADD CONSTRAINT `platform_print_technology_assigned_color_ibfk_1` FOREIGN KEY (`print_color_id`) REFERENCES `platform_print_color` (`id`),
  ADD CONSTRAINT `platform_print_technology_assigned_color_ibfk_2` FOREIGN KEY (`print_color_group_id`) REFERENCES `platform_print_technology_color_group` (`id`),
  ADD CONSTRAINT `platform_print_technology_assigned_color_ibfk_3` FOREIGN KEY (`print_technology_id`) REFERENCES `platform_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_technology_assigned_media`
--
ALTER TABLE `platform_print_technology_assigned_media`
  ADD CONSTRAINT `platform_print_technology_assigned_media_ibfk_2` FOREIGN KEY (`print_media_size_id`) REFERENCES `platform_print_media_size` (`id`),
  ADD CONSTRAINT `platform_print_technology_assigned_media_ibfk_3` FOREIGN KEY (`print_technology_id`) REFERENCES `platform_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_technology_color_group`
--
ALTER TABLE `platform_print_technology_color_group`
  ADD CONSTRAINT `platform_print_technology_color_group_ibfk_1` FOREIGN KEY (`print_technology_id`) REFERENCES `platform_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_technology_color_group_localized`
--
ALTER TABLE `platform_print_technology_color_group_localized`
  ADD CONSTRAINT `platform_print_technology_color_group_localized_ibfk_1` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`),
  ADD CONSTRAINT `platform_print_technology_color_group_localized_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `platform_print_technology_color_group` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_technology_input_preset`
--
ALTER TABLE `platform_print_technology_input_preset`
  ADD CONSTRAINT `platform_print_technology_input_preset_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`),
  ADD CONSTRAINT `platform_print_technology_input_preset_ibfk_10` FOREIGN KEY (`upload_id`) REFERENCES `platform_print_technology_input_upload` (`id`),
  ADD CONSTRAINT `platform_print_technology_input_preset_ibfk_2` FOREIGN KEY (`motive_collection_id`) REFERENCES `platform_print_technology_input_motive_collection` (`id`),
  ADD CONSTRAINT `platform_print_technology_input_preset_ibfk_3` FOREIGN KEY (`text_id`) REFERENCES `platform_print_technology_input_text` (`id`),
  ADD CONSTRAINT `platform_print_technology_input_preset_ibfk_4` FOREIGN KEY (`qrcode_id`) REFERENCES `platform_print_technology_input_qrcode` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_technology_localized`
--
ALTER TABLE `platform_print_technology_localized`
  ADD CONSTRAINT `platform_print_technology_localized_ibfk_1` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`),
  ADD CONSTRAINT `platform_print_technology_localized_ibfk_2` FOREIGN KEY (`technology_id`) REFERENCES `platform_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_technology_preview_preset`
--
ALTER TABLE `platform_print_technology_preview_preset`
  ADD CONSTRAINT `platform_print_technology_preview_preset_ibfk_1` FOREIGN KEY (`blend_mode_id`) REFERENCES `platform_print_technology_preview_blend_mode` (`id`),
  ADD CONSTRAINT `platform_print_technology_preview_preset_ibfk_2` FOREIGN KEY (`drop_shadow_id`) REFERENCES `platform_print_technology_preview_drop_shadow` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_technology_price_bitmap`
--
ALTER TABLE `platform_print_technology_price_bitmap`
  ADD CONSTRAINT `platform_print_technology_price_bitmap_ibfk_1` FOREIGN KEY (`print_technology_id`) REFERENCES `platform_print_technology` (`id`),
  ADD CONSTRAINT `platform_print_technology_price_bitmap_ibfk_2` FOREIGN KEY (`country_id`) REFERENCES `platform_country` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_technology_price_color`
--
ALTER TABLE `platform_print_technology_price_color`
  ADD CONSTRAINT `platform_print_technology_price_color_ibfk_3` FOREIGN KEY (`country_id`) REFERENCES `platform_country` (`id`),
  ADD CONSTRAINT `platform_print_technology_price_color_ibfk_4` FOREIGN KEY (`assigned_print_color_id`) REFERENCES `platform_print_technology_assigned_color` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_technology_price_const`
--
ALTER TABLE `platform_print_technology_price_const`
  ADD CONSTRAINT `platform_print_technology_price_const_ibfk_1` FOREIGN KEY (`print_technology_id`) REFERENCES `platform_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_technology_price_const_value`
--
ALTER TABLE `platform_print_technology_price_const_value`
  ADD CONSTRAINT `platform_print_technology_price_const_value_ibfk_1` FOREIGN KEY (`const_id`) REFERENCES `platform_print_technology_price_const` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_technology_price_labor_cost`
--
ALTER TABLE `platform_print_technology_price_labor_cost`
  ADD CONSTRAINT `platform_print_technology_price_labor_cost_ibfk_1` FOREIGN KEY (`print_technology_id`) REFERENCES `platform_print_technology` (`id`),
  ADD CONSTRAINT `platform_print_technology_price_labor_cost_ibfk_2` FOREIGN KEY (`country_id`) REFERENCES `platform_country` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_technology_price_setup_cost`
--
ALTER TABLE `platform_print_technology_price_setup_cost`
  ADD CONSTRAINT `platform_print_technology_price_setup_cost_ibfk_1` FOREIGN KEY (`print_technology_id`) REFERENCES `platform_print_technology` (`id`),
  ADD CONSTRAINT `platform_print_technology_price_setup_cost_ibfk_2` FOREIGN KEY (`country_id`) REFERENCES `platform_country` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_technology_price_text`
--
ALTER TABLE `platform_print_technology_price_text`
  ADD CONSTRAINT `platform_print_technology_price_text_ibfk_1` FOREIGN KEY (`print_technology_id`) REFERENCES `platform_print_technology` (`id`),
  ADD CONSTRAINT `platform_print_technology_price_text_ibfk_2` FOREIGN KEY (`font_id`) REFERENCES `platform_font` (`id`),
  ADD CONSTRAINT `platform_print_technology_price_text_ibfk_3` FOREIGN KEY (`country_id`) REFERENCES `platform_country` (`id`);

--
-- Obmedzenie pre tabuľku `platform_print_technology_profile`
--
ALTER TABLE `platform_print_technology_profile`
  ADD CONSTRAINT `platform_print_technology_profile_ibfk_1` FOREIGN KEY (`white_profile_id`) REFERENCES `platform_print_profile` (`id`),
  ADD CONSTRAINT `platform_print_technology_profile_ibfk_2` FOREIGN KEY (`colour_profile_id`) REFERENCES `platform_print_profile` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product`
--
ALTER TABLE `platform_product`
  ADD CONSTRAINT `platform_product_ibfk_10` FOREIGN KEY (`type_id`) REFERENCES `platform_product_type` (`id`),
  ADD CONSTRAINT `platform_product_ibfk_13` FOREIGN KEY (`shop_id`) REFERENCES `platform_shop` (`id`),
  ADD CONSTRAINT `platform_product_ibfk_14` FOREIGN KEY (`pool_id`) REFERENCES `platform_product_pool` (`id`);

--
-- Obmedzenie pre tabuľku `platform_production_display_addon_property`
--
ALTER TABLE `platform_production_display_addon_property`
  ADD CONSTRAINT `platform_production_display_addon_property_ibfk_1` FOREIGN KEY (`print_technology_id`) REFERENCES `platform_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `platform_production_display_module_property`
--
ALTER TABLE `platform_production_display_module_property`
  ADD CONSTRAINT `platform_production_display_module_property_ibfk_1` FOREIGN KEY (`display_module_id`) REFERENCES `platform_production_display_module` (`id`);

--
-- Obmedzenie pre tabuľku `platform_production_file_addon_property`
--
ALTER TABLE `platform_production_file_addon_property`
  ADD CONSTRAINT `platform_production_file_addon_property_ibfk_1` FOREIGN KEY (`print_technology_id`) REFERENCES `platform_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `platform_production_file_module_property`
--
ALTER TABLE `platform_production_file_module_property`
  ADD CONSTRAINT `platform_production_file_module_property_ibfk_1` FOREIGN KEY (`file_module_id`) REFERENCES `platform_production_file_module` (`id`);

--
-- Obmedzenie pre tabuľku `platform_production_preset`
--
ALTER TABLE `platform_production_preset`
  ADD CONSTRAINT `platform_production_preset_ibfk_1` FOREIGN KEY (`module_file_id`) REFERENCES `platform_production_file_module` (`id`),
  ADD CONSTRAINT `platform_production_preset_ibfk_10` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`),
  ADD CONSTRAINT `platform_production_preset_ibfk_2` FOREIGN KEY (`module_display_id`) REFERENCES `platform_production_display_module` (`id`),
  ADD CONSTRAINT `platform_production_preset_ibfk_3` FOREIGN KEY (`module_transformer_id`) REFERENCES `platform_production_transformer_module` (`id`),
  ADD CONSTRAINT `platform_production_preset_ibfk_4` FOREIGN KEY (`module_response_id`) REFERENCES `platform_production_response_module` (`id`);

--
-- Obmedzenie pre tabuľku `platform_production_preset_display_property`
--
ALTER TABLE `platform_production_preset_display_property`
  ADD CONSTRAINT `platform_production_preset_display_property_ibfk_3` FOREIGN KEY (`preset_id`) REFERENCES `platform_production_preset` (`id`);

--
-- Obmedzenie pre tabuľku `platform_production_preset_file_property`
--
ALTER TABLE `platform_production_preset_file_property`
  ADD CONSTRAINT `platform_production_preset_file_property_ibfk_3` FOREIGN KEY (`preset_id`) REFERENCES `platform_production_preset` (`id`);

--
-- Obmedzenie pre tabuľku `platform_production_preset_response_property`
--
ALTER TABLE `platform_production_preset_response_property`
  ADD CONSTRAINT `platform_production_preset_response_property_ibfk_3` FOREIGN KEY (`preset_id`) REFERENCES `platform_production_preset` (`id`);

--
-- Obmedzenie pre tabuľku `platform_production_preset_transformer_property`
--
ALTER TABLE `platform_production_preset_transformer_property`
  ADD CONSTRAINT `platform_production_preset_transformer_property_ibfk_3` FOREIGN KEY (`preset_id`) REFERENCES `platform_production_preset` (`id`);

--
-- Obmedzenie pre tabuľku `platform_production_response_addon_property`
--
ALTER TABLE `platform_production_response_addon_property`
  ADD CONSTRAINT `platform_production_response_addon_property_ibfk_1` FOREIGN KEY (`print_technology_id`) REFERENCES `platform_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `platform_production_response_module_property`
--
ALTER TABLE `platform_production_response_module_property`
  ADD CONSTRAINT `platform_production_response_module_property_ibfk_1` FOREIGN KEY (`response_module_id`) REFERENCES `platform_production_response_module` (`id`);

--
-- Obmedzenie pre tabuľku `platform_production_transformer_addon_property`
--
ALTER TABLE `platform_production_transformer_addon_property`
  ADD CONSTRAINT `platform_production_transformer_addon_property_ibfk_1` FOREIGN KEY (`print_technology_id`) REFERENCES `platform_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `platform_production_transformer_module_property`
--
ALTER TABLE `platform_production_transformer_module_property`
  ADD CONSTRAINT `platform_production_transformer_module_property_ibfk_1` FOREIGN KEY (`transformer_module_id`) REFERENCES `platform_production_transformer_module` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_area`
--
ALTER TABLE `platform_product_area`
  ADD CONSTRAINT `platform_product_area_ibfk_3` FOREIGN KEY (`boundary_geom_id`) REFERENCES `platform_product_area_geom` (`id`),
  ADD CONSTRAINT `platform_product_area_ibfk_4` FOREIGN KEY (`mask_geom_id`) REFERENCES `platform_product_area_geom` (`id`),
  ADD CONSTRAINT `platform_product_area_ibfk_5` FOREIGN KEY (`assigned_view_id`) REFERENCES `platform_product_assigned_view` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_area_geom_elipse`
--
ALTER TABLE `platform_product_area_geom_elipse`
  ADD CONSTRAINT `platform_product_area_geom_elipse_ibfk_1` FOREIGN KEY (`geom_id`) REFERENCES `platform_product_area_geom` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_area_geom_rect`
--
ALTER TABLE `platform_product_area_geom_rect`
  ADD CONSTRAINT `platform_product_area_geom_rect_ibfk_1` FOREIGN KEY (`geom_id`) REFERENCES `platform_product_area_geom` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_area_geom_spline`
--
ALTER TABLE `platform_product_area_geom_spline`
  ADD CONSTRAINT `platform_product_area_geom_spline_ibfk_1` FOREIGN KEY (`geom_id`) REFERENCES `platform_product_area_geom` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_area_real_size`
--
ALTER TABLE `platform_product_area_real_size`
  ADD CONSTRAINT `platform_product_area_real_size_ibfk_1` FOREIGN KEY (`product_area_id`) REFERENCES `platform_product_area` (`id`),
  ADD CONSTRAINT `platform_product_area_real_size_ibfk_3` FOREIGN KEY (`assigned_size_id`) REFERENCES `platform_product_assigned_size` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_area_snap`
--
ALTER TABLE `platform_product_area_snap`
  ADD CONSTRAINT `platform_product_area_snap_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `platform_product_area` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_assigned_attribute_list_item`
--
ALTER TABLE `platform_product_assigned_attribute_list_item`
  ADD CONSTRAINT `platform_product_assigned_attribute_list_item_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`),
  ADD CONSTRAINT `platform_product_assigned_attribute_list_item_ibfk_2` FOREIGN KEY (`attribute_list_item_id`) REFERENCES `platform_product_type_attribute_list_item` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_assigned_color`
--
ALTER TABLE `platform_product_assigned_color`
  ADD CONSTRAINT `platform_product_assigned_color_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`),
  ADD CONSTRAINT `platform_product_assigned_color_ibfk_2` FOREIGN KEY (`color_id`) REFERENCES `platform_product_color` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_assigned_factory`
--
ALTER TABLE `platform_product_assigned_factory`
  ADD CONSTRAINT `platform_product_assigned_factory_ibfk_1` FOREIGN KEY (`factory_assigned_id`) REFERENCES `platform_factory_assigned` (`id`),
  ADD CONSTRAINT `platform_product_assigned_factory_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_assigned_print_technology`
--
ALTER TABLE `platform_product_assigned_print_technology`
  ADD CONSTRAINT `platform_product_assigned_print_technology_ibfk_4` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`),
  ADD CONSTRAINT `platform_product_assigned_print_technology_ibfk_5` FOREIGN KEY (`product_type_print_technology_id`) REFERENCES `platform_product_type_assigned_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_assigned_print_technology_brother`
--
ALTER TABLE `platform_product_assigned_print_technology_brother`
  ADD CONSTRAINT `platform_product_assigned_print_technology_brother_ibfk_1` FOREIGN KEY (`product_print_technology_id`) REFERENCES `platform_product_assigned_print_technology` (`id`),
  ADD CONSTRAINT `platform_product_assigned_print_technology_brother_ibfk_2` FOREIGN KEY (`assigned_color_id`) REFERENCES `platform_product_assigned_color` (`id`),
  ADD CONSTRAINT `platform_product_assigned_print_technology_brother_ibfk_3` FOREIGN KEY (`assigned_view_id`) REFERENCES `platform_product_assigned_view` (`id`),
  ADD CONSTRAINT `platform_product_assigned_print_technology_brother_ibfk_4` FOREIGN KEY (`assigned_size_id`) REFERENCES `platform_product_assigned_size` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_assigned_print_technology_color`
--
ALTER TABLE `platform_product_assigned_print_technology_color`
  ADD CONSTRAINT `platform_product_assigned_print_technology_color_ibfk_1` FOREIGN KEY (`product_print_technology_id`) REFERENCES `platform_product_assigned_print_technology` (`id`),
  ADD CONSTRAINT `platform_product_assigned_print_technology_color_ibfk_2` FOREIGN KEY (`color_value_id`) REFERENCES `platform_product_color_value` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_assigned_print_technology_general`
--
ALTER TABLE `platform_product_assigned_print_technology_general`
  ADD CONSTRAINT `platform_product_assigned_print_technology_general_ibfk_1` FOREIGN KEY (`assigned_view_id`) REFERENCES `platform_product_assigned_view` (`id`),
  ADD CONSTRAINT `platform_product_assigned_print_technology_general_ibfk_2` FOREIGN KEY (`assigned_color_id`) REFERENCES `platform_product_assigned_color` (`id`),
  ADD CONSTRAINT `platform_product_assigned_print_technology_general_ibfk_3` FOREIGN KEY (`product_print_technology_id`) REFERENCES `platform_product_assigned_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_assigned_print_technology_kornit`
--
ALTER TABLE `platform_product_assigned_print_technology_kornit`
  ADD CONSTRAINT `platform_product_assigned_print_technology_kornit_ibfk_10` FOREIGN KEY (`product_print_technology_id`) REFERENCES `platform_product_assigned_print_technology` (`id`),
  ADD CONSTRAINT `platform_product_assigned_print_technology_kornit_ibfk_12` FOREIGN KEY (`assigned_size_id`) REFERENCES `platform_product_assigned_size` (`id`),
  ADD CONSTRAINT `platform_product_assigned_print_technology_kornit_ibfk_13` FOREIGN KEY (`media_name_id`) REFERENCES `platform_kornit_media_name` (`id`),
  ADD CONSTRAINT `platform_product_assigned_print_technology_kornit_ibfk_14` FOREIGN KEY (`assigned_color_id`) REFERENCES `platform_product_assigned_color` (`id`),
  ADD CONSTRAINT `platform_product_assigned_print_technology_kornit_ibfk_15` FOREIGN KEY (`assigned_view_id`) REFERENCES `platform_product_assigned_view` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_assigned_print_technology_media`
--
ALTER TABLE `platform_product_assigned_print_technology_media`
  ADD CONSTRAINT `platform_product_assigned_print_technology_media_ibfk_1` FOREIGN KEY (`assigned_view_id`) REFERENCES `platform_product_assigned_view` (`id`),
  ADD CONSTRAINT `platform_product_assigned_print_technology_media_ibfk_10` FOREIGN KEY (`assigned_media_size_id`) REFERENCES `platform_print_technology_assigned_media` (`id`),
  ADD CONSTRAINT `platform_product_assigned_print_technology_media_ibfk_4` FOREIGN KEY (`product_print_technology_id`) REFERENCES `platform_product_assigned_print_technology` (`id`),
  ADD CONSTRAINT `platform_product_assigned_print_technology_media_ibfk_5` FOREIGN KEY (`assigned_size_id`) REFERENCES `platform_product_assigned_size` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_assigned_size`
--
ALTER TABLE `platform_product_assigned_size`
  ADD CONSTRAINT `platform_product_assigned_size_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`),
  ADD CONSTRAINT `platform_product_assigned_size_ibfk_2` FOREIGN KEY (`size_id`) REFERENCES `platform_product_size` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_assigned_sticker`
--
ALTER TABLE `platform_product_assigned_sticker`
  ADD CONSTRAINT `platform_product_assigned_sticker_ibfk_1` FOREIGN KEY (`assigned_view_id`) REFERENCES `platform_product_assigned_view` (`id`),
  ADD CONSTRAINT `platform_product_assigned_sticker_ibfk_2` FOREIGN KEY (`sticker_id`) REFERENCES `platform_sticker` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_assigned_view`
--
ALTER TABLE `platform_product_assigned_view`
  ADD CONSTRAINT `platform_product_assigned_view_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`),
  ADD CONSTRAINT `platform_product_assigned_view_ibfk_10` FOREIGN KEY (`template_id`) REFERENCES `platform_product_to_template` (`id`),
  ADD CONSTRAINT `platform_product_assigned_view_ibfk_2` FOREIGN KEY (`view_id`) REFERENCES `platform_product_view` (`id`),
  ADD CONSTRAINT `platform_product_assigned_view_ibfk_4` FOREIGN KEY (`swf_id`) REFERENCES `platform_product_swf` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_assigned_view_color`
--
ALTER TABLE `platform_product_assigned_view_color`
  ADD CONSTRAINT `platform_product_assigned_view_color_ibfk_10` FOREIGN KEY (`image_id`) REFERENCES `platform_product_image` (`id`),
  ADD CONSTRAINT `platform_product_assigned_view_color_ibfk_11` FOREIGN KEY (`assigned_view_id`) REFERENCES `platform_product_assigned_view` (`id`),
  ADD CONSTRAINT `platform_product_assigned_view_color_ibfk_12` FOREIGN KEY (`assigned_color_id`) REFERENCES `platform_product_assigned_color` (`id`),
  ADD CONSTRAINT `platform_product_assigned_view_color_ibfk_15` FOREIGN KEY (`default_color_value_id`) REFERENCES `platform_product_color_value` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_assigned_view_snap`
--
ALTER TABLE `platform_product_assigned_view_snap`
  ADD CONSTRAINT `platform_product_assigned_view_snap_ibfk_1` FOREIGN KEY (`assigned_view_id`) REFERENCES `platform_product_assigned_view` (`id`),
  ADD CONSTRAINT `platform_product_assigned_view_snap_ibfk_2` FOREIGN KEY (`snap_view_id`) REFERENCES `platform_product_assigned_view` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_category`
--
ALTER TABLE `platform_product_category`
  ADD CONSTRAINT `platform_product_category_ibfk_3` FOREIGN KEY (`parent_category_id`) REFERENCES `platform_product_category` (`id`),
  ADD CONSTRAINT `platform_product_category_ibfk_5` FOREIGN KEY (`shop_id`) REFERENCES `platform_shop` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_category_localized`
--
ALTER TABLE `platform_product_category_localized`
  ADD CONSTRAINT `platform_product_category_localized_ibfk_1` FOREIGN KEY (`product_category_id`) REFERENCES `platform_product_category` (`id`),
  ADD CONSTRAINT `platform_product_category_localized_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_category_margin`
--
ALTER TABLE `platform_product_category_margin`
  ADD CONSTRAINT `platform_product_category_margin_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `platform_country` (`id`),
  ADD CONSTRAINT `platform_product_category_margin_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `platform_product_category` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_colision`
--
ALTER TABLE `platform_product_colision`
  ADD CONSTRAINT `platform_product_colision_ibfk_1` FOREIGN KEY (`assigned_view_id`) REFERENCES `platform_product_assigned_view` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_color`
--
ALTER TABLE `platform_product_color`
  ADD CONSTRAINT `platform_product_color_ibfk_1` FOREIGN KEY (`pool_id`) REFERENCES `platform_product_color_pool` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_color_localized`
--
ALTER TABLE `platform_product_color_localized`
  ADD CONSTRAINT `platform_product_color_localized_ibfk_1` FOREIGN KEY (`product_color_id`) REFERENCES `platform_product_color` (`id`),
  ADD CONSTRAINT `platform_product_color_localized_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_color_pool`
--
ALTER TABLE `platform_product_color_pool`
  ADD CONSTRAINT `platform_product_color_pool_ibfk_1` FOREIGN KEY (`product_type_id`) REFERENCES `platform_product_type` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_color_value`
--
ALTER TABLE `platform_product_color_value`
  ADD CONSTRAINT `platform_product_color_value_ibfk_1` FOREIGN KEY (`product_color_id`) REFERENCES `platform_product_color` (`id`),
  ADD CONSTRAINT `platform_product_color_value_ibfk_3` FOREIGN KEY (`reference_id`) REFERENCES `platform_product_color_value` (`id`),
  ADD CONSTRAINT `platform_product_color_value_ibfk_4` FOREIGN KEY (`texture_id`) REFERENCES `platform_color_texture` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_connection`
--
ALTER TABLE `platform_product_connection`
  ADD CONSTRAINT `platform_product_connection.connector_id` FOREIGN KEY (`connector_id`) REFERENCES `platform_shop_connection` (`id`),
  ADD CONSTRAINT `platform_product_connection.product_id` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_group`
--
ALTER TABLE `platform_product_group`
  ADD CONSTRAINT `platform_product_group_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_group_attribute`
--
ALTER TABLE `platform_product_group_attribute`
  ADD CONSTRAINT `platform_product_group_attribute_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_group_attribute_localized`
--
ALTER TABLE `platform_product_group_attribute_localized`
  ADD CONSTRAINT `platform_product_group_attribute_localized_ibfk_1` FOREIGN KEY (`attribute_id`) REFERENCES `platform_product_group_attribute` (`id`),
  ADD CONSTRAINT `platform_product_group_attribute_localized_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_image_connection`
--
ALTER TABLE `platform_product_image_connection`
  ADD CONSTRAINT `platform_product_image_connection.connector_id` FOREIGN KEY (`connector_id`) REFERENCES `platform_shop_connection` (`id`),
  ADD CONSTRAINT `platform_product_image_connection.product_id` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_localized`
--
ALTER TABLE `platform_product_localized`
  ADD CONSTRAINT `platform_product_localized_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`),
  ADD CONSTRAINT `platform_product_localized_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_photo_character`
--
ALTER TABLE `platform_product_photo_character`
  ADD CONSTRAINT `platform_product_photo_character_ibfk_1` FOREIGN KEY (`character_id`) REFERENCES `platform_photo_character` (`id`),
  ADD CONSTRAINT `platform_product_photo_character_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `platform_photo_image` (`id`),
  ADD CONSTRAINT `platform_product_photo_character_ibfk_3` FOREIGN KEY (`assigned_size_id`) REFERENCES `platform_product_assigned_size` (`id`),
  ADD CONSTRAINT `platform_product_photo_character_ibfk_4` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_photo_detail`
--
ALTER TABLE `platform_product_photo_detail`
  ADD CONSTRAINT `platform_product_photo_detail_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`),
  ADD CONSTRAINT `platform_product_photo_detail_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `platform_photo_image` (`id`),
  ADD CONSTRAINT `platform_product_photo_detail_ibfk_3` FOREIGN KEY (`assigned_color_id`) REFERENCES `platform_product_assigned_color` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_photo_measure`
--
ALTER TABLE `platform_product_photo_measure`
  ADD CONSTRAINT `platform_product_photo_measure_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`),
  ADD CONSTRAINT `platform_product_photo_measure_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `platform_photo_image` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_pool`
--
ALTER TABLE `platform_product_pool`
  ADD CONSTRAINT `platform_product_pool_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `platform_shop` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_pool_property`
--
ALTER TABLE `platform_product_pool_property`
  ADD CONSTRAINT `platform_product_pool_property_ibfk_1` FOREIGN KEY (`pool_id`) REFERENCES `platform_product_pool` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_price`
--
ALTER TABLE `platform_product_price`
  ADD CONSTRAINT `platform_product_price_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`),
  ADD CONSTRAINT `platform_product_price_ibfk_2` FOREIGN KEY (`country_id`) REFERENCES `platform_country` (`id`),
  ADD CONSTRAINT `platform_product_price_ibfk_5` FOREIGN KEY (`color_id`) REFERENCES `platform_product_assigned_color` (`id`),
  ADD CONSTRAINT `platform_product_price_ibfk_6` FOREIGN KEY (`size_id`) REFERENCES `platform_product_assigned_size` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_size`
--
ALTER TABLE `platform_product_size`
  ADD CONSTRAINT `platform_product_size_ibfk_1` FOREIGN KEY (`pool_id`) REFERENCES `platform_product_size_pool` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_size_localized`
--
ALTER TABLE `platform_product_size_localized`
  ADD CONSTRAINT `platform_product_size_localized_ibfk_1` FOREIGN KEY (`size_id`) REFERENCES `platform_product_size` (`id`),
  ADD CONSTRAINT `platform_product_size_localized_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_size_pool`
--
ALTER TABLE `platform_product_size_pool`
  ADD CONSTRAINT `platform_product_size_pool_ibfk_1` FOREIGN KEY (`product_type_id`) REFERENCES `platform_product_type` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_sku`
--
ALTER TABLE `platform_product_sku`
  ADD CONSTRAINT `platform_product_sku_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`),
  ADD CONSTRAINT `platform_product_sku_ibfk_3` FOREIGN KEY (`assigned_color_id`) REFERENCES `platform_product_assigned_color` (`id`),
  ADD CONSTRAINT `platform_product_sku_ibfk_4` FOREIGN KEY (`assigned_size_id`) REFERENCES `platform_product_assigned_size` (`id`),
  ADD CONSTRAINT `platform_product_sku_ibfk_6` FOREIGN KEY (`stock_id`) REFERENCES `platform_stock_item` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_to_template`
--
ALTER TABLE `platform_product_to_template`
  ADD CONSTRAINT `platform_product_to_template_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `platform_product_area` (`id`),
  ADD CONSTRAINT `platform_product_to_template_ibfk_2` FOREIGN KEY (`template_id`) REFERENCES `platform_template` (`id`),
  ADD CONSTRAINT `platform_product_to_template_ibfk_3` FOREIGN KEY (`assigned_print_technology_id`) REFERENCES `platform_product_assigned_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_type`
--
ALTER TABLE `platform_product_type`
  ADD CONSTRAINT `platform_product_type_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_type_assigned_print_technology`
--
ALTER TABLE `platform_product_type_assigned_print_technology`
  ADD CONSTRAINT `platform_product_type_assigned_print_technology_ibfk_2` FOREIGN KEY (`product_type_id`) REFERENCES `platform_product_type` (`id`),
  ADD CONSTRAINT `platform_product_type_assigned_print_technology_ibfk_3` FOREIGN KEY (`print_technology_id`) REFERENCES `platform_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_type_attribute_list`
--
ALTER TABLE `platform_product_type_attribute_list`
  ADD CONSTRAINT `platform_product_type_attribute_list_ibfk_1` FOREIGN KEY (`product_type_id`) REFERENCES `platform_product_type` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_type_attribute_list_item`
--
ALTER TABLE `platform_product_type_attribute_list_item`
  ADD CONSTRAINT `platform_product_type_attribute_list_item_ibfk_1` FOREIGN KEY (`list_id`) REFERENCES `platform_product_type_attribute_list` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_type_attribute_list_item_localized`
--
ALTER TABLE `platform_product_type_attribute_list_item_localized`
  ADD CONSTRAINT `platform_product_type_attribute_list_item_localized_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `platform_product_type_attribute_list_item` (`id`),
  ADD CONSTRAINT `platform_product_type_attribute_list_item_localized_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`),
  ADD CONSTRAINT `platform_product_type_attribute_list_item_localized_ibfk_4` FOREIGN KEY (`picture_id`) REFERENCES `platform_product_type_attribute_list_item_picture` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_type_attribute_list_localized`
--
ALTER TABLE `platform_product_type_attribute_list_localized`
  ADD CONSTRAINT `platform_product_type_attribute_list_localized_ibfk_1` FOREIGN KEY (`list_id`) REFERENCES `platform_product_type_attribute_list` (`id`),
  ADD CONSTRAINT `platform_product_type_attribute_list_localized_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_type_attribute_list_x_flag`
--
ALTER TABLE `platform_product_type_attribute_list_x_flag`
  ADD CONSTRAINT `platform_product_type_attribute_list_x_flag_ibfk_1` FOREIGN KEY (`attribute_list_id`) REFERENCES `platform_product_type_attribute_list` (`id`),
  ADD CONSTRAINT `platform_product_type_attribute_list_x_flag_ibfk_2` FOREIGN KEY (`attribute_flag_id`) REFERENCES `platform_product_type_attribute_flag` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_type_localized`
--
ALTER TABLE `platform_product_type_localized`
  ADD CONSTRAINT `platform_product_type_localized_ibfk_1` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`),
  ADD CONSTRAINT `platform_product_type_localized_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `platform_product_type` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_variant_connection`
--
ALTER TABLE `platform_product_variant_connection`
  ADD CONSTRAINT `platform_product_variant_connection.assigned_color_id` FOREIGN KEY (`assigned_color_id`) REFERENCES `platform_product_assigned_color` (`id`),
  ADD CONSTRAINT `platform_product_variant_connection.assigned_size_id` FOREIGN KEY (`assigned_size_id`) REFERENCES `platform_product_assigned_size` (`id`),
  ADD CONSTRAINT `platform_product_variant_connection.connector_id` FOREIGN KEY (`connector_id`) REFERENCES `platform_shop_connection` (`id`),
  ADD CONSTRAINT `platform_product_variant_connection.product_id` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_view`
--
ALTER TABLE `platform_product_view`
  ADD CONSTRAINT `platform_product_view_ibfk_2` FOREIGN KEY (`product_type_id`) REFERENCES `platform_product_type` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_view_localized`
--
ALTER TABLE `platform_product_view_localized`
  ADD CONSTRAINT `platform_product_view_localized_ibfk_1` FOREIGN KEY (`view_id`) REFERENCES `platform_product_view` (`id`),
  ADD CONSTRAINT `platform_product_view_localized_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_x_group`
--
ALTER TABLE `platform_product_x_group`
  ADD CONSTRAINT `platform_product_x_group_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`),
  ADD CONSTRAINT `platform_product_x_group_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `platform_product_group` (`id`),
  ADD CONSTRAINT `platform_product_x_group_ibfk_3` FOREIGN KEY (`attribute_id`) REFERENCES `platform_product_group_attribute` (`id`);

--
-- Obmedzenie pre tabuľku `platform_product_x_product_category`
--
ALTER TABLE `platform_product_x_product_category`
  ADD CONSTRAINT `platform_product_x_product_category_ibfk_1` FOREIGN KEY (`product_category_id`) REFERENCES `platform_product_category` (`id`),
  ADD CONSTRAINT `platform_product_x_product_category_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`);

--
-- Obmedzenie pre tabuľku `platform_shipping_module`
--
ALTER TABLE `platform_shipping_module`
  ADD CONSTRAINT `platform_shipping_module_account_ID` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`),
  ADD CONSTRAINT `platform_shipping_module_module_type_ID` FOREIGN KEY (`module_type`) REFERENCES `platform_shipping_module_type` (`id`);

--
-- Obmedzenie pre tabuľku `platform_shipping_module_provider`
--
ALTER TABLE `platform_shipping_module_provider`
  ADD CONSTRAINT `platform_shipping_module_provider_ibfk_1` FOREIGN KEY (`module_type`) REFERENCES `platform_shipping_module_type` (`id`);

--
-- Obmedzenie pre tabuľku `platform_shop`
--
ALTER TABLE `platform_shop`
  ADD CONSTRAINT `logo` FOREIGN KEY (`logo_id`) REFERENCES `platform_company_logo` (`id`),
  ADD CONSTRAINT `platform_shop_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`),
  ADD CONSTRAINT `platform_shop_ibfk_2` FOREIGN KEY (`default_product_id`) REFERENCES `platform_product` (`id`),
  ADD CONSTRAINT `platform_shop_ibfk_5` FOREIGN KEY (`creator_config_id`) REFERENCES `platform_shop_config_creator` (`id`);

--
-- Obmedzenie pre tabuľku `platform_shopify_callback`
--
ALTER TABLE `platform_shopify_callback`
  ADD CONSTRAINT `shopify_callback_connector_ID` FOREIGN KEY (`connector_id`) REFERENCES `platform_shop_connection` (`id`);

--
-- Obmedzenie pre tabuľku `platform_shop_address`
--
ALTER TABLE `platform_shop_address`
  ADD CONSTRAINT `shop_address_address_ID` FOREIGN KEY (`address_id`) REFERENCES `platform_address` (`id`),
  ADD CONSTRAINT `shop_address_country_ID` FOREIGN KEY (`country_id`) REFERENCES `platform_country` (`id`),
  ADD CONSTRAINT `shop_address_shop_ID` FOREIGN KEY (`shop_id`) REFERENCES `platform_shop` (`id`);

--
-- Obmedzenie pre tabuľku `platform_shop_callback`
--
ALTER TABLE `platform_shop_callback`
  ADD CONSTRAINT `platform_shop_callback_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `platform_shop` (`id`);

--
-- Obmedzenie pre tabuľku `platform_shop_config_creator`
--
ALTER TABLE `platform_shop_config_creator`
  ADD CONSTRAINT `platform_shop_config_creator_ibfk_1` FOREIGN KEY (`font_data_id`) REFERENCES `platform_font_data` (`id`),
  ADD CONSTRAINT `platform_shop_config_creator_ibfk_2` FOREIGN KEY (`creator_theme_id`) REFERENCES `platform_creator_theme` (`id`),
  ADD CONSTRAINT `platform_shop_config_creator_resource.gfxTextIcoUp` FOREIGN KEY (`addNewMotiveButton_gfxTextIcoUp`) REFERENCES `platform_shop_config_creator_resource` (`id`),
  ADD CONSTRAINT `platform_shop_config_creator_resource.motiveIcoUp` FOREIGN KEY (`addNewMotiveButton_motiveIcoUp`) REFERENCES `platform_shop_config_creator_resource` (`id`),
  ADD CONSTRAINT `platform_shop_config_creator_resource.textIcoUp` FOREIGN KEY (`addNewMotiveButton_textIcoUp`) REFERENCES `platform_shop_config_creator_resource` (`id`),
  ADD CONSTRAINT `platform_shop_config_creator_resource.uploadIcoUp` FOREIGN KEY (`addNewMotiveButton_uploadIcoUp`) REFERENCES `platform_shop_config_creator_resource` (`id`);

--
-- Obmedzenie pre tabuľku `platform_shop_connection`
--
ALTER TABLE `platform_shop_connection`
  ADD CONSTRAINT `platform_shop_connection_ibfk_1` FOREIGN KEY (`hash_id`) REFERENCES `secure_hash` (`id`),
  ADD CONSTRAINT `platform_shop_connection_ibfk_2` FOREIGN KEY (`oauth_id`) REFERENCES `secure_oauth` (`id`),
  ADD CONSTRAINT `shop_connection.app_user_id` FOREIGN KEY (`app_user_id`) REFERENCES `platform_user` (`id`),
  ADD CONSTRAINT `shop_connection_country_id` FOREIGN KEY (`country_id`) REFERENCES `platform_country` (`id`),
  ADD CONSTRAINT `shop_connection_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`),
  ADD CONSTRAINT `shop_connection_shop_id` FOREIGN KEY (`shop_id`) REFERENCES `platform_shop` (`id`);

--
-- Obmedzenie pre tabuľku `platform_shop_connection_property`
--
ALTER TABLE `platform_shop_connection_property`
  ADD CONSTRAINT `platform_shop_connection_property_ibfk_1` FOREIGN KEY (`connector_id`) REFERENCES `platform_shop_connection` (`id`);

--
-- Obmedzenie pre tabuľku `platform_shop_connector_front`
--
ALTER TABLE `platform_shop_connector_front`
  ADD CONSTRAINT `platform_shop_connector_front_ibfk_1` FOREIGN KEY (`connector_id`) REFERENCES `platform_shop_connection` (`id`),
  ADD CONSTRAINT `platform_shop_connector_front_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`),
  ADD CONSTRAINT `platform_shop_connector_front_ibfk_3` FOREIGN KEY (`designed_product_id`) REFERENCES `platform_designed_template_product` (`id`),
  ADD CONSTRAINT `platform_shop_connector_front_ibfk_4` FOREIGN KEY (`connector_id`) REFERENCES `platform_shop_connection` (`id`),
  ADD CONSTRAINT `platform_shop_connector_front_ibfk_5` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`),
  ADD CONSTRAINT `platform_shop_connector_front_ibfk_6` FOREIGN KEY (`designed_product_id`) REFERENCES `platform_designed_template_product` (`id`);

--
-- Obmedzenie pre tabuľku `platform_shop_property`
--
ALTER TABLE `platform_shop_property`
  ADD CONSTRAINT `platform_shop_property_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `platform_shop` (`id`);

--
-- Obmedzenie pre tabuľku `platform_state_source`
--
ALTER TABLE `platform_state_source`
  ADD CONSTRAINT `platform_state_source_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `platform_country_source` (`id`),
  ADD CONSTRAINT `platform_state_source_ibfk_2` FOREIGN KEY (`country_id`) REFERENCES `platform_country_source` (`id`);

--
-- Obmedzenie pre tabuľku `platform_sticker`
--
ALTER TABLE `platform_sticker`
  ADD CONSTRAINT `platform_sticker_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`),
  ADD CONSTRAINT `platform_sticker_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `platform_sticker_image` (`id`);

--
-- Obmedzenie pre tabuľku `platform_stock_item_to_factory`
--
ALTER TABLE `platform_stock_item_to_factory`
  ADD CONSTRAINT `platform_stock_item_to_factory_ibfk_1` FOREIGN KEY (`factory_id`) REFERENCES `platform_factory` (`id`),
  ADD CONSTRAINT `platform_stock_item_to_factory_ibfk_2` FOREIGN KEY (`stock_id`) REFERENCES `platform_stock_item` (`id`);

--
-- Obmedzenie pre tabuľku `platform_stock_supplier`
--
ALTER TABLE `platform_stock_supplier`
  ADD CONSTRAINT `platform_stock_supplier_ibfk_1` FOREIGN KEY (`owner_account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_stock_supplier_x_account`
--
ALTER TABLE `platform_stock_supplier_x_account`
  ADD CONSTRAINT `platform_stock_supplier_x_account_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`),
  ADD CONSTRAINT `platform_stock_supplier_x_account_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `platform_stock_supplier` (`id`);

--
-- Obmedzenie pre tabuľku `platform_stock_to_supplier`
--
ALTER TABLE `platform_stock_to_supplier`
  ADD CONSTRAINT `platform_stock_to_supplier_ibfk_1` FOREIGN KEY (`stock_id`) REFERENCES `platform_stock_item` (`id`),
  ADD CONSTRAINT `platform_stock_to_supplier_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `platform_stock_supplier` (`id`);

--
-- Obmedzenie pre tabuľku `platform_tag`
--
ALTER TABLE `platform_tag`
  ADD CONSTRAINT `platform_tag_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_tag_localized`
--
ALTER TABLE `platform_tag_localized`
  ADD CONSTRAINT `platform_tag_localized_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `platform_tag` (`id`),
  ADD CONSTRAINT `platform_tag_localized_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `platform_language` (`id`);

--
-- Obmedzenie pre tabuľku `platform_template`
--
ALTER TABLE `platform_template`
  ADD CONSTRAINT `platform_template_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_template_svg`
--
ALTER TABLE `platform_template_svg`
  ADD CONSTRAINT `platform_template_svg_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_template_to_factory`
--
ALTER TABLE `platform_template_to_factory`
  ADD CONSTRAINT `platform_template_to_factory_ibfk_1` FOREIGN KEY (`template_id`) REFERENCES `platform_template` (`id`),
  ADD CONSTRAINT `platform_template_to_factory_ibfk_2` FOREIGN KEY (`factory_assigned_id`) REFERENCES `platform_factory_assigned` (`id`),
  ADD CONSTRAINT `platform_template_to_factory_ibfk_3` FOREIGN KEY (`svg_id`) REFERENCES `platform_template_svg` (`id`);

--
-- Obmedzenie pre tabuľku `platform_transformation`
--
ALTER TABLE `platform_transformation`
  ADD CONSTRAINT `platform_transformation_ibfk_1` FOREIGN KEY (`assigned_view_id`) REFERENCES `platform_product_assigned_view` (`id`),
  ADD CONSTRAINT `platform_transformation_ibfk_2` FOREIGN KEY (`product_area_id`) REFERENCES `platform_product_area` (`id`);

--
-- Obmedzenie pre tabuľku `platform_user`
--
ALTER TABLE `platform_user`
  ADD CONSTRAINT `platform_user_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_user_granted_authority`
--
ALTER TABLE `platform_user_granted_authority`
  ADD CONSTRAINT `platform_user_granted_authority_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `platform_user` (`id`),
  ADD CONSTRAINT `platform_user_granted_authority_ibfk_2` FOREIGN KEY (`authority_id`) REFERENCES `platform_user_authority` (`id`);

--
-- Obmedzenie pre tabuľku `platform_user_granted_shop_access`
--
ALTER TABLE `platform_user_granted_shop_access`
  ADD CONSTRAINT `user_granted_shop_access_shop_ID` FOREIGN KEY (`shop_id`) REFERENCES `platform_shop` (`id`),
  ADD CONSTRAINT `user_granted_shop_access_user_ID` FOREIGN KEY (`user_id`) REFERENCES `platform_user` (`id`);

--
-- Obmedzenie pre tabuľku `platform_user_motive`
--
ALTER TABLE `platform_user_motive`
  ADD CONSTRAINT `platform_user_motive_ibfk_1` FOREIGN KEY (`motive_id`) REFERENCES `platform_motive` (`id`);

--
-- Obmedzenie pre tabuľku `platform_vat`
--
ALTER TABLE `platform_vat`
  ADD CONSTRAINT `platform_vat_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `platform_webhook`
--
ALTER TABLE `platform_webhook`
  ADD CONSTRAINT `platform_webhook.shop_id` FOREIGN KEY (`shop_id`) REFERENCES `platform_shop` (`id`);

--
-- Obmedzenie pre tabuľku `platform_webhook_exec`
--
ALTER TABLE `platform_webhook_exec`
  ADD CONSTRAINT `platform_webhook_exec.webhook_id` FOREIGN KEY (`webhook_id`) REFERENCES `platform_webhook` (`id`) ON DELETE CASCADE;

--
-- Obmedzenie pre tabuľku `sharing_importable_print_color`
--
ALTER TABLE `sharing_importable_print_color`
  ADD CONSTRAINT `sharing_importable_print_color_ibfk_1` FOREIGN KEY (`print_color_palette_id`) REFERENCES `platform_print_color_palette` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_importable_print_media`
--
ALTER TABLE `sharing_importable_print_media`
  ADD CONSTRAINT `sharing_importable_print_media_ibfk_1` FOREIGN KEY (`print_media_size_palette_id`) REFERENCES `platform_print_media_size_palette` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_importable_print_technology`
--
ALTER TABLE `sharing_importable_print_technology`
  ADD CONSTRAINT `sharing_importable_print_technology_ibfk_1` FOREIGN KEY (`print_technology_id`) REFERENCES `platform_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_importable_product`
--
ALTER TABLE `sharing_importable_product`
  ADD CONSTRAINT `sharing_importable_product_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `platform_product` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_motive`
--
ALTER TABLE `sharing_imported_motive`
  ADD CONSTRAINT `sharing_imported_motive_ibfk_1` FOREIGN KEY (`source_id`) REFERENCES `platform_motive` (`id`),
  ADD CONSTRAINT `sharing_imported_motive_ibfk_2` FOREIGN KEY (`destination_id`) REFERENCES `platform_motive` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_motive_category`
--
ALTER TABLE `sharing_imported_motive_category`
  ADD CONSTRAINT `sharing_imported_motive_category_ibfk_1` FOREIGN KEY (`source_id`) REFERENCES `platform_motive_category` (`id`),
  ADD CONSTRAINT `sharing_imported_motive_category_ibfk_2` FOREIGN KEY (`destination_id`) REFERENCES `platform_motive_category` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_photo_character`
--
ALTER TABLE `sharing_imported_photo_character`
  ADD CONSTRAINT `sharing_imported_photo_character_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_photo_character` (`id`),
  ADD CONSTRAINT `sharing_imported_photo_character_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_photo_character` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_print_color`
--
ALTER TABLE `sharing_imported_print_color`
  ADD CONSTRAINT `sharing_imported_print_color_ibfk_1` FOREIGN KEY (`src_print_color_id`) REFERENCES `platform_print_color` (`id`),
  ADD CONSTRAINT `sharing_imported_print_color_ibfk_2` FOREIGN KEY (`dst_print_color_id`) REFERENCES `platform_print_color` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_print_color_palette`
--
ALTER TABLE `sharing_imported_print_color_palette`
  ADD CONSTRAINT `sharing_imported_print_color_palette_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_print_color_palette` (`id`),
  ADD CONSTRAINT `sharing_imported_print_color_palette_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_print_color_palette` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_print_media_size`
--
ALTER TABLE `sharing_imported_print_media_size`
  ADD CONSTRAINT `sharing_imported_print_media_size_ibfk_1` FOREIGN KEY (`src_print_media_size_id`) REFERENCES `platform_print_media_size` (`id`),
  ADD CONSTRAINT `sharing_imported_print_media_size_ibfk_2` FOREIGN KEY (`dst_print_media_size_id`) REFERENCES `platform_print_media_size` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_print_technology`
--
ALTER TABLE `sharing_imported_print_technology`
  ADD CONSTRAINT `sharing_imported_print_technology_ibfk_1` FOREIGN KEY (`source_id`) REFERENCES `platform_print_technology` (`id`),
  ADD CONSTRAINT `sharing_imported_print_technology_ibfk_2` FOREIGN KEY (`destination_id`) REFERENCES `platform_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product`
--
ALTER TABLE `sharing_imported_product`
  ADD CONSTRAINT `sharing_imported_product_ibfk_1` FOREIGN KEY (`source_id`) REFERENCES `platform_product` (`id`),
  ADD CONSTRAINT `sharing_imported_product_ibfk_2` FOREIGN KEY (`destination_id`) REFERENCES `platform_product` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_area`
--
ALTER TABLE `sharing_imported_product_area`
  ADD CONSTRAINT `sharing_imported_product_area_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_area` (`id`),
  ADD CONSTRAINT `sharing_imported_product_area_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_area` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_area_real_size`
--
ALTER TABLE `sharing_imported_product_area_real_size`
  ADD CONSTRAINT `sharing_imported_product_area_real_size_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_area_real_size` (`id`),
  ADD CONSTRAINT `sharing_imported_product_area_real_size_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_area_real_size` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_assigned_color`
--
ALTER TABLE `sharing_imported_product_assigned_color`
  ADD CONSTRAINT `sharing_imported_product_assigned_color_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_assigned_color` (`id`),
  ADD CONSTRAINT `sharing_imported_product_assigned_color_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_assigned_color` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_assigned_print_technology`
--
ALTER TABLE `sharing_imported_product_assigned_print_technology`
  ADD CONSTRAINT `sharing_imported_product_assigned_print_technology_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_assigned_print_technology` (`id`),
  ADD CONSTRAINT `sharing_imported_product_assigned_print_technology_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_assigned_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_assigned_size`
--
ALTER TABLE `sharing_imported_product_assigned_size`
  ADD CONSTRAINT `sharing_imported_product_assigned_size_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_assigned_size` (`id`),
  ADD CONSTRAINT `sharing_imported_product_assigned_size_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_assigned_size` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_assigned_sticker`
--
ALTER TABLE `sharing_imported_product_assigned_sticker`
  ADD CONSTRAINT `sharing_imported_product_assigned_sticker_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_assigned_sticker` (`id`),
  ADD CONSTRAINT `sharing_imported_product_assigned_sticker_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_assigned_sticker` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_assigned_view`
--
ALTER TABLE `sharing_imported_product_assigned_view`
  ADD CONSTRAINT `sharing_imported_product_assigned_view_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_assigned_view` (`id`),
  ADD CONSTRAINT `sharing_imported_product_assigned_view_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_assigned_view` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_assigned_view_snap`
--
ALTER TABLE `sharing_imported_product_assigned_view_snap`
  ADD CONSTRAINT `sharing_imported_product_assigned_view_snap_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_assigned_view_snap` (`id`),
  ADD CONSTRAINT `sharing_imported_product_assigned_view_snap_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_assigned_view_snap` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_category`
--
ALTER TABLE `sharing_imported_product_category`
  ADD CONSTRAINT `sharing_imported_product_category_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_category` (`id`),
  ADD CONSTRAINT `sharing_imported_product_category_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_category` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_colision`
--
ALTER TABLE `sharing_imported_product_colision`
  ADD CONSTRAINT `sharing_imported_product_colision_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_colision` (`id`),
  ADD CONSTRAINT `sharing_imported_product_colision_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_colision` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_color`
--
ALTER TABLE `sharing_imported_product_color`
  ADD CONSTRAINT `sharing_imported_product_color_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_color` (`id`),
  ADD CONSTRAINT `sharing_imported_product_color_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_color` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_color_pool`
--
ALTER TABLE `sharing_imported_product_color_pool`
  ADD CONSTRAINT `sharing_imported_product_color_pool_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_color_pool` (`id`),
  ADD CONSTRAINT `sharing_imported_product_color_pool_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_color_pool` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_color_value`
--
ALTER TABLE `sharing_imported_product_color_value`
  ADD CONSTRAINT `sharing_imported_product_color_value_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_color_value` (`id`),
  ADD CONSTRAINT `sharing_imported_product_color_value_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_color_value` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_photo_character`
--
ALTER TABLE `sharing_imported_product_photo_character`
  ADD CONSTRAINT `sharing_imported_product_photo_character_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_photo_character` (`id`),
  ADD CONSTRAINT `sharing_imported_product_photo_character_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_photo_character` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_photo_detail`
--
ALTER TABLE `sharing_imported_product_photo_detail`
  ADD CONSTRAINT `sharing_imported_product_photo_detail_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_photo_detail` (`id`),
  ADD CONSTRAINT `sharing_imported_product_photo_detail_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_photo_detail` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_photo_measure`
--
ALTER TABLE `sharing_imported_product_photo_measure`
  ADD CONSTRAINT `sharing_imported_product_photo_measure_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_photo_measure` (`id`),
  ADD CONSTRAINT `sharing_imported_product_photo_measure_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_photo_measure` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_size`
--
ALTER TABLE `sharing_imported_product_size`
  ADD CONSTRAINT `sharing_imported_product_size_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_size` (`id`),
  ADD CONSTRAINT `sharing_imported_product_size_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_size` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_size_pool`
--
ALTER TABLE `sharing_imported_product_size_pool`
  ADD CONSTRAINT `sharing_imported_product_size_pool_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_size_pool` (`id`),
  ADD CONSTRAINT `sharing_imported_product_size_pool_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_size_pool` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_sku`
--
ALTER TABLE `sharing_imported_product_sku`
  ADD CONSTRAINT `sharing_imported_product_sku_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_sku` (`id`),
  ADD CONSTRAINT `sharing_imported_product_sku_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_sku` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_to_template`
--
ALTER TABLE `sharing_imported_product_to_template`
  ADD CONSTRAINT `sharing_imported_product_to_template_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_to_template` (`id`),
  ADD CONSTRAINT `sharing_imported_product_to_template_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_to_template` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_type`
--
ALTER TABLE `sharing_imported_product_type`
  ADD CONSTRAINT `sharing_imported_product_type_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_type` (`id`),
  ADD CONSTRAINT `sharing_imported_product_type_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_type` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_type_assigned_print_technology`
--
ALTER TABLE `sharing_imported_product_type_assigned_print_technology`
  ADD CONSTRAINT `sharing_imported_product_type_assigned_print_technology_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_type_assigned_print_technology` (`id`),
  ADD CONSTRAINT `sharing_imported_product_type_assigned_print_technology_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_type_assigned_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_type_attribute_list`
--
ALTER TABLE `sharing_imported_product_type_attribute_list`
  ADD CONSTRAINT `sharing_imported_product_type_attribute_list_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_type_attribute_list` (`id`),
  ADD CONSTRAINT `sharing_imported_product_type_attribute_list_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_type_attribute_list` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_type_attribute_list_item`
--
ALTER TABLE `sharing_imported_product_type_attribute_list_item`
  ADD CONSTRAINT `sharing_imported_product_type_attribute_list_item_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_type_attribute_list_item` (`id`),
  ADD CONSTRAINT `sharing_imported_product_type_attribute_list_item_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_type_attribute_list_item` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_product_view`
--
ALTER TABLE `sharing_imported_product_view`
  ADD CONSTRAINT `sharing_imported_product_view_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_product_view` (`id`),
  ADD CONSTRAINT `sharing_imported_product_view_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_product_view` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_sticker`
--
ALTER TABLE `sharing_imported_sticker`
  ADD CONSTRAINT `sharing_imported_sticker_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_sticker` (`id`),
  ADD CONSTRAINT `sharing_imported_sticker_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_sticker` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_template`
--
ALTER TABLE `sharing_imported_template`
  ADD CONSTRAINT `sharing_imported_template_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_template` (`id`),
  ADD CONSTRAINT `sharing_imported_template_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_template` (`id`);

--
-- Obmedzenie pre tabuľku `sharing_imported_transformation`
--
ALTER TABLE `sharing_imported_transformation`
  ADD CONSTRAINT `sharing_imported_transformation_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `platform_transformation` (`id`),
  ADD CONSTRAINT `sharing_imported_transformation_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `platform_transformation` (`id`);

--
-- Obmedzenie pre tabuľku `spod_product_variant`
--
ALTER TABLE `spod_product_variant`
  ADD CONSTRAINT `spod_product_variant.manufacturer_module_id` FOREIGN KEY (`manufacturer_module_id`) REFERENCES `tool_manufacturer_module` (`id`),
  ADD CONSTRAINT `spod_product_variant.shop_id` FOREIGN KEY (`shop_id`) REFERENCES `platform_shop` (`id`);

--
-- Obmedzenie pre tabuľku `spod_remote_order`
--
ALTER TABLE `spod_remote_order`
  ADD CONSTRAINT `spod_remote_order.factory_order_id` FOREIGN KEY (`factory_order_id`) REFERENCES `tool_order` (`id`),
  ADD CONSTRAINT `spod_remote_order.shop_id` FOREIGN KEY (`shop_id`) REFERENCES `platform_shop` (`id`),
  ADD CONSTRAINT `spod_remote_order_.manufacturer_module_id` FOREIGN KEY (`manufacturer_module_id`) REFERENCES `tool_manufacturer_module` (`id`);

--
-- Obmedzenie pre tabuľku `tool_element`
--
ALTER TABLE `tool_element`
  ADD CONSTRAINT `tool_element_ibfk_1` FOREIGN KEY (`platform_element_id`) REFERENCES `platform_design_element` (`id`),
  ADD CONSTRAINT `tool_element_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `tool_product` (`id`),
  ADD CONSTRAINT `tool_element_ibfk_3` FOREIGN KEY (`media_size_id`) REFERENCES `tool_media_size` (`Id`);

--
-- Obmedzenie pre tabuľku `tool_external_endpoint_configuration`
--
ALTER TABLE `tool_external_endpoint_configuration`
  ADD CONSTRAINT `tool_external_endpoint_configuration_ibfk_1` FOREIGN KEY (`secure_basic_id`) REFERENCES `secure_basic` (`id`);

--
-- Obmedzenie pre tabuľku `tool_factory_place`
--
ALTER TABLE `tool_factory_place`
  ADD CONSTRAINT `tool_factory_place_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `tool_factory_place_user_setting`
--
ALTER TABLE `tool_factory_place_user_setting`
  ADD CONSTRAINT `tool_factory_place_user_setting_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `platform_user` (`id`),
  ADD CONSTRAINT `tool_factory_place_user_setting_ibfk_2` FOREIGN KEY (`warehouse_id`) REFERENCES `tool_factory_place` (`id`);

--
-- Obmedzenie pre tabuľku `tool_filter_item`
--
ALTER TABLE `tool_filter_item`
  ADD CONSTRAINT `tool_filter_item_ibfk_1` FOREIGN KEY (`preset_id`) REFERENCES `tool_filter_preset` (`id`);

--
-- Obmedzenie pre tabuľku `tool_filter_preset`
--
ALTER TABLE `tool_filter_preset`
  ADD CONSTRAINT `tool_filter_preset_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`),
  ADD CONSTRAINT `tool_filter_preset_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `platform_user` (`id`);

--
-- Obmedzenie pre tabuľku `tool_gather_record`
--
ALTER TABLE `tool_gather_record`
  ADD CONSTRAINT `tool_gather_record_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `tool_shipping_provider` (`id`),
  ADD CONSTRAINT `tool_gather_record_ibfk_2` FOREIGN KEY (`carrier_id`) REFERENCES `platform_carrier_service` (`id`);

--
-- Obmedzenie pre tabuľku `tool_group`
--
ALTER TABLE `tool_group`
  ADD CONSTRAINT `tool_group_ibfk_1` FOREIGN KEY (`factory_place_id`) REFERENCES `tool_factory_place` (`id`);

--
-- Obmedzenie pre tabuľku `tool_manufacturer_module`
--
ALTER TABLE `tool_manufacturer_module`
  ADD CONSTRAINT `factory` FOREIGN KEY (`factory_id`) REFERENCES `platform_factory` (`id`),
  ADD CONSTRAINT `tool_manufacturer_module_ibfk_1` FOREIGN KEY (`external_endpoint_configuration`) REFERENCES `tool_external_endpoint_configuration` (`id`),
  ADD CONSTRAINT `tool_manufacturer_module_ibfk_2` FOREIGN KEY (`secure_basic_id`) REFERENCES `secure_basic` (`id`),
  ADD CONSTRAINT `warehouse` FOREIGN KEY (`warehouse_id`) REFERENCES `tool_factory_place` (`id`);

--
-- Obmedzenie pre tabuľku `tool_manufacturer_module_property`
--
ALTER TABLE `tool_manufacturer_module_property`
  ADD CONSTRAINT `tool_manufacturer_module_property_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `tool_manufacturer_module` (`id`);

--
-- Obmedzenie pre tabuľku `tool_manufacturer_module_shop_property`
--
ALTER TABLE `tool_manufacturer_module_shop_property`
  ADD CONSTRAINT `tool_manufacturer_module_shop_property_ibfk_1` FOREIGN KEY (`platform_shop_id`) REFERENCES `platform_shop` (`id`),
  ADD CONSTRAINT `tool_manufacturer_module_shop_property_ibfk_2` FOREIGN KEY (`tool_manufacturer_module_id`) REFERENCES `tool_manufacturer_module` (`id`);

--
-- Obmedzenie pre tabuľku `tool_notification_seen`
--
ALTER TABLE `tool_notification_seen`
  ADD CONSTRAINT `tool_notification_seen_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `platform_user` (`id`);

--
-- Obmedzenie pre tabuľku `tool_order`
--
ALTER TABLE `tool_order`
  ADD CONSTRAINT `tool_order_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `tool_shop` (`id`),
  ADD CONSTRAINT `tool_order_ibfk_2` FOREIGN KEY (`platform_order_id`) REFERENCES `platform_order` (`id`),
  ADD CONSTRAINT `tool_order_ibfk_3` FOREIGN KEY (`reserved_warehouse_id`) REFERENCES `tool_factory_place` (`id`),
  ADD CONSTRAINT `tool_order_ibfk_4` FOREIGN KEY (`sender_address_id`) REFERENCES `tool_shipping_address` (`id`),
  ADD CONSTRAINT `tool_order_ibfk_5` FOREIGN KEY (`receiver_address_id`) REFERENCES `tool_shipping_address` (`id`),
  ADD CONSTRAINT `tool_order_ibfk_6` FOREIGN KEY (`shipping_id`) REFERENCES `tool_order_shipping` (`id`),
  ADD CONSTRAINT `tool_order_ibfk_7` FOREIGN KEY (`reservation_id`) REFERENCES `tool_order_reservation` (`id`);

--
-- Obmedzenie pre tabuľku `tool_order_action`
--
ALTER TABLE `tool_order_action`
  ADD CONSTRAINT `tool_order_action_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`),
  ADD CONSTRAINT `tool_order_action_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `platform_user` (`id`);

--
-- Obmedzenie pre tabuľku `tool_order_action_item`
--
ALTER TABLE `tool_order_action_item`
  ADD CONSTRAINT `tool_order_action_item_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `tool_order` (`id`),
  ADD CONSTRAINT `tool_order_action_item_ibfk_2` FOREIGN KEY (`action_id`) REFERENCES `tool_order_action` (`id`);

--
-- Obmedzenie pre tabuľku `tool_order_attachment`
--
ALTER TABLE `tool_order_attachment`
  ADD CONSTRAINT `tool_order_attachment_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `tool_order` (`id`),
  ADD CONSTRAINT `tool_order_attachment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `platform_user` (`id`);

--
-- Obmedzenie pre tabuľku `tool_order_history`
--
ALTER TABLE `tool_order_history`
  ADD CONSTRAINT `tool_order_history_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `tool_order` (`id`),
  ADD CONSTRAINT `tool_order_history_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `platform_user` (`id`);

--
-- Obmedzenie pre tabuľku `tool_order_reservation`
--
ALTER TABLE `tool_order_reservation`
  ADD CONSTRAINT `tool_order_reservation_ibfk_2` FOREIGN KEY (`manufacturer_module_id`) REFERENCES `tool_manufacturer_module` (`id`);

--
-- Obmedzenie pre tabuľku `tool_order_shipping`
--
ALTER TABLE `tool_order_shipping`
  ADD CONSTRAINT `tool_order_shipping_ibfk_3` FOREIGN KEY (`shipping_provider_id`) REFERENCES `tool_shipping_provider` (`id`),
  ADD CONSTRAINT `tool_order_shipping_ibfk_4` FOREIGN KEY (`ticket_print_user_id`) REFERENCES `platform_user` (`id`),
  ADD CONSTRAINT `tool_order_shipping_ibfk_5` FOREIGN KEY (`gather_record_id`) REFERENCES `tool_gather_record` (`id`),
  ADD CONSTRAINT `tool_order_shipping_ibfk_6` FOREIGN KEY (`carrier_id`) REFERENCES `platform_carrier_service` (`id`);

--
-- Obmedzenie pre tabuľku `tool_order_x_flag`
--
ALTER TABLE `tool_order_x_flag`
  ADD CONSTRAINT `tool_order_x_flag_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `tool_order` (`id`),
  ADD CONSTRAINT `tool_order_x_flag_ibfk_2` FOREIGN KEY (`flag_id`) REFERENCES `tool_order_flag` (`id`);

--
-- Obmedzenie pre tabuľku `tool_product`
--
ALTER TABLE `tool_product`
  ADD CONSTRAINT `tool_product_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `tool_order` (`id`),
  ADD CONSTRAINT `tool_product_ibfk_2` FOREIGN KEY (`platform_ordered_product_id`) REFERENCES `platform_designed_ordered_product` (`id`);

--
-- Obmedzenie pre tabuľku `tool_product_comment`
--
ALTER TABLE `tool_product_comment`
  ADD CONSTRAINT `tool_product_comment_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `tool_product` (`id`),
  ADD CONSTRAINT `tool_product_comment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `platform_user` (`id`),
  ADD CONSTRAINT `tool_product_comment_ibfk_3` FOREIGN KEY (`order_id`) REFERENCES `tool_order` (`id`);

--
-- Obmedzenie pre tabuľku `tool_provider_branch`
--
ALTER TABLE `tool_provider_branch`
  ADD CONSTRAINT `tool_provider_branch_ibfk_1` FOREIGN KEY (`module_provider_id`) REFERENCES `platform_shipping_module_provider` (`id`);

--
-- Obmedzenie pre tabuľku `tool_shipping_customize`
--
ALTER TABLE `tool_shipping_customize`
  ADD CONSTRAINT `shipping` FOREIGN KEY (`platform_shipping_module_provider_id`) REFERENCES `platform_shipping_module_provider` (`id`),
  ADD CONSTRAINT `tool_shipping_customize_ibfk_1` FOREIGN KEY (`filter_preset_id`) REFERENCES `tool_filter_preset` (`id`),
  ADD CONSTRAINT `tool_shipping_customize_ibfk_2` FOREIGN KEY (`warehouse_id`) REFERENCES `tool_factory_place` (`id`);

--
-- Obmedzenie pre tabuľku `tool_shipping_module`
--
ALTER TABLE `tool_shipping_module`
  ADD CONSTRAINT `tool_shipping_module_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `tool_shipping_property`
--
ALTER TABLE `tool_shipping_property`
  ADD CONSTRAINT `tool_shipping_property_ibfk_1` FOREIGN KEY (`shipping_id`) REFERENCES `tool_order_shipping` (`id`);

--
-- Obmedzenie pre tabuľku `tool_shipping_provider`
--
ALTER TABLE `tool_shipping_provider`
  ADD CONSTRAINT `tool_shipping_provider_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `tool_shipping_module` (`id`);

--
-- Obmedzenie pre tabuľku `tool_shipping_setup`
--
ALTER TABLE `tool_shipping_setup`
  ADD CONSTRAINT `tool_shipping_setup_ibfk_1` FOREIGN KEY (`module_type_id`) REFERENCES `platform_shipping_module_type` (`id`),
  ADD CONSTRAINT `tool_shipping_setup_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `tool_shipping_setup_property`
--
ALTER TABLE `tool_shipping_setup_property`
  ADD CONSTRAINT `tool_shipping_setup_property_ibfk_1` FOREIGN KEY (`setup_id`) REFERENCES `tool_shipping_setup` (`id`);

--
-- Obmedzenie pre tabuľku `tool_shop`
--
ALTER TABLE `tool_shop`
  ADD CONSTRAINT `tool_shop_ibfk_2` FOREIGN KEY (`platform_shop_id`) REFERENCES `platform_shop` (`id`),
  ADD CONSTRAINT `tool_shop_ibfk_3` FOREIGN KEY (`factory_account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `tool_shop_x_address`
--
ALTER TABLE `tool_shop_x_address`
  ADD CONSTRAINT `tool_shop_x_address_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `tool_shop` (`id`),
  ADD CONSTRAINT `tool_shop_x_address_ibfk_2` FOREIGN KEY (`address_id`) REFERENCES `tool_shipping_address` (`id`);

--
-- Obmedzenie pre tabuľku `tool_stock_management_rule`
--
ALTER TABLE `tool_stock_management_rule`
  ADD CONSTRAINT `tool_stock_management_rule_ibfk_1` FOREIGN KEY (`preset_id`) REFERENCES `tool_filter_preset` (`id`),
  ADD CONSTRAINT `tool_stock_management_rule_ibfk_2` FOREIGN KEY (`warehouse_id`) REFERENCES `tool_factory_place` (`id`);

--
-- Obmedzenie pre tabuľku `tool_summary`
--
ALTER TABLE `tool_summary`
  ADD CONSTRAINT `tool_summary_ibfk_3` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`),
  ADD CONSTRAINT `tool_summary_ibfk_4` FOREIGN KEY (`timeslot_id`) REFERENCES `tool_timeslot` (`id`);

--
-- Obmedzenie pre tabuľku `tool_summary_x_order`
--
ALTER TABLE `tool_summary_x_order`
  ADD CONSTRAINT `tool_summary_x_order_ibfk_1` FOREIGN KEY (`summary_id`) REFERENCES `tool_summary` (`id`),
  ADD CONSTRAINT `tool_summary_x_order_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `tool_order` (`id`);

--
-- Obmedzenie pre tabuľku `tool_technology_map`
--
ALTER TABLE `tool_technology_map`
  ADD CONSTRAINT `tool_technology_map_ibfk_1` FOREIGN KEY (`factory_technology_id`) REFERENCES `platform_print_technology` (`id`),
  ADD CONSTRAINT `tool_technology_map_ibfk_2` FOREIGN KEY (`shop_technology_id`) REFERENCES `platform_print_technology` (`id`);

--
-- Obmedzenie pre tabuľku `tool_timeslot`
--
ALTER TABLE `tool_timeslot`
  ADD CONSTRAINT `tool_timeslot_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`),
  ADD CONSTRAINT `tool_timeslot_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `tool_group` (`id`),
  ADD CONSTRAINT `tool_timeslot_ibfk_3` FOREIGN KEY (`workspace_id`) REFERENCES `tool_work_space` (`id`);

--
-- Obmedzenie pre tabuľku `tool_warehouse_stock_email`
--
ALTER TABLE `tool_warehouse_stock_email`
  ADD CONSTRAINT `tool_warehouse_stock_email_ibfk_1` FOREIGN KEY (`warehouse_id`) REFERENCES `tool_factory_place` (`id`);

--
-- Obmedzenie pre tabuľku `tool_work_space`
--
ALTER TABLE `tool_work_space`
  ADD CONSTRAINT `tool_work_space_ibfk_1` FOREIGN KEY (`factory_place_id`) REFERENCES `tool_factory_place` (`id`);

--
-- Obmedzenie pre tabuľku `tool_zoho_contact`
--
ALTER TABLE `tool_zoho_contact`
  ADD CONSTRAINT `tool_zoho_contact_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `tool_manufacturer_module` (`id`);

--
-- Obmedzenie pre tabuľku `tool_zoho_fulfilled`
--
ALTER TABLE `tool_zoho_fulfilled`
  ADD CONSTRAINT `tool_zoho_fulfilled_ibfk_1` FOREIGN KEY (`zoho_order_id`) REFERENCES `tool_zoho_order` (`id`);

--
-- Obmedzenie pre tabuľku `tool_zoho_inventory`
--
ALTER TABLE `tool_zoho_inventory`
  ADD CONSTRAINT `tool_zoho_inventory_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `tool_manufacturer_module` (`id`);

--
-- Obmedzenie pre tabuľku `tool_zoho_invoice`
--
ALTER TABLE `tool_zoho_invoice`
  ADD CONSTRAINT `tool_zoho_invoice_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `platform_account` (`id`);

--
-- Obmedzenie pre tabuľku `tool_zoho_invoice_item`
--
ALTER TABLE `tool_zoho_invoice_item`
  ADD CONSTRAINT `tool_zoho_invoice_item_ibfk_1` FOREIGN KEY (`invoice_id`) REFERENCES `tool_zoho_invoice` (`id`),
  ADD CONSTRAINT `tool_zoho_invoice_item_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `tool_product` (`id`);

--
-- Obmedzenie pre tabuľku `tool_zoho_invoice_x_order`
--
ALTER TABLE `tool_zoho_invoice_x_order`
  ADD CONSTRAINT `tool_zoho_invoice_x_order_ibfk_1` FOREIGN KEY (`invoice_id`) REFERENCES `tool_zoho_invoice` (`id`),
  ADD CONSTRAINT `tool_zoho_invoice_x_order_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `tool_order` (`id`);

--
-- Obmedzenie pre tabuľku `tool_zoho_item`
--
ALTER TABLE `tool_zoho_item`
  ADD CONSTRAINT `tool_zoho_item_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `tool_manufacturer_module` (`id`);

--
-- Obmedzenie pre tabuľku `tool_zoho_order`
--
ALTER TABLE `tool_zoho_order`
  ADD CONSTRAINT `tool_zoho_order_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `tool_manufacturer_module` (`id`),
  ADD CONSTRAINT `tool_zoho_order_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `tool_order` (`id`);

--
-- Obmedzenie pre tabuľku `tool_zoho_order_item`
--
ALTER TABLE `tool_zoho_order_item`
  ADD CONSTRAINT `tool_zoho_order_item_ibfk_1` FOREIGN KEY (`zoho_order_id`) REFERENCES `tool_zoho_order` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
